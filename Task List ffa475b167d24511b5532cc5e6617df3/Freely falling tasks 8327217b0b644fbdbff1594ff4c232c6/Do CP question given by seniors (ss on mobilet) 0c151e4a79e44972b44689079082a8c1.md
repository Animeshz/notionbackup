# Do CP question given by seniors (ss on mobilet)

Done: Yes
Duration: Day
Tags: Needs to be done

# InfoSec

## Binary Search

- [https://codeforces.com/edu/course/2/lesson/6/1/practice/contest/283911/problem/A](https://codeforces.com/edu/course/2/lesson/6/1/practice/contest/283911/problem/A)
- [https://codeforces.com/edu/course/2/lesson/6/1/practice/contest/283911/problem/B](https://codeforces.com/edu/course/2/lesson/6/1/practice/contest/283911/problem/B)
- [https://codeforces.com/edu/course/2/lesson/6/1/practice/contest/283911/problem/C](https://codeforces.com/edu/course/2/lesson/6/1/practice/contest/283911/problem/C)
- [https://www.hackerrank.com/contests/launchpad-1-winter-challenge/challenges/binary-search-advanced](https://www.hackerrank.com/contests/launchpad-1-winter-challenge/challenges/binary-search-advanced)
- [https://www.codechef.com/problems/CHEF10Y](https://www.codechef.com/problems/CHEF10Y)
- [https://www.codechef.com/problems/RIGHTTRI](https://www.codechef.com/problems/RIGHTTRI)
- [https://www.interviewbit.com/problems/allocate-books/](https://www.interviewbit.com/problems/allocate-books/)
- [https://www.spoj.com/problems/EKO/](https://www.codechef.com/problems/RIGHTTRIhttps://www.spoj.com/problems/EKO)
- [https://www.spoj.com/problems/AGGRCOW/](https://www.spoj.com/problems/AGGRCOW/)
- [https://codeforces.com/contest/1223/problem/C](https://codeforces.com/contest/1223/problem/C)
- [https://codeforces.com/contest/1315/problem/B](https://codeforces.com/contest/1315/problem/B)
    - Another question of Binary Search  [https://codeforces.com/contest/1480/problem/C](https://codeforces.com/contest/1480/problem/C). It's a bit hard (so you'll need the help of editorial most probably) but is a good question for understanding BS.

## Sieve of Erathonos

1. [https://www.codechef.com/problems/CHEFPRMS](https://www.codechef.com/problems/CHEFPRMS)
2. [https://codeforces.com/problemset/problem/26/A](https://codeforces.com/problemset/problem/26/A)
3. [https://www.codechef.com/problems/KPRIME](https://www.codechef.com/problems/KPRIME)
4. [https://www.spoj.com/problems/TDPRIMES/](https://www.spoj.com/problems/TDPRIMES/) (constraints are really tight so be careful)
5. [https://www.spoj.com/problems/TDKPRIME/](https://www.spoj.com/problems/TDKPRIME/) (again pay attention to the constraints)
6. [https://codeforces.com/problemset/problem/17/A](https://codeforces.com/problemset/problem/17/A)
7. [https://www.spoj.com/problems/NFACTOR/](https://www.spoj.com/problems/NFACTOR/)
8. [https://codeforces.com/problemset/problem/230/B](https://codeforces.com/problemset/problem/230/B)
9. [https://www.hackerearth.com/problem/algorithm/game-of-function/](https://www.hackerearth.com/problem/algorithm/game-of-function/)

## A heavy edge-case problem

- HXOR (CodeChef)

# CP

Yesterday's Recording
[https://drive.google.com/file/d/1AJrqUxinuESSGg5WiQlB-n9rgOASoEGl/view?usp=sharing](https://drive.google.com/file/d/1AJrqUxinuESSGg5WiQlB-n9rgOASoEGl/view?usp=sharing)

Finish these rooms before next weekend
[https://tryhackme.com/room/linuxfundamentalspart1](https://tryhackme.com/room/linuxfundamentalspart1)[https://tryhackme.com/room/linuxfundamentalspart2](https://tryhackme.com/room/linuxfundamentalspart2)[https://tryhackme.com/room/linuxfundamentalspart3](https://tryhackme.com/room/linuxfundamentalspart3)

this was the command

wget [https://mercury.picoctf.net/static/f95b1ee9f29d631d99073e34703a2826/warm](https://mercury.picoctf.net/static/f95b1ee9f29d631d99073e34703a2826/warm)