# xlocate: allow to search through all the active repositories (as defined in /etc/xbps.d)

Done: Yes
Duration: Week
Tags: Needs to be done

[https://github.com/leahneukirchen/xtools/issues/230](https://github.com/leahneukirchen/xtools/issues/230)

```bash
# git merge -Xours -s octopus?? Not possible? Unable to find common commit with xpack/main Automatic merge failed; fix conflicts and then commit the result.
# Do rebase -i with script: https://stackoverflow.com/questions/12394166/how-do-i-run-git-rebase-interactive-in-non-interactive-manner, move lines based on /etc/xbps.d, squash/fixup or drop all below and itself + rebuild
# Build steps: https://github.com/TheLocehiliosan/yadm/blob/master/yadm do_checkout (ours strategy, donot do anything for conflicting lesser priority/higher number)
XBPS_REPOSITORIES=$(sort -u <(ls /etc/xbps.d) <(ls /usr/share/xbps.d) | xargs -I{} bash -c 'source /etc/xbps.d/{} 2>/dev/null || source /usr/share/xbps.d/{}; echo $repository' | grep .)
XBPS_REPOSITORIES_DBPATH=$(echo $XBPS_REPOSITORIES | awk '{if(index($0, ":")) {gsub(/[\.:\/]/, "_", $0); gsub(/^/, "/var/db/xbps/", $0);} print}')
XLOCATE_REPOSITORY=$(echo $XBPS_REPOSITORIES | sed 's/^\(.*\)\/.*$/\1\/xlocate\/xlocate.git/')

# https://stackoverflow.com/questions/13772550/deleting-all-commits-in-a-branch-after-certain-commit#:~:text=If%20you%20want%20to%20revert,original%2Dbranch%20new%2Dbranch%20.
# drop all commits after this REPOSITORY, rebuild later(s) now
if changes_before || ($(loop XBPS_REPOSITORIES_DBPATH)/*-repodata is changed && drop_commits); then
  if git ls-remote $(loop XLOCATE_REPOSITORY) &>/dev/null; then
    # default branch: https://stackoverflow.com/questions/28666357/git-how-to-get-default-branch
    remote_name=$(XBPS_REPOSITORIES[index]%.conf)  #filename w/o extension
    branch_name=$(git remote show [your_remote] | sed -n '/HEAD branch/s/.*: //p')
    git remote [add|set-url] $remote_name <XLOCATE_REPOSITORY[index]> && git fetch $remote_name $branch_name
    git ls-files --deleted | xargs git checkout  # yadm-ref: https://github.com/TheLocehiliosan/yadm/blob/master/yadm
  else
    xbps-query --repository=$(loop XBPS_REPOSITORIES not having XLOCATE_REPOSITORY[index]) -Mio '*' | awk 'xlocate db conversion script'
  fi
  git commit -am 'remote_name'
fi
```

```bash
#!/bin/sh
# xlocate [-g | -S | PATTERN] - locate files in all XBPS packages

: ${XDG_CACHE_HOME:=~/.cache}
: ${XDG_CONFIG_HOME:=~/.config}
: ${XLOCATE_CONF:="${XDG_CONFIG_HOME}/xlocate.conf"}

if [ -f "${XLOCATE_CONF}" ]; then
	. "${XLOCATE_CONF}";
fi

: ${XLOCATE_GIT:=$XDG_CACHE_HOME/xlocate.git}
# git merge -Xours -s octopus?? Not possible? Unable to find common commit with xpack/main Automatic merge failed; fix conflicts and then commit the result.
# Do rebase -i with script: https://stackoverflow.com/questions/12394166/how-do-i-run-git-rebase-interactive-in-non-interactive-manner, move lines based on /etc/xbps.d, squash/fixup or drop all below and itself + rebuild
# Build steps: https://github.com/TheLocehiliosan/yadm/blob/master/yadm do_checkout (ours strategy, donot do anything for conflicting lesser priority/higher number)
: ${XLOCATE_REPOS:=($(find /etc/xbps.d -name '*repodata'))}

if command -v pv >/dev/null; then
	PROGRESS="pv -l"
else
	PROGRESS=cat
fi

xupdategit() {
	set -e
	DIR=$(mktemp -dt xlocate.XXXXXX)
	DIR=$(/usr/bin/realpath -e "$DIR")
	git init -q $DIR
	cd $DIR
	xbps-query -M -Ro '*' | $PROGRESS | awk '
		$0 ~ ": " {
			s = index($0, ": ")
			pkg = substr($0, 1, s-1)
			file = substr($0, s+2)
			sub(" *\\([^)]*\\)$", "", file)
			print file >>pkg
			}'
	printf '%s\n' ./* |
		LC_ALL= xargs -d'\n' -I'{}' -n1 -P "$(nproc)" -r -- \
			sort -o {} {}
	git add ./*
	git -c user.name=xupdategit -c user.email=xupdategit@none commit -q -m 'xupdategit'
	git repack -ad
	rm -rf "$XLOCATE_GIT" .git/COMMIT_EDITMSG .git/description \
		.git/index .git/hooks .git/logs
	[ -n "${XLOCATE_GIT%/*}" ] && mkdir -p "${XLOCATE_GIT%/*}"
	mv .git "$XLOCATE_GIT"
	rm -rf "$DIR"
}

xsyncgit() {
	if ! [ -d "$XLOCATE_GIT" ]; then
		[ -n "${XLOCATE_GIT%/*}" ] && mkdir -p "${XLOCATE_GIT%/*}"
		git clone --bare "$XLOCATE_REPO" "$XLOCATE_GIT"
	fi
	git -C "$XLOCATE_GIT" fetch -u -f "$XLOCATE_REPO" master:master
}

xupdategit() {
	set -e
	DIR=$(mktemp -dt xlocate.XXXXXX)
	DIR=$(/usr/bin/realpath -e "$DIR")
	git init -q $DIR
	cd $DIR
	xbps-query -M -Ro '*' | $PROGRESS | awk '
		$0 ~ ": " {
			s = index($0, ": ")
			pkg = substr($0, 1, s-1)
			file = substr($0, s+2)
			sub(" *\\([^)]*\\)$", "", file)
			print file >>pkg
			}'
	printf '%s\n' ./* |
		LC_ALL= xargs -d'\n' -I'{}' -n1 -P "$(nproc)" -r -- \
			sort -o {} {}
	git add ./*
	git -c user.name=xupdategit -c user.email=xupdategit@none commit -q -m 'xupdategit'
	git repack -ad
	rm -rf "$XLOCATE_GIT" .git/COMMIT_EDITMSG .git/description \
		.git/index .git/hooks .git/logs
	[ -n "${XLOCATE_GIT%/*}" ] && mkdir -p "${XLOCATE_GIT%/*}"
	mv .git "$XLOCATE_GIT"
	rm -rf "$DIR"
}

case "$1" in
-g)
	xupdategit
	exit $?;;
-S)
	xsyncgit
	exit $?;;
'')
	echo "Usage: xlocate [-g | -S | PATTERN]" 1>&2
	exit 1;;
esac

if [ -d "$XLOCATE_GIT" ]; then
	if [ -f "$XLOCATE_GIT/refs/heads/master" ]; then
		BASE="$XLOCATE_GIT/refs/heads/master"
	elif [ -f "$XLOCATE_GIT/FETCH_HEAD" ]; then
		BASE="$XLOCATE_GIT/FETCH_HEAD"
	fi
	if [ -z "$BASE" ] || find /var/db/xbps/ -name '*repodata' -newer "$BASE" | grep -q .; then
		if grep -q origin "$XLOCATE_GIT/config"; then
			echo "xlocate: database outdated, please run xlocate -S." 1>&2
		else
			echo "xlocate: database outdated, please run xlocate -g." 1>&2
		fi
	fi
	git -c grep.lineNumber=false --git-dir="$XLOCATE_GIT" grep -- "$@" @ |
		sed 's/^@://; s/:/\t/' | grep .
else
	echo "xlocate: database not found, please use xlocate -S." 1>&2
	exit 1
fi
```