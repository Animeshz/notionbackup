# College Portal

Done: No
Duration: Month
Priority: Important & Urgent
Project Source: https://github.com/Animeshz/infix
Tags: Needs to be done
Type: Project

# Infix - A platform to create interactions

**TechStack (expected):**

1. Frontend:
    1. TailwindCSS (+DaisyUI)
    2. Vue
2. Backend:
    1. Rocket.rs (API & default page-serving)
    2. [Diesel](https://diesel.rs/guides/getting-started) / Meilisearch (database / search)

[SPA-Routing (back)](https://www.youtube.com/watch?v=O-4uc9UOYcE)

[Svelte Playground](https://svelte.dev/examples/hello-world)

https://github.com/zupzup/rust-fullstack-example | ‣ [+msg from author](https://discord.com/channels/273534239310479360/335502747409317928/971949056404385840)

[Tailwind Dashboard - YT](https://www.youtube.com/results?search_query=tailwind+dashboard)

File-Based GIT (delta-checkpoints & signing)

[Rust-SSR-Demo-full-rust](https://www.reddit.com/r/rust/comments/ioa2b4/serverside_rendering_a_rust_web_app_sauron_warp/?utm_medium=android_app&utm_source=share)

[Dashboard in Tailwind - YT](https://www.youtube.com/results?search_query=dashboard+in+tailwind)

[Express.js to Rocket.rs](https://yidaotus.medium.com/learning-rust-by-porting-an-express-js-server-to-rocket-rs-part-2-71c2f8ec388e)

### Home

### Project LayOut

<aside>
💡 Description:

- Display your projects
- Let others know about your project, may use if like, or contribute if they like
- Put description, pre-requisites, project-learnings (if sb wants to contrib), contributer-needed tag
- Author contact/chat, project-suggestion panel
- Project listing (sorting by new or popular) - like youtube
</aside>

### Idea Enhancement

- Discussing idea with people let it be shaped more connected to the real-world, and offers great suggestions to improve on it.
- I myself was shocked by how much a confronting group discussion (at the stage without preparation) had improvised on topics given to us in the PC (professional communication) class.
- It’ll also give real-life validation without spending on ads to check if idea gonna work or not.

15-15% share holding for thinker & improver

### Resources Share / News / Student Blog Share (whatever I’m good at), collaborative pages showing links to awesome topics with right for everyone to drop suggestions.

Distraction Free 

multi-people resource enhancement - wiki-like (for non-personal blog)

What I can/want to deliver ():

# Git

# StackFrame / Scopes

# Stack & Heap growth

# Best Practises

Short Circuiting code

  - early exit

  - simplify boolean expr 

# Linux

### Team Forming section (chat + announcements) [invite-accept and open-join too]

### Club Annoucements

### QnA Forum

Not always true only wing member can reply, also true that anybody listening to topic assigned to question may get notification and can comment.

E.g. My questions:

- Donation sponsor funding shares
- How much current is required if i want to keep spring based lever intact. (or just use relays?

### Anonymous Chat (e.g. for borrowing special items like soldering iron, or whatever)

### Voice Raise (optional)

### Centralized Prime Product-Ordering / Market-Going day / Prime proxy-based data tunneling / etc. (Sharing purchased prime plans / stuffs)

**Findings (while exploring):**

SPA including rust: https://github.com/SergioBenitez/Rocket/issues/1183

Template engine in rust (jinja/pebble/twig alternative): https://github.com/djc/askama

[TailwindCSS Plugin](https://daisyui.com/)

Using cargo as makefile: https://github.com/sagiegurari/cargo-make

[Plugin Architecture](https://stackoverflow.com/a/10913898/11377112)

Client side caching vs Server side rendering

Chrome Lighthouse for PWA ready check

[Progressive Web Apps in 100 Seconds // Build a PWA from Scratch](https://www.youtube.com/watch?v=sFsRylCQblw)