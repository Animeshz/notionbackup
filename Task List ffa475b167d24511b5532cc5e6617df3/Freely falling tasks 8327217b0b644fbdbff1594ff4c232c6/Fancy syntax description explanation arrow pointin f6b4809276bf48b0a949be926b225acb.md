# Fancy syntax description explanation arrow pointing

Done: No
Duration: Week
Priority: Important & Not Urgent
Project Source: https://github.com/Animeshz/unidraw
Tags: Needs to be done
Type: Project

FancyUnicodeDrawingLibrary (Non Movable CLI) - FancyPrint?

# API Description

**Abstraction Level 0:**

- Small unicode printing utilities like arrow symbol or special characters like sigma, pi, etc.
Ability to choose from unicode or ascii equivalent (ref: [ariadne/draw.rs](https://github.com/zesterer/ariadne/blob/dc1396c5e430b455d3b685db5e773cab689f7c1d/src/draw.rs#L31))
- Colorizing characters
- ability to get terminal dimensions (`tput cols` | `tput lines`)

**Abstraction Level 1:**

- **Two modes:**
GLOBAL→ **TUI** (reactive - flutter/compose like)  |  **CLI** (static, print, exit)
CLI: one buffer matrix, write, flush, exit.
TUI: two buffer matrix, compare & diff, redraw diff, flush, listen for events like terminal resize / manually emitted events (keybind/hooks).
- **ConstraintLayout System** (% | weight-index system ++ a minimum-width maximum-width <also in % or character-sections value> thingy)
    - Spring connections described as:
    - Every Shape has Vec<left> edges that can be referenced from any other shape’s Vec<right>’s any one edge. Vice-Versa for top-bottom.
    - 4 special edges pointing to terminal’s left, bottom, top, right.
    - Runtime Check for all the constraint assigned before rendering (and probably an optional option which tries to fit randomly if not specified, very unlikely in first release)
    - No concept of parent-child unlike flutter/compose, there are edges which can be used to flexize the position and span of widget.
- Ability to write custom shapes (reusable) with low-level access.
- **Shape Representation:** 90°/270° anchors with direction (like bottom-left OR bottom-right).
We’ll be drawing top to bottom, so linear on y (up to down) is good to go.
Initial draw into buffer matrix → intercept (like in abstraction level 2) → finalize buffer matrix →draw
- **Shape fn:** union, subtract, intersect, exclude with other shapes

**Abstraction Level 2:**

- **Span** (e.g. Text, containing a definite size / character-width) to modify the size of spring-connected adjacent (neighboring) shapes.
    - Usecase: Label/Description Group which tries to minimize lines and prints the label (downward), algorithm: lower length up to higher length down.
- **Path:** From free (unconstrained) edge of any shape (left, right, top, bottom vector). With 3 algorithms: start middle end
- Finding a good path while having no clashes (w/reserved space)…
→ Minimum distance line (min characters consumed)
→ Outer nearest border line
**Note:** No same directional line overlap allowed, in case of 90° intersection tho allow if next char to it (except another perpendicular line) is available free from reserved space.
  ⇒ For n lines to pass through a line, there must be atleast n free (unreserved) characters in each of the line, below.

### Inspirations:

[https://github.com/zesterer/ariadne](https://github.com/zesterer/ariadne)

[https://github.com/pouyakary/TextGraphic](https://github.com/pouyakary/TextGraphic)

[https://github.com/fdehau/tui-rs](https://github.com/fdehau/tui-rs)

**Misc things:**

Provide printing in both stderr (primary) and stdout

Integrate usage of Modifier into Cells of Shape

Loopback algorithm for printing lines/paths - rizin/cutter (ez: free edge vectors)

Self structure printing using arrows and boxes repr struct

**Proposal:**

1. common.rs (reserving cells, etc.)
2. highlight.rs (color)
3. unicode/mod.rs (2d)
    1. path.rs (description - anchor’s char & directional info, color, description in case of an optional label)
    2. unicode.rs (box, etc.)

- We can do everything with 2d (unicode/mod) but other things like path are just for convineance instead of drawing the symbols. (that [includes mod.rs in every subfile](https://doc.rust-lang.org/std/keyword.super.html))

```rust
/// Newer thought
rendering_ctx.line().from().to().to().to()
rendering_ctx.[closed_]shape().from().to()  // how to care about holes
                                            // maybe virtual rendering_ctx under shape?

// More like:
terminal.draw(|rendering_ctx| { /*draw*/ })
// where rendering_ctx is only valid for the moment, with the terminal dimensions, it's like virtual-DOM.

/// Older thought
struct Config {
    percentage_roundoff: RoundOff|CoerceDown|CoerceUp;
}

// Anchor/Vertex
struct Anchor(x, y);
type Edge = (Anchor, Anchor)

// Somehow integrate modifiers to cells of `Shape`
trait Shape {
    fn new(shape_attrib)  // rect: wxh, circle: r, etc.

    fn edges() -> Vec<Edge>
    fn constraint(edge: &Edge, other_edge: &Edge) -> bool
    fn constrained() -> bool              // properly constrained or not

    fn render(ctx: &mut RenderCtx, drawable_space: (x, y), interceptor: F) -> ???  {}

    // widget specific functions in widget impl, that should be reflected in render
}

struct CharacterFillShape {  // CharacterFillShape : Shape
    c: char;
    anchors: Vec<(Anchor,Direction)>;
    edges: Vec<Edge>;
}

impl Modifier {
    fn default() -> Self  // default trait

    fn color(&mut self, c: Color)
    fn bg_color(&mut self, c: Color)
    // fn padding(&mut self, l: u16, b: u16, t: u16, r: u16) -> Self
    // fn path_reserve(&mut self, opt: bool) -> Self
    // fn z_index(value: u8) -> Self                // overlapping or for arrow

    fn and(&mut self, other: Modifier) -> Self
}
```

Stuffs found while searching:

[slotmap](https://docs.rs/slotmap/latest/slotmap/index.html)

[How to restrict the construction of struct?](https://stackoverflow.com/a/70965787/11377112)

[Star History](https://star-history.com/)

[third_party/WebKit/Source/core/dom/TreeScope.cpp - chromium/src - Git at Google](https://chromium.googlesource.com/chromium/src/+/8138e62d38b64d24e2220d50907b8aeb71efd32e/third_party/WebKit/Source/core/dom/TreeScope.cpp)

How chromium get object by location (coordinates) in 2d

Characters:

[command line drawing unicode boxes for a graph](https://stackoverflow.com/questions/34913107/command-line-drawing-unicode-boxes-for-a-graph)

[Draw a horizontal 1 line box with unicode characters](https://stackoverflow.com/questions/62699355/draw-a-horizontal-1-line-box-with-unicode-characters)

### Layout System

[](https://developer.android.com/reference/androidx/constraintlayout/widget/ConstraintLayout)

[Flutter_ConstraintLayout/core.dart at master · hackware1993/Flutter_ConstraintLayout](https://github.com/hackware1993/Flutter_ConstraintLayout/blob/master/lib/src/core.dart)

Hold (store) both shape & path. Use [layout.rs](http://layout.rs) at root to layout them up into a buffer (specify min/max, so to resize in case there is not enough space). Use [render.rs](http://render.rs) to render the buffer into terminal. No need of [terminal.rs](http://terminal.rs).