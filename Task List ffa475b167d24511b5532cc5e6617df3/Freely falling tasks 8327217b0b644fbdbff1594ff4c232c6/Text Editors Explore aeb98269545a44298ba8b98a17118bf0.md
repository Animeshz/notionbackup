# Text Editors Explore

Done: No
Duration: Week
Priority: Not Important & Not Urgent
Tags: Archived, Needs to be done, Stale

[Folding](https://vim.fandom.com/wiki/Folding)

[Everything you need to know to configure neovim using lua](https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua/)

---

Amp looks great actually, the default keybinds (capital movement, and m/, and jumping with f)

Mainly copy the keybind

[https://github.com/jmacdonald/amp](https://github.com/jmacdonald/amp)

[https://github.com/helix-editor/helix](https://github.com/helix-editor/helix)

[Vscode Alternative or all Rust IDE WIP?](https://www.reddit.com/r/rust/comments/c07rpi/vscode_alternative_or_all_rust_ide_wip/)

(vim in same post: [https://www.reddit.com/r/rust/comments/c07rpi/comment/er2e12n/?utm_source=share&utm_medium=web2x&context=3](https://www.reddit.com/r/rust/comments/c07rpi/comment/er2e12n/?utm_source=share&utm_medium=web2x&context=3))

[Emacs to Neovim](https://www.reddit.com/r/neovim/comments/n8kxx7/emacs_to_neovim/)

(nice plugins same post: [https://www.reddit.com/r/neovim/comments/n8kxx7/comment/gxje1j0/?utm_source=share&utm_medium=web2x&context=3](https://www.reddit.com/r/neovim/comments/n8kxx7/comment/gxje1j0/?utm_source=share&utm_medium=web2x&context=3))

[Collections of Awesome Neovim Plugins](https://morioh.com/p/a7063de46490)

[https://github.com/rockerBOO/awesome-neovim](https://github.com/rockerBOO/awesome-neovim)

[https://github.com/LunarVim/Neovim-from-scratch](https://github.com/LunarVim/Neovim-from-scratch)

[https://github.com/potamides/dotfiles](https://github.com/potamides/dotfiles)

### Plugins

[r/neovim - Comment by u/MiraculousMoon on "Is your NeoVim still fast after adding plugins ?"](https://www.reddit.com/r/neovim/comments/qn1cci/comment/hje1jm6/?utm_source=share&utm_medium=web2x&context=3)

[https://github.com/CosmicNvim/cosmic-ui](https://github.com/CosmicNvim/cosmic-ui)

[https://github.com/romgrk/barbar.nvim](https://github.com/romgrk/barbar.nvim)

[https://github.com/mbbill/undotree](https://github.com/mbbill/undotree)

[https://github.com/folke/which-key.nvim](https://github.com/folke/which-key.nvim)

[https://github.com/mg979/vim-visual-multi](https://github.com/mg979/vim-visual-multi)

parinfer-rust

[https://github.com/phaazon/hop.nvim](https://github.com/phaazon/hop.nvim)

[https://github.com/feline-nvim/feline.nvim](https://github.com/feline-nvim/feline.nvim)

[https://github.com/nvim-lualine/lualine.nvim](https://github.com/nvim-lualine/lualine.nvim)

<aside>
💡 Siduck suggestion

learn basic lua first then probably in this order:

1. Install packer
2. Install a colorscheme plugin
3. install treesitter and setup its config
4. setup auto completion by cmp
5. setup lsp stuff by installing lspconfig
6. Install a file picking /tree plugin like nvimtree or telescope , done!

Window-like panel

</aside>

<aside>
💡 Custom Settings (concept)

CodeJumping: g + <two-chars>  # from Amp
CodeFolding: <C>-<L|R-arrow> & [C]-Tab opening too… or <C-[> or <C-]>
CodeNavigation: HJKL (capital) & ,. or m,

</aside>

```lua
-- init.lua
-- Important Notation: ^T == <C-t> == Ctrl-t
-- Important Keybinds: (idk why I put it here)
--
-- === Default ===
-- 5|                Goto 5th character on current line
-- f+<x>             Goto next occurrence of the character x  (previous: F)
-- t+<x>             Goto character just before next occurrence of the character x (previous: T)
-- ;                 Repeat last f/t-<x> command
-- <<|>>|==          Tab in normal mode (== is smart add/reducer)
-- gg=G              Reindent whole file (gg & =G), similarly ggyG to yank file
-- "                 Access any register (+ being system clipboard); "+p and "+y being paste and copy respectively
-- viw | diw | yiw   Visual/Delete/Yank inside (a) word
-- v|V|<C-v>         Visual / LineVisual / VisualBlock mode
-- *|#               Search next/prev occurrence of currently focused word
-- <C-o>|<C-S-i>     Next/Prev location of jump, in find (/)
-- <C-d>|<C-u>       Next/Prev function (not that important)
--
-- === Custom ===
-- jk                <Esc>
-- go+<xx>           Jump Anywhere in viewable buffer (with <xx> keybind appeared)
-- gl+<x>+<y>        Jump Anywhere in viewable buffer at <x>'s occurrence (with <y> keybind appeared)
-- HJKL              Jump to start/end of line/document
-- , | .             Move 1 line up/down
-- < | >             Move 10 lines up/down
-- <C-n>             MultiSelect (visual/normal)

-- Niece Articles:
-- https://spin.atomicobject.com/2017/09/24/favorite-vim-commands
-- https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua
-- https://morioh.com/p/a7063de46490   | https://github.com/rockerBOO/awesome-neovim
-- https://vim.fandom.com/wiki/Mapping_keys_in_Vim_-_Tutorial_(Part_2)
-- https://github.com/LunarVim/Neovim-from-scratch

local fn = vim.fn

if vim.fn.has('nvim-0.7') ~= 1 then
   print('This config is only tested in nvim-0.7, please upgrade to 0.7+ to get better experience')
end

local ok, msg = pcall(require, 'plugins')
if not ok then
   print("Error loading lua/plugins.lua\n\n" .. msg)
end

-- ===== Cmd & AutoCmd =====
vim.cmd('syntax on')

vim.api.nvim_command("autocmd Filetype json setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2")
vim.api.nvim_command("autocmd Filetype lua setlocal expandtab tabstop=3 shiftwidth=3 softtabstop=3")
vim.api.nvim_command("autocmd BufWritePre * :%s/\\s\\+$//e")  -- autoremove trailing spaces
vim.api.nvim_command("autocmd BufWritePre /tmp/* setlocal noundofile")
vim.api.nvim_command("autocmd BufWritePost ~/.config/nvim/lua/plugins.lua luafile %")

vim.api.nvim_command("command! W exe '!mkdir -p %:h'|exe 'w'")  -- https://stackoverflow.com/a/44161948/11377112
vim.api.nvim_command("command! Ws lua require('sudo_write').sudowrite()")

-- ===== KeyBindings =====
vim.g.mapleader = " "

local map = vim.api.nvim_set_keymap
local mapl = vim.keymap.set

map("i", "jk", "<C-\\><C-n>", {})

mapl("n", "<leader>c" , function()     -- Toggle colorcolumn
   vim.opt.colorcolumn = vim.api.nvim_get_option_value('colorcolumn', {}) ~= '' and '' or '120'
end)

map("n", "H", "0", {})
map("v", "H", "0", {})
map("o", "H", "0", {})
map("n", "J", "G", {})
map("v", "J", "G", {})
map("o", "J", "G", {})
map("n", "K", "gg", {})
map("v", "K", "gg", {})
map("o", "K", "gg", {})
map("n", "L", "$", {})
map("v", "L", "$", {})
map("o", "L", "$", {})

map("i", "<C-h>", "<left>", {})
map("i", "<C-j>", "<down>", {})
map("i", "<C-k>", "<up>", {})
map("i", "<C-l>", "<right>", {})

map("n", ".", "<C-y>", {})
map("n", ",", "<C-e>", {})
map("n", ">", "10<C-y>", {})
map("n", "<", "10<C-e>", {})

map("n", "go", "<cmd>HopWord<CR>", {})
map("v", "go", "<cmd>HopWord<CR>", {})
map("o", "go", "<cmd>HopWord<CR>", {})

map("n", "gl", "<cmd>HopChar1<CR>", {})
map("v", "gl", "<cmd>HopChar1<CR>", {})
map("o", "gl", "<cmd>HopChar1<CR>", {})

-- https://mr-destructive.github.io/techstructive-blog/vim/comnpetitive-programming/2021/09/13/Vim-for-cp.html
map("n", "cpf", "i#include <bits/stdc++.h><Esc>ousing namespace std;<Esc>o<CR>int main() {<Esc>o<Esc>oreturn 0;<Esc>o}<Esc>kki", {})
map("n", "cp", "i#include <stdio.h><Esc>o<CR>int main() {<Esc>o<Esc>oreturn 0;<Esc>o}<Esc>kki", {})
mapl("n", "cpj" , function()
   vim.api.nvim_input("ipublic class " .. vim.fn.expand("%:t:r") .. " {<Esc>opublic static void main(String args[]) {<Esc>o<Esc>o}<Esc>o}<Esc>kki")
end)

-- ===== Extra =====
local api = vim.api
local opt = vim.opt

api.nvim_command("highlight ExtraWhitespace ctermbg=red guibg=red")
api.nvim_command("match ExtraWhitespace /\\s\\+$/")

opt.title = true
opt.confirm = true         -- Prompt to save (y/n) instead of error
opt.clipboard = "unnamedplus"
opt.laststatus = 3         -- Single statusline even at splits

opt.timeoutlen = 400       -- Overloading time for keybind (e.g. cp|cpf)
opt.updatetime = 500
opt.undofile = true
opt.mouse = "a"            -- How can anybody live without this?

opt.cursorline = true
opt.termguicolors = true
opt.background = 'dark'
opt.colorcolumn = '120'

opt.number = true
opt.numberwidth = 2
opt.relativenumber = true
opt.ruler = true
opt.signcolumn = "yes"     -- Left marginal space for git change indicator

opt.hidden = true
opt.ignorecase = true
opt.smartcase = true

opt.expandtab = true
opt.shiftwidth = 4         -- ^T (insert) or >> (normal) will produce 4 spaces
opt.tabstop = 8            -- Viewable indentation of actual Tab, <Tab> (insert) will produce 8 spaces
opt.softtabstop = 4        -- Overrides Tab/Backspace (insert) generated spaces back to 4, also makes sure Backspace deletes 4 not 1
opt.smartindent = true
vim.opt.autoindent = true

-- opt.shortmess:append "sI"  -- Hides startup page
-- opt.splitbelow = true
-- opt.splitright = true
-- opt.foldmethod = 'expr'
-- opt.foldexpr = 'nvim_treesitter#foldexpr()'
```

```lua
-- lua/plugins.lua
-- https://github.com/wbthomason/packer.nvim/issues/750#issuecomment-1130001764
local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
   PACKER_BOOTSTRAP = fn.system({'git', 'clone', '--depth=1', 'https://github.com/wbthomason/packer.nvim', install_path})
   vim.o.runtimepath = vim.fn.stdpath('data') .. '/site/pack/*/start/*,' .. vim.o.runtimepath
end

-- ===== Plugins =====
require('packer').startup(function(use)
   use 'wbthomason/packer.nvim'  -- let packer manage itself

   use {
      'nvim-treesitter/nvim-treesitter',
      event = { "BufRead", "BufNewFile" },
      run = ':TSUpdate',
      config = function()
         require('nvim-treesitter.configs').setup {
            ensure_installed = { "cpp", "lua", "rust" },
            sync_install = false,  -- async
            highlight = {
               enable = true,
               additional_vim_regex_highlighting = false,
            },
            indent = {
               enable = true
            }
         }
      end
   }
   use {
      'navarasu/onedark.nvim',
      config = function()
         local colorscheme = require('onedark')
         colorscheme.setup { style = 'darker' }
         colorscheme.load()
      end
   }
   use {
      "akinsho/bufferline.nvim",
      config = function()
         require("bufferline").setup {
            -- options = {
            --    offsets = { { filetype = "NvimTree", text = "", padding = 1 } },
            --    show_tab_indicators = true,
            --    enforce_regular_tabs = false,
            --    view = "multiwindow",
            --    separator_style = "thin",
            --    always_show_bufferline = true,
            --    diagnostics = false,
            --    themable = true,
            -- }
         }
      end
   }
   use "kyazdani42/nvim-web-devicons"
   use {
      "feline-nvim/feline.nvim",
      config = function()
         vim.api.nvim_command("highlight FelineIcon guifg=#22262e guibg=#81a1c1")
         vim.api.nvim_command("highlight FelineIconSeparator guifg=#81a1c1 guibg=#2d3139")
         vim.api.nvim_command("highlight FelineDirName guifg=#abb2bf guibg=#2d3139")
         vim.api.nvim_command("highlight FelineDirNameSeparator guifg=#2d3139 guibg=#22262e")
         vim.api.nvim_command("highlight FelinePositionIcon guifg=#1e222a guibg=#a3be8c")
         vim.api.nvim_command("highlight FelineCurrentLine guifg=#a3be8c guibg=#282c34")
         vim.api.nvim_command("highlight FelinePositionSeparator guifg=#a3be8c guibg=#42464e")
         vim.api.nvim_command("highlight FelineEmptySpace guifg=#353b45 guibg=#22262e")

         icons = {
            left = "",
            right = " ",
            main_icon = "  ",
            vi_mode_icon = " ",
            position_icon = " ",
         }
         mode_hlgroups = {
            ["n"] = { "NORMAL", "Feline_NormalMode" },
            ["no"] = { "N-PENDING", "Feline_NormalMode" },
            ["i"] = { "INSERT", "Feline_InsertMode" },
            ["ic"] = { "INSERT", "Feline_InsertMode" },
            ["t"] = { "TERMINAL", "Feline_TerminalMode" },
            ["v"] = { "VISUAL", "Feline_VisualMode" },
            ["V"] = { "V-LINE", "Feline_VisualMode" },
            [""] = { "V-BLOCK", "Feline_VisualMode" },
            ["R"] = { "REPLACE", "Feline_ReplaceMode" },
            ["Rv"] = { "V-REPLACE", "Feline_ReplaceMode" },
            ["s"] = { "SELECT", "Feline_SelectMode" },
            ["S"] = { "S-LINE", "Feline_SelectMode" },
            [""] = { "S-BLOCK", "Feline_SelectMode" },
            ["c"] = { "COMMAND", "Feline_CommandMode" },
            ["cv"] = { "COMMAND", "Feline_CommandMode" },
            ["ce"] = { "COMMAND", "Feline_CommandMode" },
            ["r"] = { "PROMPT", "Feline_ConfirmMode" },
            ["rm"] = { "MORE", "Feline_ConfirmMode" },
            ["r?"] = { "CONFIRM", "Feline_ConfirmMode" },
            ["!"] = { "SHELL", "Feline_TerminalMode" },
         }
         require('feline').setup {
            components = {
               active = {
                  {
                     {  -- main_icon
                        hl = "FelineIcon",
                        provider = function()
                           return icons.main_icon .. mode_hlgroups[vim.fn.mode()][1] .. " "
                        end,
                        right_sep = {
                           str = icons.right,
                           hl = "FelineIconSeparator",
                        },
                     },
                     {  -- dir_name
                        hl = "FelineDirName",
                        provider = function()
                           local dir_name = vim.fn.fnamemodify(vim.fn.getcwd(), ":t")
                           if dir_name == '' then dir_name = '/' end
                           return "  " .. dir_name .. " "
                        end,
                        right_sep = {
                           str = "",
                           hl = "FelineDirNameSeparator",
                        },
                     },

                  },
                  {
                     {  -- git branch
                        provider = "git_branch",
                        hl = {
                           fg = "#6f737b",
                           bg = "#22262e",
                        },
                        icon = "  ",
                     },
                     {  -- position_logo_prefix
                        provider = icons.left,
                        hl = "FelineEmptySpace",
                     },
                     {  -- position_logo
                        provider = icons.left,
                        hl = "FelinePositionSeparator",
                     },
                     {  -- position_icon
                        provider = icons.position_icon,
                        hl = "FelinePositionIcon",
                     },
                     {  -- position_current_line
                        provider = function()
                           local current_line = vim.fn.line "."
                           local total_line = vim.fn.line "$"

                           if current_line == 1 then
                              return " Top "
                           elseif current_line == vim.fn.line "$" then
                              return " Bot "
                           end
                           local result, _ = math.modf((current_line / total_line) * 100)
                           return " " .. result .. "%% "
                        end,

                        hl = "FelineCurrentLine",
                     },
                  }
               }
            }
         }
      end
   }
   use {
      "lukas-reineke/indent-blankline.nvim",
      event = "BufRead",
      config = function()
         vim.opt.list = true
         vim.opt.listchars:append("trail: ")
         require('indent_blankline').setup {
            indentLine_enabled = 1,
            char = "▏",
            show_trailing_blankline_indent = true,
            show_first_indent_level = false,
            filetype_exclude = {
               "help",
               "terminal",
               "alpha",
               "packer",
               "lspinfo",
               "TelescopePrompt",
               "TelescopeResults",
               "nvchad_cheatsheet",
               "lsp-installer",
               "",
            },
            buftype_exclude = { "terminal" },
            show_current_context = true,
            show_current_context_start = true,
         }
      end
   }
   use {
      'phaazon/hop.nvim',
      config = function()
         require('hop').setup()
      end
   }
   use 'mg979/vim-visual-multi'
   -- require("luasnip.loaders.from_snipmate").load({ paths = { "./snippets" } })
   -- TODO: refer more https://github.com/xaerru/dots/blob/main/NVelox/.config/nvlx/init.lua
   -- TODO: refer more https://github.com/savq/dotfiles/blob/master/nvim/init.lua

   if PACKER_BOOTSTRAP then  -- first time run hook
      require('packer').sync()
   end
end)
```

[Lua solution to writing a file using sudo](https://www.reddit.com/r/neovim/comments/p3b20j/lua_solution_to_writing_a_file_using_sudo/)

[sudowrite.lua: I wrote a small module that allows writing files as root](https://www.reddit.com/r/neovim/comments/uqhql7/sudowritelua_i_wrote_a_small_module_that_allows/)

[sudowrite.lua](https://gist.github.com/oessessnex/d63ebe89380abff5a3ee70d6e76e4ec8)

```lua
-- Highly inspired by: https://gist.github.com/oessessnex/d63ebe89380abff5a3ee70d6e76e4ec8

local M = {}

local uv = vim.loop
local fn = vim.fn
local api = vim.api

function password()
   fn.inputsave()
   local user = fn.expand("$USER")
   local pw = fn.inputsecret(string.format("password for %s: ", user))
   fn.inputrestore()
   return pw
end

function test(pw, k)
   local stdin = uv.new_pipe()
   uv.spawn("sudo", {
      args = {"-S", "-k", "true"},
      stdio = {stdin, nil, nil}
   }, k)

   stdin:write(pw)
   stdin:write("\n")
   stdin:shutdown()
end

function dir(str,sep) -- https://stackoverflow.com/a/9102300/11377112
   sep=sep or'/'
   return str:match("(.*"..sep..")")
end

function write(pw, buf, lines, k)
   local stdin = uv.new_pipe()
   uv.spawn("sudo", {
      -- args = {"-S", "-k", "tee", buf},
      args = {"-S", "-k", "sh", "-c", "mkdir -p " .. dir(buf) .. " && tee " .. buf},
      stdio = {stdin, nil, nil}
   }, k)

   local ok, posix = pcall(require, 'posix.unistd')
   if not ok then
      error("Error loading luaposix library\n\n" .. posix)
   end

   if posix.geteuid() ~= 0 then
      stdin:write(pw)
      stdin:write("\n")
   end
   local last = table.remove(lines)
   for _, line in ipairs(lines) do
      stdin:write(line)
      stdin:write("\n")
   end
   stdin:write(last)
   stdin:shutdown()
end

function M.sudowrite()
   local pw = password()
   local buf = api.nvim_buf_get_name(0)
   local bufnr = api.nvim_get_current_buf()
   local lines = api.nvim_buf_get_lines(bufnr, 0, -1, false)

   local function exitWrite(code, _)
      if code == 0 then
         vim.schedule(function()
            api.nvim_echo({{string.format('"%s" written', buf), "Normal"}}, true, {})
            api.nvim_buf_set_option(0, "modified", false)
         end)
      end
   end
   local function exitTest(code, _)
      if code == 0 then
         write(pw, buf, lines, exitWrite)
      else
         vim.schedule(function()
            api.nvim_echo({{"incorrect password", "ErrorMsg"}}, true, {})
         end)
      end
   end
   test(pw, exitTest)
end

return M
```

[luaposix 35.1](https://luaposix.github.io/luaposix/modules/posix.unistd.html)

[NvChad/statusline.lua at main · NvChad/NvChad](https://github.com/NvChad/NvChad/blob/main/lua/plugins/configs/statusline.lua)

[base46/feline.lua at master · NvChad/base46](https://github.com/NvChad/base46/blob/master/lua/integrations/feline.lua)

Sample packer

[dotfiles/packerInit.lua at main · emuel-vassallo/dotfiles](https://github.com/emuel-vassallo/dotfiles/blob/main/.config/nvim/lua/packerInit.lua)

[NeoVim Plugins You Don't Want To Miss](https://www.youtube.com/watch?v=1f7l2-Fap2s)

![c25-05-22-22h49m33s.jpg](Text%20Editors%20Explore%20aeb98269545a44298ba8b98a17118bf0/c25-05-22-22h49m33s.jpg)

[](https://alpha2phi.medium.com/neovim-for-beginners-snippets-450a855584f8)

Neovim top and bottom bar

[[DWM] [NEOVIM] I really like looking at diagnostics now.](https://www.reddit.com/r/unixporn/comments/s2l4vy/dwm_neovim_i_really_like_looking_at_diagnostics/)

[https://github.com/linty-org/key-menu.nvim](https://github.com/linty-org/key-menu.nvim)