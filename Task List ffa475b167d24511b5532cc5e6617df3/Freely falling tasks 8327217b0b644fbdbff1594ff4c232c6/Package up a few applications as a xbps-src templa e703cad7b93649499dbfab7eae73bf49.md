# Package up a few applications as a xbps-src template (void-packages)

Done: No
Duration: Day
Priority: Not Important & Urgent
Project Source: https://github.com/void-linux/void-packages
Tags: Needs to be done
Type: Project

An essence of package management

[Main Repo](https://github.com/void-linux/void-packages) - Strict quality standards like package must be versioned

[Personal Repo](https://github.com/Animeshz/void-xpackages) - LoL :p

- [x]  brave-bin (fork from ElPresidentePoole)
- [x]  rofi-blocks (tag: https://github.com/OmarCastro/rofi-blocks/issues/25)
- [x]  eww (tag: [https://github.com/elkowar/eww/discussions/383](https://github.com/elkowar/eww/discussions/383))
- [ ]  [Emote](https://github.com/tom-james-watson/Emote) - already I’ve sent a PR?
- [ ]  [hyprland](https://github.com/void-linux/void-packages/issues/37544)
- [ ]  [bwmenu](https://github.com/mattydebie/bitwarden-rofi)
- [ ]  [Ignite & FirecrackerMicroVM](https://github.com/void-linux/void-packages/issues/37638)

- Stale/Archive
    - [x]  JB-Toolbox (Manual Install, but maybe automated todo?, cuz autoupdate functionality)
    - [ ]  Kiwix/LibZim
    - [ ]  freedownloadmanager (with brave-patch: https://github.com/brave/brave-browser/issues/12202)
    - [ ]  VMWare (low-priority, qemu/quickemu alternative, mainly for cross-platform but low performance)