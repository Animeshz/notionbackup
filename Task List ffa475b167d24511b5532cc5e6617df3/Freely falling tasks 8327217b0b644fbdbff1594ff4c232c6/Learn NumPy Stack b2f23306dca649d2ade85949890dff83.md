# Learn NumPy Stack

Done: Yes
Duration: Week
Priority: Important & Urgent
Tags: Needs to be done

[Installing Python Packages from a Jupyter Notebook](https://jakevdp.github.io/blog/2017/12/05/installing-python-packages-from-jupyter/)

[](https://www.udemy.com/course/numpy-python/learn/lecture/19643272#overview)

[Deep Learning Prerequisites: The Numpy Stack in Python Extra Resources - Lazy Programmer](http://lazyprogrammer.me/numpy)