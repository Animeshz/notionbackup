# Parallel fingerprint OR password auth for sudo (also probably a OR security key usb/pen-drive too)

Done: Yes
Duration: Week
Tags: Stale

[fprint](https://wiki.archlinux.org/title/fprint#Login_configuration)

[Commits · grosshack · mishakmak / pam-fprint-grosshack](https://gitlab.com/mishakmak/pam-fprint-grosshack/-/commits/grosshack/pam)

[Commits · master · libfprint / fprintd](https://gitlab.freedesktop.org/libfprint/fprintd/-/commits/master/pam/pam_fprintd.c)

Useful for creating usb lock:

[https://github.com/LuisRusso/PamUUID](https://github.com/LuisRusso/PamUUID)