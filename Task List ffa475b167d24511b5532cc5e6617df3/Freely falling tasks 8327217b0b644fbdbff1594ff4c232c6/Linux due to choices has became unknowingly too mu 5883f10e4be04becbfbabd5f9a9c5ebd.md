# Linux due to choices has became unknowingly too much complex, make sth that can build install configure to what we need by just interacting with prompt (see more)

Done: No
Duration: Month
Priority: Important & Not Urgent
Tags: Needs to be done
Type: Project

Hybrid of Cluster+ (my old project, to create scripting modules at runtime and hook it) & linux modules.

**E.g:**

- Setup a printer
- Power saving
- Setup a dual screen
- And those android top-panel buttons regardless of a WM/DE/Distro

References (similar tools):

[https://github.com/tldr-pages/tldr](https://github.com/tldr-pages/tldr)

[https://github.com/dbrgn/tealdeer](https://github.com/dbrgn/tealdeer)

Multiple Apps integration layer based on above two concepts