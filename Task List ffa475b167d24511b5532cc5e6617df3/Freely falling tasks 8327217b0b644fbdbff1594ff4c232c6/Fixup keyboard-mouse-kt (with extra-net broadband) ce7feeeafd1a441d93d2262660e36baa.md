# Fixup keyboard-mouse-kt (with extra-net/broadband)

Done: No
Duration: Month
Priority: Not Important & Urgent
Project Source: https://github.com/Animeshz/keyboard-mouse-kt
Tags: Stale
Type: Project

[Move to MavenCentral/Sonatype as Bintray has been discontinued. · Issue #20 · Animeshz/keyboard-mouse-kt](https://github.com/Animeshz/keyboard-mouse-kt/issues/20#issuecomment-839493203)

MavenCentral/Sonatype upload

## Roadmap

common native naming, then native mouse stuffs, then finally build n docker stuffs

- **Mouse Native Stuffs**
    
    [https://github.com/boppreh/mouse/blob/master/mouse/_nixcommon.py](https://github.com/boppreh/mouse/blob/master/mouse/_nixcommon.py)
    
    [https://github.com/boppreh/mouse/blob/master/mouse/_nixmouse.py](https://github.com/boppreh/mouse/blob/master/mouse/_nixmouse.py)
    
    Windows Listening: [https://github.com/xanderfrangos/global-mouse-events/blob/master/global-mouse-events.cc](https://github.com/xanderfrangos/global-mouse-events/blob/master/global-mouse-events.cc)
    

### proposal of structure, src/native/<platform-native/jvm/js>/* all cpp/kt files which is used in interfacing, rather than jvmMain, mingwMain etc.

### Remain in C++ due to jni complexity as well as it can’t be header only in K/N as we need to store the instance in a variable

maybe we can have static function with static variable, but, anyways 😛

Or we can move to rust lately easily, rust-dlopen is great... But rust comes with its own problems, ~<300kb size, compared to ~<15kb, with the std.

### Publish MPP

[https://dev.to/kotlin/how-to-build-and-publish-a-kotlin-multiplatform-library-going-public-4a8k](https://dev.to/kotlin/how-to-build-and-publish-a-kotlin-multiplatform-library-going-public-4a8k)

[https://kotlinlang.org/docs/multiplatform-publish-lib.html#avoid-duplicate-publications](https://kotlinlang.org/docs/multiplatform-publish-lib.html#avoid-duplicate-publications)

[https://medium.com/kodein-koders/publish-a-kotlin-multiplatform-library-on-maven-central-6e8a394b7030](https://medium.com/kodein-koders/publish-a-kotlin-multiplatform-library-on-maven-central-6e8a394b7030)

[https://stackoverflow.com/questions/66329999/publish-kotlin-multiplatform-library-to-maven-central-invalidmavenpublicationex](https://stackoverflow.com/questions/66329999/publish-kotlin-multiplatform-library-to-maven-central-invalidmavenpublicationex)

### Fix naming errors:

> Task :keyboard-mouse-kt:compileCommonMainKotlinMetadata FAILED
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Keyboard.kt: (30, 35): Unresolved reference: registerKb
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Keyboard.kt: (30, 48): Cannot infer a type for this parameter. Please specify it explicitly.
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Keyboard.kt: (30, 57): Cannot infer a type for this parameter. Please specify it explicitly.
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Keyboard.kt: (48, 35): Unresolved reference: unregisterKb
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Mouse.kt: (37, 35): Unresolved reference: registerKb
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Mouse.kt: (37, 48): Cannot infer a type for this parameter. Please specify it explicitly.
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Mouse.kt: (37, 57): Cannot infer a type for this parameter. Please specify it explicitly.
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Mouse.kt: (39, 38): Cannot access 'handlers': it is private in 'Keyboard'
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Mouse.kt: (55, 35): Unresolved reference: unregisterKb
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Mouse.kt: (64, 85): Unresolved reference: key
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Mouse.kt: (64, 98): Unresolved reference: isPressed
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Mouse.kt: (71, 45): Unresolved reference: ButtonState
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Mouse.kt: (71, 91): Unresolved reference: key
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Mouse.kt: (76, 42): Type mismatch: inferred type is Boolean but Position was expected
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/Mouse.kt: (76, 74): Unresolved reference: key
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/MouseExt.kt: (6, 5): Function 'recordTill' without a body must be abstract
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/MouseExt.kt: (6, 30): Unresolved reference: Finalizer
e: /home/animesh/Projects/KotlinProjects/keyboard-mouse-kt/keyboard-mouse-kt/src/commonMain/kotlin/io/github/animeshz/kbms/MouseExt.kt: (7, 5): Function 'play' without a body must be abstract
> 

[Move to MavenCentral/Sonatype as Bintray has been discontinued. · Issue #20 · Animeshz/keyboard-mouse-kt](https://github.com/Animeshz/keyboard-mouse-kt/issues/20#issuecomment-839493203)

[New project request for keyboard-mouse-kt library](https://issues.sonatype.org/browse/OSSRH-68605)