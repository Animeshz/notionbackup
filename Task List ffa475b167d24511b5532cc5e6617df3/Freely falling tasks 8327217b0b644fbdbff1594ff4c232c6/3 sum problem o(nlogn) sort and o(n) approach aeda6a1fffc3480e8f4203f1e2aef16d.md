# 3 sum problem o(nlogn) sort and o(n) approach

Done: Yes
Duration: Day
Tags: Needs to be done

[3SUM - Wikipedia](https://en.m.wikipedia.org/wiki/3SUM)

Actually O(n^2) is the minimum complexity we can reach at the moment