# HotSwapping concept (read more)

Done: No
Duration: Week
Priority: Important & Not Urgent
Tags: Needs to be done
Type: Project

- Nix-Like configuration/bootstrap
- Make WM setup more like pluggable like a DE
- Hotswapping with GUI with settings panel composed of scripting language like lua/nix/rhai per application config | OR | text-based config with herbstclient hybrid - shell to test before writing/committing.
- Hotswapping CLI - [swim](https://github.com/dawsbot/swim) inspired with [enquirer](https://github.com/termapps/enquirer) like frontend