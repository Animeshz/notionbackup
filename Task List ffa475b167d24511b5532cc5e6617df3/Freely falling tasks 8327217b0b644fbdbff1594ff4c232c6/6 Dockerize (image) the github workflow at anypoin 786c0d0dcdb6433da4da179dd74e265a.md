# 6. Dockerize (image) the github workflow at anypoint it fails (uses & if failed())

Done: No
Duration: Day
Priority: Important & Urgent
Tags: Archived, Needs to be done

At start so to intercept other steps, refer:  checkout exit hook

Make a checkpoint commit at every step end (incl env variables local to shell at that point)

Maybe just statically add a commit hook at end of each step (dynamically on use of action)