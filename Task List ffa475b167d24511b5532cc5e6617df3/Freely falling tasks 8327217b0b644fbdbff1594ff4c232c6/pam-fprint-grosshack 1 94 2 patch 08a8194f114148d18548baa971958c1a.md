# pam-fprint-grosshack 1.94.2 patch

Done: Yes
Duration: Quick
Priority: Not Important & Urgent
Tags: Needs to be done

```diff
diff --git a/AUTHORS b/AUTHORS
index 935f5b4..22916c2 100644
--- a/AUTHORS
+++ b/AUTHORS
@@ -1,3 +1,4 @@
 Daniel Drake <dsd@gentoo.org>
 Bastien Nocera <hadess@hadess.net>
+Misha <mishakmak@gmail.com>
 
diff --git a/README b/README
index dd81c64..2c9af42 100644
--- a/README
+++ b/README
@@ -1,3 +1,32 @@
+This is a fork of the pam module which implements the simultaneous
+password and fingerprint behaviour present in pam_thinkfinger. It
+was called a 'gross hack' by the fprintd developers in their README
+for the PAM module, but it works, and I am not beneath using it.
+
+Compilation uses meson/ninja. Configure with "meson-build . build"
+and run "meson compile -C build" to compile. To install, move the
+generated pam_fprintd_grosshack.so to the correct directory on the
+system. Alternatively, Arch users can install pam-fprint-grosshack
+from the AUR.
+
+To use this module, add the following lines to the appropriate files
+in /etc/pam.d/ (i.e. /etc/pam.d/system-auth for any use):
+
+auth    sufficient    pam_fprintd_grosshack.so
+auth    sufficient    pam_unix.so try_first_pass nullok
+
+Warning! The original developers called this implementation a gross
+hack for a reason. This code may have major security flaws or other
+unforeseen bugs, so use with caution. And, of course, the standard
+legalese applies: No warranty, provided as-is, whatever you do or
+whatever happens to you is not my fault, etc.
+
+-- Misha
+
+-----------------------------------------------------------
+Original README
+-----------------------------------------------------------
+
 fprintd
 =======
 
diff --git a/meson.build b/meson.build
index 159f0e8..9062a0a 100644
--- a/meson.build
+++ b/meson.build
@@ -97,6 +97,7 @@ pam_dep = cc.find_library('pam',
     required: get_option('pam'),
     has_headers: 'security/pam_modules.h',
 )
+pthread_dep = dependency('threads')
 
 pod2man = find_program('pod2man', required: get_option('man'))
 xsltproc = find_program('xsltproc', required: get_option('gtk_doc'))
@@ -170,17 +171,17 @@ config_h = configure_file(
   configuration: cdata
 )
 
-subdir('src')
-subdir('data')
-subdir('utils')
+#subdir('src')
+#subdir('data')
+#subdir('utils')
 if get_option('pam')
     subdir('pam')
 endif
-if get_option('gtk_doc')
-    subdir('doc')
-endif
-subdir('tests')
-subdir('po')
+#if get_option('gtk_doc')
+#    subdir('doc')
+#endif
+#subdir('tests')
+#subdir('po')
 
 output = []
 output += 'System paths:'
@@ -202,6 +203,6 @@ output += '  Manuals: ' + get_option('man').to_string()
 output += '  GTK Doc: ' + get_option('gtk_doc').to_string()
 output += '  XML Linter ' + xmllint.found().to_string()
 output += '\nTest setup:\n'
-output += '  With address sanitizer: ' + address_sanitizer.to_string()
+#output += '  With address sanitizer: ' + address_sanitizer.to_string()
 
 message('\n'+'\n'.join(output)+'\n')
diff --git a/meson_options.txt b/meson_options.txt
index 5daa9a4..226d580 100644
--- a/meson_options.txt
+++ b/meson_options.txt
@@ -5,11 +5,11 @@ option('pam',
 option('man',
     description: 'Generate the man files',
     type: 'boolean',
-    value: true)
+    value: false)
 option('systemd',
     description: 'Install system service files',
     type: 'boolean',
-    value: true)
+    value: false)
 option('systemd_system_unit_dir',
     description: 'Directory for systemd service files',
     type: 'string')
diff --git a/pam/meson.build b/pam/meson.build
index e95bcde..42a0282 100644
--- a/pam/meson.build
+++ b/pam/meson.build
@@ -4,7 +4,7 @@ if pam_modules_dir == ''
     pam_modules_dir = '/' / get_option('libdir') / 'security'
 endif
 
-pam_fprintd = shared_module('pam_fprintd',
+pam_fprintd = shared_module('pam_fprintd_grosshack',
     name_prefix: '',
     include_directories: [
         include_directories('..'),
@@ -16,6 +16,7 @@ pam_fprintd = shared_module('pam_fprintd',
     dependencies: [
         libsystemd_dep,
         pam_dep,
+        pthread_dep,
     ],
     c_args: [
         '-DLOCALEDIR="@0@"'.format(localedir),
diff --git a/pam/pam_fprintd.c b/pam/pam_fprintd.c
index 150872a..3978bce 100644
--- a/pam/pam_fprintd.c
+++ b/pam/pam_fprintd.c
@@ -19,6 +19,7 @@
  */
 
 #include <config.h>
+#include <security/_pam_types.h>
 
 #define _GNU_SOURCE
 #include <limits.h>
@@ -31,6 +32,8 @@
 #include <syslog.h>
 #include <errno.h>
 
+#include <pthread.h>
+
 #include <libintl.h>
 #include <systemd/sd-bus.h>
 #include <systemd/sd-login.h>
@@ -195,6 +198,9 @@ typedef struct
   pam_handle_t *pamh;
 
   char         *driver;
+
+  bool          stop_got_pw;
+  pid_t         ppid;
 } verify_data;
 
 static void
@@ -297,8 +303,7 @@ verify_finger_selected (sd_bus_message *m,
     }
   if (debug)
     pam_syslog (data->pamh, LOG_DEBUG, "verify_finger_selected %s", msg);
-  send_info_msg (data->pamh, msg);
-
+  //send_info_msg (data->pamh, msg);
   return 0;
 }
 
@@ -383,9 +388,15 @@ fd_cleanup (int *fd)
 typedef int fd_int;
 PF_DEFINE_AUTO_CLEAN_FUNC (fd_int, fd_cleanup);
 
+static bool has_recieved_sigusr1 = false;
+static void
+handle_sigusr1 (int sig)
+{
+    has_recieved_sigusr1 = true;
+}
+
 static int
-do_verify (sd_bus      *bus,
-           verify_data *data)
+do_verify (sd_bus *bus, verify_data *data)
 {
   pf_autoptr (sd_bus_slot) verify_status_slot = NULL;
   pf_autoptr (sd_bus_slot) verify_finger_selected_slot = NULL;
@@ -444,6 +455,8 @@ do_verify (sd_bus      *bus,
 
   sigemptyset (&signals);
   sigaddset (&signals, SIGINT);
+  signal (SIGUSR1, handle_sigusr1);
+  sigaddset (&signals, SIGUSR1);
   signal_fd = signalfd (signal_fd, &signals, SFD_NONBLOCK);
 
   while (data->max_tries > 0)
@@ -487,7 +500,7 @@ do_verify (sd_bus      *bus,
           int64_t wait_time;
 
           wait_time = verification_end - now ();
-          if (wait_time <= 0)
+          if (wait_time <= 0 || data->stop_got_pw)
             break;
 
           if (read (signal_fd, &siginfo, sizeof (siginfo)) > 0)
@@ -529,6 +542,12 @@ do_verify (sd_bus      *bus,
                   pam_syslog (data->pamh, LOG_ERR, "Error waiting for events: %d", errno);
                   return PAM_AUTHINFO_UNAVAIL;
                 }
+              if (has_recieved_sigusr1)
+                {
+                  if (debug)
+                      pam_syslog (data->pamh, LOG_DEBUG, "Got SIGUSR1: assuming pw recieved");
+                  return PAM_AUTHINFO_UNAVAIL;
+                }
             }
         }
 
@@ -561,7 +580,7 @@ do_verify (sd_bus      *bus,
                                  NULL,
                                  NULL);
 
-      if (data->timed_out)
+      if (data->timed_out || data->stop_got_pw)
         {
           return PAM_AUTHINFO_UNAVAIL;
         }
@@ -714,6 +733,20 @@ name_owner_changed (sd_bus_message *m,
   return 0;
 }
 
+static void
+prompt_pw (void *d)
+{
+  verify_data *data = d;
+  char *pw;
+  pam_prompt (data->pamh, PAM_PROMPT_ECHO_OFF, &pw, "Enter Password or Place finger on fingerprint reader: ");
+  pam_set_item (data->pamh, PAM_AUTHTOK, pw);
+  data->stop_got_pw = true;
+  if (debug)
+    pam_syslog (data->pamh, LOG_DEBUG, "PW received, Should be killing parent");
+  kill(data->ppid, SIGUSR1);
+  return;
+}
+
 static int
 do_auth (pam_handle_t *pamh, const char *username)
 {
@@ -750,7 +783,16 @@ do_auth (pam_handle_t *pamh, const char *username)
 
   if (claim_device (pamh, bus, data->dev, username))
     {
-      int ret = do_verify (bus, data);
+      data->stop_got_pw = false;
+      data->ppid = getpid();
+
+      pthread_t pw_prompt_thread;
+      if (pthread_create (&pw_prompt_thread, NULL, (void*) &prompt_pw, data) != 0)
+        send_err_msg (pamh, _("Failed to create thread"));
+
+      int ret = do_verify(bus, data);
+      pthread_cancel (pw_prompt_thread);
+      pam_prompt(data->pamh, PAM_TEXT_INFO, NULL, "***");
 
       /* Simply disconnect from bus if we return PAM_SUCCESS */
       if (ret != PAM_SUCCESS)
```

Unchecked diff

```diff
@@ -297,8 +302,7 @@ verify_finger_selected (sd_bus_message *m,
     }
   if (debug)
     pam_syslog (data->pamh, LOG_DEBUG, "verify_finger_selected %s", msg);
-  send_info_msg (data->pamh, msg);
-
+  //send_info_msg (data->pamh, msg);
   return 0;
 }

// Later may need to put newline
@@ -714,6 +729,20 @@ name_owner_changed (sd_bus_message *m,
   return 0;
 }

+static void
+prompt_pw (void *d)
+{
+  verify_data *data = d;
+  char *pw;
+  pam_prompt (data->pamh, PAM_PROMPT_ECHO_OFF, &pw, "Enter Password or Place finger on fingerprint reader: ");
+  pam_set_item (data->pamh, PAM_AUTHTOK, pw);
+  data->stop_got_pw = true;
+  if (debug)
+    pam_syslog (data->pamh, LOG_DEBUG, "PW received, Should be killing parent");
+  kill(data->ppid, SIGUSR1);
+  return;
+}
+
```