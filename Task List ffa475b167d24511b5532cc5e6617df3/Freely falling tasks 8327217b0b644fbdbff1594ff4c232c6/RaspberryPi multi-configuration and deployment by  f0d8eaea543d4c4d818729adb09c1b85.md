# RaspberryPi: multi-configuration and deployment by choosing at runtime (web/ssh) and reboot (not nixos anymore, also track pmm brl 0.8) + <Merged Idea> Headless Tools (create application/package) for distro-free headless setup of IoT/RPi

Done: No
Duration: Month
Priority: Important & Urgent
Project Source: (not started)
Tags: Needs to be done
Type: Project

<aside>
💡 Bedrock like subsystem for docker imgs, kernel + hijacked init + moby based filesystem access single container run (like docker can occupy full space remaining in /) + bedrock/btrfs like file mount + bedrock like common file/binaries for switching container on next run. With ability to push docker images over web/lan with optional parameter to make it default on next boot. Fallback boot accepting pushes in case default selected container goes wrong.

</aside>

**(Web) UI Diagram:**

[Flowchart Maker & Online Diagram Software](https://viewer.diagrams.net/?tags=%7B%7D&highlight=0000ff&edit=_blank&layers=1&nav=1&title=Docker-Bedrock-Hybrid#RzZbfb9MwEMf%2FmkrwAMqPNiqPa9cypCHQOsR4Qia5JQbHFzlOk%2FDXc26cpCFZYaLTeKrvez4797mz65m%2FTqu3imXJe4xAzDwnqmb%2B5czzXMcJ6McodaMEwaIRYsUjO6kXdvwntJFWLXgE%2BWCiRhSaZ0MxRCkh1AONKYXlcNo9iuGuGYthJOxCJsbqZx7ppFGXC6fXr4DHSbszZdx4UtZOtkKesAjLI8nfzPy1QtTNKK3WIAy8lksTt33A232YAqn%2FJmDjfsxZmcvqbsVuv3L4dAvZK7vKnonCJvxhRzbJgaBVV98UjWIzenFTSMll%2FNJmo%2BsWkcJCRmB2cWlemXANu4yFxltSU5CW6FRYd64V%2FoA1ClSHaD8IttsgII%2F9ElAaqgdTdDtw1HGAKWhV05Q2oC2LbTa3ZV%2F2peu05KhsgdWY7Za4W7oHSgPL9BF8vWm%2B3r9BfAJUnvPcqPxpVP5EK14h5X7uRtxuLxYE4WnoLp%2Bb7nJECyK66KyJSicYo2Ri06urnqfB0s%2B5Rswsxe%2BgdW1vbVZoHDKGiuu7o%2FEXs9TrhbUuK7vywahbQ1K6XZAxjqKM2YcdrDYuLNS%2Bq32TrMnwdAEJCBYqhBPg5vZPh6kY9J%2Fad9wQCgTTfD%2F8jrNXdz59dub%2F3TXjB899EN6MUN1AkQNJ71LzFvCca1aDykfoCII%2BdYVIlObQ3HMhfpOY4LEkMyRoQPrKIOX0xriwjpRH0eHETRVkeArPUJPuMmpr4k%2FUxJ2oif%2F4mpDZv3AOvqN3or%2F5BQ%3D%3D)

### Bedrock

Build commands:

[BedrockBuild](https://gist.github.com/Animeshz/6ac975d177a48c7d0abb1b6229dd07fb)

Words from maintainer (base architecture):

[ParadigmComplex's comment from discussion "I'm the founder and lead developer of Bedrock Linux. We just released 0.7 Poki. AMA."](https://www.reddit.com/r/linux/comments/a724qe/comment/ebzr90m)

- **What does Bedrock use?** (read)
    - overlayfs
    - subvol (btrfs-like)
    - tmpfs overlay
    - balenaos architecture front page
    - moby implementation
- Notes
    
    ```jsx
    How bedrock linux works entirely 
      dash -xc 
    
    Hybrid Naming:
      HighbreedLinux (hybrid of bedrock and docker, also a fancy name from ben 10 universe symbolizing strength & durability)
      HyberOS or HyperLinux (hybrid hipervisor bedrock)
      RottenCore Linux
      FreshCore Linux
      HeadlessCore (best from me i guess :p)
    
    Make runit primary init, and distro init as subinit runit or subinit systemd from there
    
    Bedrock
      - crossfs binary merge/mount on guest
      - control host from guest, access all hardware, init work as it was running as pid 1
    
    Docker
      - image reuse on commit/step (by hash and overlays)
    
    Balena: 140M
    Void: 70M
    Bedrock (meta): 4M (needs other distro)
    ```
    

### BalenaOS

[What is balenaOS? - Balena Documentation](https://www.balena.io/docs/reference/OS/overview)

[Moby](https://mobyproject.org/)

- **How does docker save space/steps while rebuilding for small change in below instruction or running multiple containers?**
    
    [How are docker images built? A look into the Linux overlay file-systems and the OCI specification](https://dev.to/napicella/how-are-docker-images-built-a-look-into-the-linux-overlay-file-systems-and-the-oci-specification-175n)
    
- **YoctoProject**
    
    [Software - Yocto Project](https://www.yoctoproject.org/software-overview/)
    
    [https://jumpnowtek.com/rpi/Raspberry-Pi-4-64bit-Systems-with-Yocto.html](https://jumpnowtek.com/rpi/Raspberry-Pi-4-64bit-Systems-with-Yocto.html)
    
    [https://medium.com/nerd-for-tech/build-your-own-linux-image-for-the-raspberry-pi-f61adb799652](https://medium.com/nerd-for-tech/build-your-own-linux-image-for-the-raspberry-pi-f61adb799652)
    
    [https://himvis.com/bake-64-bit-raspberrypi3-images-with-yoctoopenembedded/](https://himvis.com/bake-64-bit-raspberrypi3-images-with-yoctoopenembedded/)
    

### First filter out if these help

AWS Firecracker MicroVM

QubesOS

even if not, UI is inspiring, specially oVirt & ProxMox

[8 Free & Best Open source bare metal hypervisors (Foss) 2021](https://www.how2shout.com/tools/8-free-best-open-source-bare-metal-hypervisors-foss.html)

- [Alternatively Try to check Docker’s --privileged](https://stackoverflow.com/questions/36425230/privileged-containers-and-capabilities)

[Communicate outside the container - Balena Documentation](https://www.balena.io/docs/learn/develop/runtime/)

> It also just defeats the purpose of container isolation, but it does have some use cases. Running as privileged is the same as running a process on the host machine, except you do get the organizational benefits of being able to run in a container. If you’re running processes that need to directly access hardware on the machine, like network devices, or need to have read/write access to the root partition, you can run them as privileged to allow for this. Another major use case is [Docker in Docker](https://www.docker.com/blog/docker-can-now-run-within-docker/#:~:text=One%20of%20the%20(many!),kernel%20features%20and%20device%20access.), or DinD, which is used by Jenkins to allow for building Docker containers inside the Jenkins container.
> 

# Misc

Raspverry pi multi app/image/tempfs-overlay deployer, choose restart over web or static connection without dynamic ip (static connection anytime connected to network).

[https://github.com/kmxz/overlayfs-tools](https://github.com/kmxz/overlayfs-tools)

Similar discussions

[https://github.com/NixOS/nixpkgs/issues/23458](https://github.com/NixOS/nixpkgs/issues/23458)

[https://github.com/serokell/deploy-rs](https://github.com/serokell/deploy-rs)

[https://github.com/viperML/octoprint-nix](https://github.com/viperML/octoprint-nix)

[https://serokell.io/blog/deploy-rs](https://serokell.io/blog/deploy-rs)

[https://github.com/t184256/nix-on-droid/issues/94](https://github.com/t184256/nix-on-droid/issues/94)

Integral part: TermEmu (for our remote shell)

[https://github.com/xtermjs/xterm.js/](https://github.com/xtermjs/xterm.js/)

Very Very random thought: pluggable components | rootfs tarball (only useful for raspberry pi)

But then dockerfile like syntax could work too (build again on top of previous)…

Emulate RasPi on main computer (boot/health checkup)

[https://raspberrypi.stackexchange.com/questions/165/emulation-on-a-linux-pc](https://raspberrypi.stackexchange.com/questions/165/emulation-on-a-linux-pc)

[Raspberry Pi Documentation - Configuration](https://www.raspberrypi.com/documentation/computers/configuration.html#led-warning-flash-codes)

Name: HeadlessCore Linux

# Headless Tools

Wifi Pre-Configuration (country+ssid+passwd + priority + dns-config)

Wifi Hotspot fallback (not connected to any for 15-20s)

SSH server start

Static IP Configuration (+hostname)

GPIO (or anything else) if this then that configuration

[Audio Forwarding](https://www.reddit.com/r/linux/comments/ssueu6/just_discovered_mpv_works_over_ssh_i_no_longer/)

HDMI Power cut: `vcgencmd display_power 0`

FileTransfer (scp/rsync | web-integration?)

[FailSafe OS Update](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#fail-safe-os-updates-tryboot)

[RasPi Check](https://github.com/eidottermihi/rpicheck) | [Get HardwareInfo](https://kalitut.com/RaspberryPi-cpu-ram-information/)

- **Void-RPi**
    
    ```python
    [    0.000000] Booting Linux on physical CPU 0x0000000000 [0x410fd034]
    [    0.000000] Linux version 5.10.92-v8+ (dom@buildbot) (aarch64-linux-gnu-gcc-8 (Ubuntu/Linaro 8.4.0-3ubuntu1) 8.4.0, GNU ld (GNU Binutils for Ubuntu) 2.34) #1514 SMP PREEMPT Mon Jan 17 17:39:38 GMT 2022
    [    0.000000] random: fast init done
    [    0.000000] Machine model: Raspberry Pi 3 Model B Rev 1.2
    [    0.000000] efi: UEFI not found.
    [    0.000000] Reserved memory: created CMA memory pool at 0x000000001ec00000, size 256 MiB
    [    0.000000] OF: reserved mem: initialized node linux,cma, compatible id shared-dma-pool
    [    0.000000] Zone ranges:
    [    0.000000]   DMA      [mem 0x0000000000000000-0x000000003b3fffff]
    [    0.000000]   DMA32    empty
    [    0.000000]   Normal   empty
    [    0.000000] Movable zone start for each node
    [    0.000000] Early memory node ranges
    [    0.000000]   node   0: [mem 0x0000000000000000-0x000000003b3fffff]
    [    0.000000] Initmem setup node 0 [mem 0x0000000000000000-0x000000003b3fffff]
    [    0.000000] On node 0 totalpages: 242688
    [    0.000000]   DMA zone: 3792 pages used for memmap
    [    0.000000]   DMA zone: 0 pages reserved
    [    0.000000]   DMA zone: 242688 pages, LIFO batch:63
    [    0.000000] percpu: Embedded 32 pages/cpu s91416 r8192 d31464 u131072
    [    0.000000] pcpu-alloc: s91416 r8192 d31464 u131072 alloc=32*4096
    [    0.000000] pcpu-alloc: [0] 0 [0] 1 [0] 2 [0] 3 
    [    0.000000] Detected VIPT I-cache on CPU0
    [    0.000000] CPU features: detected: ARM erratum 845719
    [    0.000000] CPU features: detected: ARM erratum 843419
    [    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 238896
    [    0.000000] Kernel command line: coherent_pool=1M 8250.nr_uarts=0 snd_bcm2835.enable_compat_alsa=0 snd_bcm2835.enable_hdmi=1 video=HDMI-A-1:1920x1080M@60 vc_mem.mem_base=0x3ec00000 vc_mem.mem_size=0x40000000  console=ttyS0,115200 console=tty1 root=PARTUUID=df45d8b8-02 rootfstype=ext4 fsck.repair=yes rootwait
    [    0.000000] Dentry cache hash table entries: 131072 (order: 8, 1048576 bytes, linear)
    [    0.000000] Inode-cache hash table entries: 65536 (order: 7, 524288 bytes, linear)
    [    0.000000] mem auto-init: stack:off, heap alloc:off, heap free:off
    [    0.000000] Memory: 665880K/970752K available (11136K kernel code, 1950K rwdata, 3988K rodata, 3712K init, 1253K bss, 42728K reserved, 262144K cma-reserved)
    [    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=4, Nodes=1
    [    0.000000] ftrace: allocating 36801 entries in 144 pages
    [    0.000000] ftrace: allocated 144 pages with 2 groups
    [    0.000000] rcu: Preemptible hierarchical RCU implementation.
    [    0.000000] rcu: 	RCU event tracing is enabled.
    [    0.000000] rcu: 	RCU restricting CPUs from NR_CPUS=256 to nr_cpu_ids=4.
    [    0.000000] 	Trampoline variant of Tasks RCU enabled.
    [    0.000000] 	Rude variant of Tasks RCU enabled.
    [    0.000000] 	Tracing variant of Tasks RCU enabled.
    [    0.000000] rcu: RCU calculated value of scheduler-enlistment delay is 25 jiffies.
    [    0.000000] rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=4
    [    0.000000] NR_IRQS: 64, nr_irqs: 64, preallocated irqs: 0
    [    0.000000] random: get_random_bytes called from start_kernel+0x3b0/0x570 with crng_init=1
    [    0.000000] arch_timer: cp15 timer(s) running at 19.20MHz (phys).
    [    0.000000] clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0x46d987e47, max_idle_ns: 440795202767 ns
    [    0.000007] sched_clock: 56 bits at 19MHz, resolution 52ns, wraps every 4398046511078ns
    [    0.000330] Console: colour dummy device 80x25
    [    0.001223] printk: console [tty1] enabled
    [    0.001304] Calibrating delay loop (skipped), value calculated using timer frequency.. 38.40 BogoMIPS (lpj=76800)
    [    0.001370] pid_max: default: 32768 minimum: 301
    [    0.001621] LSM: Security Framework initializing
    [    0.001928] Mount-cache hash table entries: 2048 (order: 2, 16384 bytes, linear)
    [    0.001995] Mountpoint-cache hash table entries: 2048 (order: 2, 16384 bytes, linear)
    [    0.003737] cgroup: Disabling memory control group subsystem
    [    0.007181] rcu: Hierarchical SRCU implementation.
    [    0.008715] EFI services will not be available.
    [    0.009540] smp: Bringing up secondary CPUs ...
    [    0.010978] Detected VIPT I-cache on CPU1
    [    0.011081] CPU1: Booted secondary processor 0x0000000001 [0x410fd034]
    [    0.012818] Detected VIPT I-cache on CPU2
    [    0.012885] CPU2: Booted secondary processor 0x0000000002 [0x410fd034]
    [    0.014495] Detected VIPT I-cache on CPU3
    [    0.014552] CPU3: Booted secondary processor 0x0000000003 [0x410fd034]
    [    0.014772] smp: Brought up 1 node, 4 CPUs
    [    0.014929] SMP: Total of 4 processors activated.
    [    0.014966] CPU features: detected: 32-bit EL0 Support
    [    0.015002] CPU features: detected: CRC32 instructions
    [    0.015039] CPU features: detected: 32-bit EL1 Support
    [    0.053686] CPU: All CPU(s) started at EL2
    [    0.053831] alternatives: patching kernel code
    [    0.055854] devtmpfs: initialized
    [    0.078623] Enabled cp15_barrier support
    [    0.078714] Enabled setend support
    [    0.079055] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 7645041785100000 ns
    [    0.079125] futex hash table entries: 1024 (order: 4, 65536 bytes, linear)
    [    0.092803] pinctrl core: initialized pinctrl subsystem
    [    0.094069] DMI not present or invalid.
    [    0.094652] NET: Registered protocol family 16
    [    0.106061] DMA: preallocated 1024 KiB GFP_KERNEL pool for atomic allocations
    [    0.107023] DMA: preallocated 1024 KiB GFP_KERNEL|GFP_DMA pool for atomic allocations
    [    0.108642] DMA: preallocated 1024 KiB GFP_KERNEL|GFP_DMA32 pool for atomic allocations
    [    0.108867] audit: initializing netlink subsys (disabled)
    [    0.109343] audit: type=2000 audit(0.108:1): state=initialized audit_enabled=0 res=1
    [    0.110220] thermal_sys: Registered thermal governor 'step_wise'
    [    0.110571] cpuidle: using governor menu
    [    0.111196] hw-breakpoint: found 6 breakpoint and 4 watchpoint registers.
    [    0.111494] ASID allocator initialised with 65536 entries
    [    0.111762] Serial: AMBA PL011 UART driver
    [    0.148519] bcm2835-mbox 3f00b880.mailbox: mailbox enabled
    [    0.156818] raspberrypi-firmware soc:firmware: Attached to firmware from 2022-01-20T13:58:22, variant start
    [    0.160831] raspberrypi-firmware soc:firmware: Firmware hash is bd88f66f8952d34e4e0613a85c7a6d3da49e13e2
    [    0.212380] bcm2835-dma 3f007000.dma: DMA legacy API manager, dmachans=0x1
    [    0.218225] vgaarb: loaded
    [    0.218950] SCSI subsystem initialized
    [    0.219330] usbcore: registered new interface driver usbfs
    [    0.219439] usbcore: registered new interface driver hub
    [    0.219573] usbcore: registered new device driver usb
    [    0.220122] usb_phy_generic phy: supply vcc not found, using dummy regulator
    [    0.223031] clocksource: Switched to clocksource arch_sys_counter
    [    1.897538] VFS: Disk quotas dquot_6.6.0
    [    1.897714] VFS: Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
    [    1.897992] FS-Cache: Loaded
    [    1.898366] CacheFiles: Loaded
    [    1.900010] simple-framebuffer 3e402000.framebuffer: framebuffer at 0x3e402000, 0x7f8000 bytes, mapped to 0x(____ptrval____)
    [    1.900068] simple-framebuffer 3e402000.framebuffer: format=a8r8g8b8, mode=1920x1080x32, linelength=7680
    [    1.936378] Console: switching to colour frame buffer device 240x67
    [    1.969898] simple-framebuffer 3e402000.framebuffer: fb0: simplefb registered!
    [    1.985413] NET: Registered protocol family 2
    [    1.985933] IP idents hash table entries: 16384 (order: 5, 131072 bytes, linear)
    [    1.987918] tcp_listen_portaddr_hash hash table entries: 512 (order: 1, 8192 bytes, linear)
    [    1.988340] TCP established hash table entries: 8192 (order: 4, 65536 bytes, linear)
    [    1.988689] TCP bind hash table entries: 8192 (order: 5, 131072 bytes, linear)
    [    1.989122] TCP: Hash tables configured (established 8192 bind 8192)
    [    1.989645] UDP hash table entries: 512 (order: 2, 16384 bytes, linear)
    [    1.989905] UDP-Lite hash table entries: 512 (order: 2, 16384 bytes, linear)
    [    1.990475] NET: Registered protocol family 1
    [    1.991930] RPC: Registered named UNIX socket transport module.
    [    1.992127] RPC: Registered udp transport module.
    [    1.992284] RPC: Registered tcp transport module.
    [    1.992444] RPC: Registered tcp NFSv4.1 backchannel transport module.
    [    1.992659] PCI: CLS 0 bytes, default 64
    [    1.995560] hw perfevents: enabled with armv8_cortex_a53 PMU driver, 7 counters available
    [    1.996133] kvm [1]: IPA Size Limit: 40 bits
    [    1.998014] kvm [1]: Hyp mode initialized successfully
    [    2.004344] Initialise system trusted keyrings
    [    2.004998] workingset: timestamp_bits=46 max_order=18 bucket_order=0
    [    2.016016] zbud: loaded
    [    2.018899] FS-Cache: Netfs 'nfs' registered for caching
    [    2.020179] NFS: Registering the id_resolver key type
    [    2.020388] Key type id_resolver registered
    [    2.020534] Key type id_legacy registered
    [    2.020854] nfs4filelayout_init: NFSv4 File Layout Driver Registering...
    [    2.021070] nfs4flexfilelayout_init: NFSv4 Flexfile Layout Driver Registering...
    [    2.023166] Key type asymmetric registered
    [    2.023321] Asymmetric key parser 'x509' registered
    [    2.023558] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 249)
    [    2.024195] io scheduler mq-deadline registered
    [    2.024353] io scheduler kyber registered
    [    2.040937] bcm2835-rng 3f104000.rng: hwrng registered
    [    2.042007] vc-mem: phys_addr:0x00000000 mem_base=0x3ec00000 mem_size:0x40000000(1024 MiB)
    [    2.044370] gpiomem-bcm2835 3f200000.gpiomem: Initialised: Registers at 0x3f200000
    [    2.045145] cacheinfo: Unable to detect cache hierarchy for CPU 0
    [    2.063674] brd: module loaded
    [    2.091977] loop: module loaded
    [    2.103562] Loading iSCSI transport class v2.0-870.
    [    2.117207] libphy: Fixed MDIO Bus: probed
    [    2.127904] usbcore: registered new interface driver r8152
    [    2.136433] usbcore: registered new interface driver lan78xx
    [    2.144755] usbcore: registered new interface driver smsc95xx
    [    2.153608] dwc_otg: version 3.00a 10-AUG-2012 (platform bus)
    [    2.889918] Core Release: 2.80a
    [    2.897920] Setting default values for core params
    [    2.905878] Finished setting default values for core params
    [    3.114397] Using Buffer DMA mode
    [    3.122349] Periodic Transfer Interrupt Enhancement - disabled
    [    3.130480] Multiprocessor Interrupt Enhancement - disabled
    [    3.138651] OTG VER PARAM: 0, OTG VER FLAG: 0
    [    3.146794] Dedicated Tx FIFOs mode
    
    [    3.159560] WARN::dwc_otg_hcd_init:1074: FIQ DMA bounce buffers: virt = ffffffc011a19000 dma = 0x00000000df000000 len=9024
    [    3.175762] FIQ FSM acceleration enabled for :
                   Non-periodic Split Transactions
                   Periodic Split Transactions
                   High-Speed Isochronous Endpoints
                   Interrupt/Control Split Transaction hack enabled
    [    3.216173] dwc_otg: Microframe scheduler enabled
    
    [    3.216244] WARN::hcd_init_fiq:497: MPHI regs_base at ffffffc011625000
    [    3.232136] dwc_otg 3f980000.usb: DWC OTG Controller
    [    3.240062] dwc_otg 3f980000.usb: new USB bus registered, assigned bus number 1
    [    3.248044] dwc_otg 3f980000.usb: irq 74, io mem 0x00000000
    [    3.255912] Init: Port Power? op_state=1
    [    3.263647] Init: Power Port (0)
    [    3.271705] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 5.10
    [    3.279459] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
    [    3.287163] usb usb1: Product: DWC OTG Controller
    [    3.294783] usb usb1: Manufacturer: Linux 5.10.92-v8+ dwc_otg_hcd
    [    3.302432] usb usb1: SerialNumber: 3f980000.usb
    [    3.311147] hub 1-0:1.0: USB hub found
    [    3.318784] hub 1-0:1.0: 1 port detected
    [    3.328172] dwc_otg: FIQ enabled
    [    3.328192] dwc_otg: NAK holdoff enabled
    [    3.328208] dwc_otg: FIQ split-transaction FSM enabled
    [    3.328230] Module dwc_common_port init
    [    3.329048] usbcore: registered new interface driver uas
    [    3.337109] usbcore: registered new interface driver usb-storage
    [    3.345057] mousedev: PS/2 mouse device common for all mice
    [    3.355177] bcm2835-wdt bcm2835-wdt: Broadcom BCM2835 watchdog timer
    [    3.367147] sdhci: Secure Digital Host Controller Interface driver
    [    3.374691] sdhci: Copyright(c) Pierre Ossman
    [    3.383181] mmc-bcm2835 3f300000.mmcnr: could not get clk, deferring probe
    [    3.391783] sdhost-bcm2835 3f202000.mmc: could not get clk, deferring probe
    [    3.399802] sdhci-pltfm: SDHCI platform and OF driver helper
    [    3.410552] ledtrig-cpu: registered to indicate activity on CPUs
    [    3.418825] hid: raw HID events driver (C) Jiri Kosina
    [    3.426707] usbcore: registered new interface driver usbhid
    [    3.434334] usbhid: USB HID core driver
    [    3.442193] Indeed it is in host mode hprt0 = 00021501
    [    3.459119] ashmem: initialized
    [    3.518308] Initializing XFRM netlink socket
    [    3.526049] NET: Registered protocol family 17
    [    3.533828] Key type dns_resolver registered
    [    3.542363] registered taskstats version 1
    [    3.549773] Loading compiled-in X.509 certificates
    [    3.558004] Key type ._fscrypt registered
    [    3.565437] Key type .fscrypt registered
    [    3.572690] Key type fscrypt-provisioning registered
    [    3.601327] uart-pl011 3f201000.serial: cts_event_workaround enabled
    [    3.608846] 3f201000.serial: ttyAMA0 at MMIO 0x3f201000 (irq = 99, base_baud = 0) is a PL011 rev2
    [    3.621772] bcm2835-power bcm2835-power: Broadcom BCM2835 power domains driver
    [    3.632567] mmc-bcm2835 3f300000.mmcnr: mmc_debug:0 mmc_debug2:0
    [    3.640195] usb 1-1: new high-speed USB device number 2 using dwc_otg
    [    3.647971] Indeed it is in host mode hprt0 = 00001101
    [    3.655502] mmc-bcm2835 3f300000.mmcnr: DMA channel allocated
    [    3.688747] sdhost: log_buf @ (____ptrval____) (c2be2000)
    [    3.777222] mmc0: sdhost-bcm2835 loaded - DMA enabled (>1)
    [    3.790379] of_cfs_init
    [    3.798536] of_cfs_init: OK
    [    3.807793] Waiting for root device PARTUUID=df45d8b8-02...
    [    3.899571] usb 1-1: New USB device found, idVendor=0424, idProduct=9514, bcdDevice= 2.00
    [    3.907447] usb 1-1: New USB device strings: Mfr=0, Product=0, SerialNumber=0
    [    3.921917] hub 1-1:1.0: USB hub found
    [    3.929904] hub 1-1:1.0: 5 ports detected
    [    4.227073] usb 1-1.1: new high-speed USB device number 3 using dwc_otg
    [    4.335543] usb 1-1.1: New USB device found, idVendor=0424, idProduct=ec00, bcdDevice= 2.00
    [    4.343380] usb 1-1.1: New USB device strings: Mfr=0, Product=0, SerialNumber=0
    [    4.354181] smsc95xx v2.0.0
    [    4.407900] libphy: smsc95xx-mdiobus: probed
    [    4.416859] smsc95xx 1-1.1:1.0 eth0: register 'smsc95xx' at usb-3f980000.usb-1.1, smsc95xx USB 2.0 Ethernet, b8:27:eb:03:43:ef
    [    4.647072] usb 1-1.3: new high-speed USB device number 4 using dwc_otg
    [    4.756615] usb 1-1.3: New USB device found, idVendor=0781, idProduct=5567, bcdDevice= 1.00
    [    4.764708] usb 1-1.3: New USB device strings: Mfr=1, Product=2, SerialNumber=3
    [    4.772733] usb 1-1.3: Product: Cruzer Blade
    [    4.780701] usb 1-1.3: Manufacturer: SanDisk
    [    4.788558] usb 1-1.3: SerialNumber: 4C530001180411108240
    [    4.797692] usb-storage 1-1.3:1.0: USB Mass Storage device detected
    [    4.806767] scsi host0: usb-storage 1-1.3:1.0
    [    4.977265] random: crng init done
    [    5.824596] scsi 0:0:0:0: Direct-Access     SanDisk  Cruzer Blade     1.00 PQ: 0 ANSI: 6
    [    5.838443] sd 0:0:0:0: [sda] 30605312 512-byte logical blocks: (15.7 GB/14.6 GiB)
    [    5.847712] sd 0:0:0:0: [sda] Write Protect is off
    [    5.855690] sd 0:0:0:0: [sda] Mode Sense: 43 00 00 00
    [    5.856382] sd 0:0:0:0: [sda] Write cache: disabled, read cache: enabled, doesn't support DPO or FUA
    [    5.876840]  sda: sda1 sda2
    [    5.891707] sd 0:0:0:0: [sda] Attached SCSI removable disk
    [    5.927979] EXT4-fs (sda2): mounted filesystem with ordered data mode. Opts: (null)
    [    5.936543] VFS: Mounted root (ext4 filesystem) readonly on device 8:2.
    [    5.953068] devtmpfs: mounted
    [    5.975372] Freeing unused kernel memory: 3712K
    [    5.984175] Run /sbin/init as init process
    [    5.992458]   with arguments:
    [    5.992475]     /sbin/init
    [    5.992489]   with environment:
    [    5.992505]     HOME=/
    [    5.992519]     TERM=linux
    [    6.547089] systemd[1]: System time before build time, advancing clock.
    [    6.710032] NET: Registered protocol family 10
    [    6.721055] Segment Routing with IPv6
    [    6.810974] systemd[1]: systemd 247.3-6 running in system mode. (+PAM +AUDIT +SELINUX +IMA +APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +ZSTD +SECCOMP +BLKID +ELFUTILS +KMOD +IDN2 -IDN +PCRE2 default-hierarchy=unified)
    [    6.832461] systemd[1]: Detected architecture arm64.
    [    6.880290] systemd[1]: Set hostname to <raspberrypi>.
    [    7.977986] systemd[1]: Queued start job for default target Graphical Interface.
    [    7.992801] systemd[1]: Created slice system-getty.slice.
    [    8.014133] systemd[1]: Created slice system-modprobe.slice.
    [    8.035255] systemd[1]: Created slice system-systemd\x2dfsck.slice.
    [    8.056503] systemd[1]: Created slice system-wpa_supplicant.slice.
    [    8.077730] systemd[1]: Created slice User and Session Slice.
    [    8.098283] systemd[1]: Started Dispatch Password Requests to Console Directory Watch.
    [    8.118674] systemd[1]: Started Forward Password Requests to Wall Directory Watch.
    [    8.140291] systemd[1]: Set up automount Arbitrary Executable File Formats File System Automount Point.
    [    8.161172] systemd[1]: Reached target Local Encrypted Volumes.
    [    8.182025] systemd[1]: Reached target Paths.
    [    8.202658] systemd[1]: Reached target Slices.
    [    8.223207] systemd[1]: Reached target Swap.
    [    8.244978] systemd[1]: Listening on fsck to fsckd communication Socket.
    [    8.266390] systemd[1]: Listening on initctl Compatibility Named Pipe.
    [    8.289172] systemd[1]: Listening on Journal Audit Socket.
    [    8.311432] systemd[1]: Listening on Journal Socket (/dev/log).
    [    8.333403] systemd[1]: Listening on Journal Socket.
    [    8.355785] systemd[1]: Listening on Network Service Netlink Socket.
    [    8.386985] systemd[1]: Listening on udev Control Socket.
    [    8.409239] systemd[1]: Listening on udev Kernel Socket.
    [    8.431505] systemd[1]: Condition check resulted in Huge Pages File System being skipped.
    [    8.447763] systemd[1]: Mounting POSIX Message Queue File System...
    [    8.476763] systemd[1]: Mounting RPC Pipe File System...
    [    8.507472] systemd[1]: Mounting Kernel Debug File System...
    [    8.536687] systemd[1]: Mounting Kernel Trace File System...
    [    8.558653] systemd[1]: Condition check resulted in Kernel Module supporting RPCSEC_GSS being skipped.
    [    8.576923] systemd[1]: Starting Restore / save the current clock...
    [    8.608353] systemd[1]: Starting Set the console keyboard layout...
    [    8.640167] systemd[1]: Starting Create list of static device nodes for the current kernel...
    [    8.670164] systemd[1]: Starting Load Kernel Module configfs...
    [    8.700229] systemd[1]: Starting Load Kernel Module drm...
    [    8.736890] systemd[1]: Starting Load Kernel Module fuse...
    [    8.786578] systemd[1]: Condition check resulted in Set Up Additional Binary Formats being skipped.
    [    8.788187] fuse: init (API version 7.32)
    [    8.803196] systemd[1]: Starting File System Check on Root Device...
    [    8.843595] systemd[1]: Starting Journal Service...
    [    8.885505] systemd[1]: Starting Load Kernel Modules...
    [    8.930231] systemd[1]: Starting Coldplug All udev Devices...
    [    8.986641] systemd[1]: Mounted POSIX Message Queue File System.
    [    9.004313] cryptd: max_cpu_qlen set to 1000
    [    9.021304] systemd[1]: Mounted RPC Pipe File System.
    [    9.043556] systemd[1]: Mounted Kernel Debug File System.
    [    9.066248] systemd[1]: Mounted Kernel Trace File System.
    [    9.091295] systemd[1]: Finished Restore / save the current clock.
    [    9.117527] systemd[1]: Finished Create list of static device nodes for the current kernel.
    [    9.143329] systemd[1]: modprobe@configfs.service: Succeeded.
    [    9.155826] systemd[1]: Finished Load Kernel Module configfs.
    [    9.179859] systemd[1]: modprobe@drm.service: Succeeded.
    [    9.192337] systemd[1]: Finished Load Kernel Module drm.
    [    9.217410] systemd[1]: modprobe@fuse.service: Succeeded.
    [    9.230143] systemd[1]: Finished Load Kernel Module fuse.
    [    9.255978] systemd[1]: Finished Load Kernel Modules.
    [    9.286959] systemd[1]: Mounting FUSE Control File System...
    [    9.318002] systemd[1]: Mounting Kernel Configuration File System...
    [    9.351183] systemd[1]: Started File System Check Daemon to report status.
    [    9.380463] systemd[1]: Starting Apply Kernel Variables...
    [    9.423578] systemd[1]: Mounted FUSE Control File System.
    [    9.452271] systemd[1]: Finished File System Check on Root Device.
    [    9.474726] systemd[1]: Mounted Kernel Configuration File System.
    [    9.503674] systemd[1]: Starting Remount Root and Kernel File Systems...
    [    9.531828] systemd[1]: Started Journal Service.
    [    9.737581] EXT4-fs (sda2): re-mounted. Opts: (null)
    [    9.837854] systemd-journald[143]: Received client request to flush runtime journal.
    [    9.863936] systemd-journald[143]: File /var/log/journal/cfe3a8b947b14b989500202e0e76bf3f/system.journal corrupted or uncleanly shut down, renaming and replacing.
    [   11.373976] mc: Linux media interface: v0.10
    [   11.406891] vc_sm_cma: module is from the staging directory, the quality is unknown, you have been warned.
    [   11.414183] bcm2835_vc_sm_cma_probe: Videocore shared memory driver
    [   11.414259] [vc_sm_connected_init]: start
    [   11.427709] [vc_sm_connected_init]: installed successfully
    [   11.464274] videodev: Linux video capture interface: v2.00
    [   11.470805] snd_bcm2835: module is from the staging directory, the quality is unknown, you have been warned.
    [   11.479546] bcm2835_audio bcm2835_audio: card created with 8 channels
    [   11.504404] bcm2835_mmal_vchiq: module is from the staging directory, the quality is unknown, you have been warned.
    [   11.504411] bcm2835_mmal_vchiq: module is from the staging directory, the quality is unknown, you have been warned.
    [   11.509695] bcm2835_mmal_vchiq: module is from the staging directory, the quality is unknown, you have been warned.
    [   11.522601] bcm2835_v4l2: module is from the staging directory, the quality is unknown, you have been warned.
    [   11.524789] bcm2835_isp: module is from the staging directory, the quality is unknown, you have been warned.
    [   11.527293] bcm2835_codec: module is from the staging directory, the quality is unknown, you have been warned.
    [   11.537266] bcm2835-isp bcm2835-isp: Device node output[0] registered as /dev/video13
    [   11.540166] bcm2835-isp bcm2835-isp: Device node capture[0] registered as /dev/video14
    [   11.541264] bcm2835-isp bcm2835-isp: Device node capture[1] registered as /dev/video15
    [   11.547162] bcm2835-isp bcm2835-isp: Device node stats[2] registered as /dev/video16
    [   11.547228] bcm2835-isp bcm2835-isp: Register output node 0 with media controller
    [   11.547262] bcm2835-isp bcm2835-isp: Register capture node 1 with media controller
    [   11.547290] bcm2835-isp bcm2835-isp: Register capture node 2 with media controller
    [   11.547319] bcm2835-isp bcm2835-isp: Register capture node 3 with media controller
    [   11.548226] bcm2835-codec bcm2835-codec: Device registered as /dev/video10
    [   11.548316] bcm2835-codec bcm2835-codec: Loaded V4L2 decode
    [   11.557549] bcm2835-codec bcm2835-codec: Device registered as /dev/video11
    [   11.557641] bcm2835-codec bcm2835-codec: Loaded V4L2 encode
    [   11.565933] bcm2835-isp bcm2835-isp: Device node output[0] registered as /dev/video20
    [   11.566829] bcm2835-codec bcm2835-codec: Device registered as /dev/video12
    [   11.566851] bcm2835-isp bcm2835-isp: Device node capture[0] registered as /dev/video21
    [   11.566913] bcm2835-codec bcm2835-codec: Loaded V4L2 isp
    [   11.568255] bcm2835-isp bcm2835-isp: Device node capture[1] registered as /dev/video22
    [   11.569073] bcm2835-isp bcm2835-isp: Device node stats[2] registered as /dev/video23
    [   11.569131] bcm2835-isp bcm2835-isp: Register output node 0 with media controller
    [   11.569164] bcm2835-isp bcm2835-isp: Register capture node 1 with media controller
    [   11.569193] bcm2835-isp bcm2835-isp: Register capture node 2 with media controller
    [   11.569245] bcm2835-isp bcm2835-isp: Register capture node 3 with media controller
    [   11.569694] bcm2835-isp bcm2835-isp: Loaded V4L2 bcm2835-isp
    [   11.571258] bcm2835-codec bcm2835-codec: Device registered as /dev/video18
    [   11.571327] bcm2835-codec bcm2835-codec: Loaded V4L2 image_fx
    [   12.071876] checking generic (3e402000 7f8000) vs hw (0 ffffffffffffffff)
    [   12.071911] fb0: switching to vc4drmfb from simple
    [   12.077239] Console: switching to colour dummy device 80x25
    [   12.089883] vc4-drm soc:gpu: bound 3f400000.hvs (ops vc4_hvs_ops [vc4])
    [   12.112071] Registered IR keymap rc-cec
    [   12.112380] rc rc0: vc4 as /devices/platform/soc/3f902000.hdmi/rc/rc0
    [   12.112690] input: vc4 as /devices/platform/soc/3f902000.hdmi/rc/rc0/input0
    [   12.171410] sd 0:0:0:0: Attached scsi generic sg0 type 0
    [   12.366625] SMSC LAN8700 usb-001:003:01: attached PHY driver [SMSC LAN8700] (mii_bus:phy_addr=usb-001:003:01, irq=POLL)
    [   12.367674] smsc95xx 1-1.1:1.0 eth0: hardware isn't capable of remote wakeup
    [   12.393545] smsc95xx 1-1.1:1.0 eth0: Link is Down
    [   12.890496] vc4-drm soc:gpu: bound 3f400000.hvs (ops vc4_hvs_ops [vc4])
    [   12.892807] Registered IR keymap rc-cec
    [   12.893388] rc rc0: vc4 as /devices/platform/soc/3f902000.hdmi/rc/rc0
    [   12.893748] input: vc4 as /devices/platform/soc/3f902000.hdmi/rc/rc0/input1
    [   12.903591] vc4-drm soc:gpu: bound 3f902000.hdmi (ops vc4_hdmi_ops [vc4])
    [   12.904392] vc4-drm soc:gpu: bound 3f004000.txp (ops vc4_txp_ops [vc4])
    [   12.904778] vc4-drm soc:gpu: bound 3f206000.pixelvalve (ops vc4_crtc_ops [vc4])
    [   12.905205] vc4-drm soc:gpu: bound 3f207000.pixelvalve (ops vc4_crtc_ops [vc4])
    [   12.905574] vc4-drm soc:gpu: bound 3f807000.pixelvalve (ops vc4_crtc_ops [vc4])
    [   12.905927] vc4-drm soc:gpu: bound 3fc00000.v3d (ops vc4_v3d_ops [vc4])
    [   12.921223] [drm] Initialized vc4 0.0.0 20140616 for soc:gpu on minor 0
    [   12.990775] Console: switching to colour frame buffer device 240x67
    [   13.034846] vc4-drm soc:gpu: [drm] fb0: vc4drmfb frame buffer device
    [   13.663181] Under-voltage detected! (0x00050005)
    [   15.449393] uart-pl011 3f201000.serial: no DMA platform data
    [   15.659157] Adding 102396k swap on /var/swap.  Priority:-2 extents:3 across:131072k FS
    [   35.807267] cam-dummy-reg: disabling
    [   35.807307] cam1-reg: disabling
    [   83.219101] usb 1-1.4: new high-speed USB device number 5 using dwc_otg
    [   83.336622] usb 1-1.4: New USB device found, idVendor=0cf3, idProduct=9271, bcdDevice= 1.08
    [   83.336650] usb 1-1.4: New USB device strings: Mfr=16, Product=32, SerialNumber=48
    [   83.336671] usb 1-1.4: Product: USB2.0 WLAN
    [   83.336690] usb 1-1.4: Manufacturer: ATHEROS
    [   83.336709] usb 1-1.4: SerialNumber: 12345
    [   83.520302] cfg80211: Loading compiled-in X.509 certificates for regulatory database
    [   83.554312] cfg80211: Loaded X.509 cert 'sforshee: 00b28ddf47aef9cea7'
    [   83.562030] cfg80211: loaded regulatory.db is malformed or signature is missing/invalid
    [   83.763883] usb 1-1.4: ath9k_htc: Firmware ath9k_htc/htc_9271-1.4.0.fw requested
    [   83.764773] usbcore: registered new interface driver ath9k_htc
    [   84.082503] usb 1-1.4: ath9k_htc: Transferred FW: ath9k_htc/htc_9271-1.4.0.fw, size: 51008
    [   84.338894] ath9k_htc 1-1.4:1.0: ath9k_htc: HTC initialized with 33 credits
    [   85.074014] ath9k_htc 1-1.4:1.0: ath9k_htc: FW Version: 1.4
    [   85.074029] ath9k_htc 1-1.4:1.0: FW RMW support: On
    [   85.074039] ath: EEPROM regdomain: 0x809c
    [   85.074047] ath: EEPROM indicates we should expect a country code
    [   85.074056] ath: doing EEPROM country->regdmn map search
    [   85.074063] ath: country maps to regdmn code: 0x52
    [   85.074073] ath: Country alpha2 being used: CN
    [   85.074100] ath: Regpair used: 0x52
    [   85.112791] ieee80211 phy0: Atheros AR9271 Rev:1
    [   92.467297] IPv6: ADDRCONF(NETDEV_CHANGE): wlan0: link becomes ready
    [  163.859306] systemd-journald[143]: File /var/log/journal/cfe3a8b947b14b989500202e0e76bf3f/user-1000.journal corrupted or uncleanly shut down, renaming and replacing.
    ```
    
    script:
    
    ```python
    #!/usr/bin/env bash
    
    if [[ $EUID -ne 0 ]]; then
      >&2 echo "This script must be run as root"
      exit 1
    fi
    
    usage() {
      >&2 echo "$1"
      >&2 echo "Usage: $(basename $0) <disk-name> [platformfs-file-path]"
      >&2 echo "ENV: MKLIVE_PATH  - Points to already existing void-mklive clone"
      >&2 echo ""
      >&2 echo "Example: $(basename $0) /dev/sda"
      exit 1
    }
    
    disk=$(basename $1)
    [[ -b /dev/$disk && $(stat -c "%Lr" /dev/$disk) == '0' ]] || usage "Unknown device: $1"
    
    # 100M vfat (0c) + REST_OF_DISK ext4 (83)
    printf '%s\n' o n p 1 '' +100M a n p 2 '' '' t 1 c t 2 83 w | fdisk /dev/$disk --wipe always
    sync && sleep 1
    
    read boot boot_uuid <<< "$(lsblk -ro NAME,FSTYPE,PARTUUID | grep $disk | grep vfat | tr -s ' ' | cut -d' ' -f1,3)"
    read root root_uuid <<< "$(lsblk -ro NAME,FSTYPE,PARTUUID | grep $disk | grep ext4 | tr -s ' ' | cut -d' ' -f1,3)"
    
    echo "Partitions created:"
    echo $boot $boot_uuid
    echo $root $root_uuid
    echo
    
    yes | mkfs.vfat /dev/$boot
    yes | mkfs.ext4 -O ^has_journal /dev/$root
    
    platformfs_file="$2"
    if [[ ! -f "$platformfs_file" ]]; then
        platformfs_file="void-rpi3-PLATFORMFS-$(date '+%Y%m%d').tar.xz"
    fi
    
    if [[ ! -f "$platformfs_file" ]]; then
        [[ -d "$MKLIVE_PATH" ]] && cd $MKLIVE_PATH || git clone https://github.com/void-linux/void-mklive && cd void-mklive
    
        # Use void-mklive to build this (disable binfmt-support while doing so).
        xbps-install -Sy xbps
        xbps-install -Sy bash findutils which util-linux git make kmod qemu-user-static tar xz
        ./mkrootfs.sh aarch64
        ./mkplatformfs.sh -p 'bash fish-shell neovim' rpi3 void-aarch64-ROOTFS-$(date '+%Y%m%d').tar.xz
    fi
    
    mkdir -p /mnt/rpi
    mount /dev/$root /mnt/rpi
    mkdir -p /mnt/rpi/boot
    mount /dev/$boot /mnt/rpi/boot
    
    tar xvfp "$platformfs_file" -C /mnt/rpi 1>/dev/null
    echo "Successfully extracted $platformfs_file to /mnt/rpi"
    
    {
        echo "proc  /proc proc defaults 0 0"
        echo "PARTUUID=$boot_uuid /boot vfat defaults 0 0"
        echo "PARTUUID=$root_uuid /     ext4 defaults 0 0"
    } > /mnt/rpi/etc/fstab
    
    umount -R /mnt/rpi
    ```
    

```bash
Web interface: 
  https://www.reddit.com/r/homelab/comments/mnhm2p/raspberry_pi_8gb_docker_based_self_hosted_services
  https://www.reddit.com/r/selfhosted/comments/poca6i/selfhosting_all_these_services_on_two_raspberry https://www.reddit.com/r/startpages/comments/i0zrs3/my_home_network_dashboard
```