# Enable Hardware Video Accel on chromium/brave

Done: Yes
Duration: Day
Tags: Needs to be done

# References

[https://www.reddit.com/r/linux/comments/p2ivtq/updated_guide_on_how_to_get_hardware_acceleration/](https://www.reddit.com/r/linux/comments/p2ivtq/updated_guide_on_how_to_get_hardware_acceleration/)

[https://community.frame.work/t/video-decoding-acceleration-in-linux/11688](https://community.frame.work/t/video-decoding-acceleration-in-linux/11688)

[https://www.reddit.com/r/Fedora/comments/bzvdwy/got_hardware_acceleration_with_new_intel_iris/](https://www.reddit.com/r/Fedora/comments/bzvdwy/got_hardware_acceleration_with_new_intel_iris/)

[https://askubuntu.com/questions/1299067/ubuntu-20-04-no-driver-loaded-for-intel-iris-xe-graphics](https://askubuntu.com/questions/1299067/ubuntu-20-04-no-driver-loaded-for-intel-iris-xe-graphics)