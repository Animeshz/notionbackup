# Advertise laptop as keyboard/mouse input to Android/OtherDevices

Done: No
Duration: Day
Priority: Not Important & Not Urgent
Tags: Low Priority
Type: Personal

[https://github.com/kcolford/hidclient](https://github.com/kcolford/hidclient)

[Is there a way to turn my computer into a bluetooth keyboard?](https://superuser.com/questions/467389/is-there-a-way-to-turn-my-computer-into-a-bluetooth-keyboard)

[Using laptop keyboard as wireless bluetooth keyboard](https://superuser.com/questions/615027/using-laptop-keyboard-as-wireless-bluetooth-keyboard)