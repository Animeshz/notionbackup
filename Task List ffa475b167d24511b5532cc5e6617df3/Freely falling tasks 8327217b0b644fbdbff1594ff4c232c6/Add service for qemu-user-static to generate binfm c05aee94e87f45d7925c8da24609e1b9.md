# Add service for qemu-user-static to generate binfmts

Done: Yes
Duration: Quick
Priority: Important & Urgent
Tags: Needs to be done

Reference: [binfmt-support’s service](https://github.com/void-linux/void-packages/blob/master/srcpkgs/binfmt-support/files/binfmt-support/run)

[https://github.com/multiarch/qemu-user-static/blob/master/containers/latest/register.sh](https://github.com/multiarch/qemu-user-static/blob/master/containers/latest/register.sh)

[https://github.com/qemu/qemu/blob/master/scripts/qemu-binfmt-conf.sh](https://github.com/qemu/qemu/blob/master/scripts/qemu-binfmt-conf.sh)

[https://wiki.archlinux.org/title/Binfmt_misc_for_Java](https://wiki.archlinux.org/title/Binfmt_misc_for_Java)