# Fish Shell: Autocompletion plugin to have enquirer like prompt or autocomplete if single option with Enter pressed at the cursor position. Escaped enter using <C-Enter> can be reserved for normal enter operations.

Done: Yes
Duration: Week
Priority: Important & Not Urgent
Tags: Not worth it, Stale

```bash
  ~/TrashThings/builds/docker_host_tar >>> docker im<Tab>
images  (List images)  import  (Create a new filesystem image from the contents of a tarball)
  ~/TrashThings/builds/docker_host_tar >>> docker ima<Tab> # -> images (no prompt)
```

bc most of times I would want to write `git rem`, `docker im` for remote/images, and fish usually can parse that up with tab completions…

Actually,

[https://github.com/nvbn/thefuck](https://github.com/nvbn/thefuck)

is exactly what we want with better integration.