# Restart on Cluster+ Project

Done: No
Duration: Month
Priority: Not Important & Not Urgent
Tags: Archived, Not worth it, Stale

## Ideas

### Command

+cluster add <name>: cluster or a command script, to be added

+cluster run <name> <args>: run that script

[Styler Extraction](https://github.com/Animeshz/ObseleteClusterPlusPHP/blob/master/src/Utils/CommandHelpers.php)