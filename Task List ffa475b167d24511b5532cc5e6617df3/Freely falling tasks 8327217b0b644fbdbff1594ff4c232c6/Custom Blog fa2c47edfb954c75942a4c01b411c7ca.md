# Custom Blog

Done: Yes
Priority: Important & Urgent
Project Source: https://github.com/Animeshz/blog
Tags: Needs to be done
Type: Project

[Oinam Jekyll](https://oinam.github.io/oinam-jekyll/)

[Ubuntu Server 20.04 installation and basic setup](https://golas.blog/administration/2021/06/14/ubuntu-server-install.html)

[GSoC 2021 with Navidrome Part 3 - Data Fetching](https://samarsault.com/open-source/gsoc/GSoC-21-Navidrome-3/)

Require: Inject custom css on themes

Inspiration of using oinam theme from creator: ‣ | [https://notes.oinam.com/books/how-to-win-friends-and-influence-people](https://notes.oinam.com/books/how-to-win-friends-and-influence-people/)

Last few suggestions

[https://github.com/squidfunk/mkdocs-material/issues/3353](https://github.com/squidfunk/mkdocs-material/issues/3353)

```markdown
---
title: Blog Post Title
author:
  name: Author Name
  link: https://github.com/github_username
date: yyyy-mm-dd hh:mm:ss
categories: [Category, Category]
tags: [tag, tag, tag]
---
```

Findings (while going through):

https://github.com/oinam/whatsapp

https://github.com/oinam/no.phone.wtf

[https://github.com/oinam/oinam.fyi](https://github.com/oinam/oinam.fyi)

https://github.com/oinam/oinam.fyi - notes in oinam (blog) theme

[Character Entities for HTML, CSS and Javascript](https://oinam.github.io/entities/)