# Paritioning Utility (sparter)

Done: No
Duration: Month
Priority: Important & Urgent
Project Source: https://github.com/Animeshz/sparter
Tags: Needs to be done
Type: Project

> **Khatarnaak limitations of CloneZilla:**
> 
> - The destination partition must be equal or larger than the source one.
> - Differential/incremental backup is not implemented yet.
> - Online imaging/cloning is not implemented yet. The partition to be imaged or cloned has to be unmounted.
> - Due to the image format limitation, the image can not be explored or mounted. You can _NOT_ recover single file from the image. However, you still have workaround to make it, read [this](http://drbl.org/faq/fine-print.php?path=./2_System/43_read_ntfsimg_content.faq#43_read_ntfsimg_content.faq).
> - Recovery Clonezilla live with multiple CDs or DVDs is not implemented yet. Now all the files have to be in one CD or DVD if you choose to create the recovery iso file.

### GooGle: [what's difference between different filesystem types if they can be accessed as vfs](https://www.google.com/search?q=what%27s+difference+between+different+filesystem+types+if+they+can+be+accessed+as+vfs&oq=what%27s+difference+between+different+filesystem+types+if+they+can+be+accessed+as+vfs&aqs=chrome..69i57.14469j0j1&sourceid=chrome&ie=UTF-8)

[What is the difference between a tar of a complete filesystem and an image?](https://unix.stackexchange.com/questions/96622/what-is-the-difference-between-a-tar-of-a-complete-filesystem-and-an-image)

[What Is a File System? Types of Computer File Systems and How they Work - Explained with Examples](https://www.freecodecamp.org/news/file-systems-architecture-explained/)

FS: “organize the data” (space management, metadata, data encryption, file access control, and data integrity are the responsibilities)

In case not using rust::tar (shell/etc)

[How can I run the "tar -czf" command in Windows?](https://superuser.com/questions/244703/how-can-i-run-the-tar-czf-command-in-windows)

**Note:** Extra care (or give suggestion) if backing up rootfs to also backup efi entries (or atleast generate them back at reinstallation)

```bash
Application for easily resize-move-create-delete partitions also to save them as an image file into a file (referencing https://github.com/Thomas-Tsai/partclone for only processing used space). With separate strategy for mounted and unmounted fs. Probably in rust.
  - https://askubuntu.com/questions/390769/how-do-i-resize-partitions-using-command-line-without-using-a-gui-on-a-server
  - https://www.makeuseof.com/how-to-create-resize-and-delete-linux-partitions-with-cfdisk/
Could be my first test driven project?
https://docs.rs/gpt/2.0.0/gpt - virtual partition table in RAM?  + https://github.com/cecton/mbrman if needed, not necessary.
Maybe name SParter (s-part-er) - Simple Smart and Robust Partitioner?
RMCDBR - resize-move-create-delete-backup-restore
Integration tests on a pendrive maybe (with confirmation twice)?
  - List all partitions
    - <bin> list
  - Backup/Restore
    - <bin> backup create /lsbackup.img
    - <bin> backup restore backup.img /dev/sdxx  |||  cat backup.img | <bin> backup restore - /dev/sdxx
  - Shrink
    :alert: may take a while if the partition is fragmented and requested size is high enough, donot interrupt the process or data-loss may happen
    - <bin> shrink 500M [before|after] /dev/sdxx
  - Move unallocated space
    :alert: indirect movement of partitions it is crossing by, advised to backup anything important first
	:alert: may take a while if requested size is high enough, donot interrupt the process or data-loss may happen
    - <bin> list
	- <bin> move <n> 10G [before|after] /dev/sdxx
  - Extend unallocated space next to a partition
    - <bin> list
	- <bin> extend 500M [before|after] /dev/sdxx
  - Create partition
    - <bin> list
    - <bin> create <n> 2G [before|after] /dev/sdxx
  - Delete partition (create unallocated space)
    :alert: deletion of a partition is irreversible (or is it? unless rewritten, lets see...)
    - <bin> delete /dev/sdxx
  - Rename partition
    :alert: be cautious, partition table is altered manually. A backup is created if given to restore easily.
    reference: https://unix.stackexchange.com/a/90224/259021
	- <bin> rename /dev/sdxx /dev/sdxx [--backup original-partition-table.save]
Testing similar to https://github.com/Thomas-Tsai/partclone/blob/master/tests/mini_clone_restore_test || https://github.com/karelzak/util-linux/blob/256e524f5779629aaeba1e071b50ab1fc66d7c07/tests/functions.sh#L637-L645 + https://github.com/karelzak/util-linux/blob/master/tests/ts/fdisk/gpt but as cargo/rust tests.
```

## Implementation

![GUID_Partition_Table_Scheme.svg](Paritioning%20Utility%20(sparter)%2021cd06d26c5c494785c3ba259c781244/GUID_Partition_Table_Scheme.svg)

- [UEFI (GPT) specs](https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_entries_(LBA_2%E2%80%9333)) says:
    - minimum partition entry array of size 128 each having size of exactly 128 bytes for storing metadata about it and partition label.
- Partition contains file details (inode - ext4 | mft - ntfs | etc.) and file storage
    - df -ih
    - df -h
- File requires minimum 1 block (specified at time of formatting, e.g. 4K), that block cannot be used by another file
    - du -b <file>
    - du -B 1 <file>
- We basically would tar the contents and store the metadata in a struct/file
- [Temporary mount points](https://superuser.com/a/1030777/776142) | [Process-only-visible mountpoints](https://serverfault.com/a/491639/960542) | [/tmp vs /run](https://unix.stackexchange.com/questions/316161/whats-the-difference-between-tmp-and-run)
- [Listing disks](https://docs.rs/libparted/latest/libparted/) | [ffi switch different funs](https://stackoverflow.com/a/66317617/11377112)

### Handling dirty_write

1. Reading is cheaper than writing: If file is opened in some program
    1. Read the file, hash it, write it to tar, read it back and check the hash
    2. If not matched repeat the same process, if matched write it (& keep an eye on until its closed, if closed before our backup ends rewrite it onto the tar).
2. Kernel stuffs for controlling (bdflush/pdflush) | dmsetup
    
    [Writing Dirty Buffers to Disk - Linux Kernel Reference](https://www.halolinux.us/kernel-reference/writing-dirty-buffers-to-disk.html)
    
    [Backup Internals: What is VSS, how does it work and why do we use it?](https://blog.macrium.com/backup-internals-what-is-vss-how-does-it-work-and-why-do-we-use-it-4e566223125a)
    
3. Provide `--inconsistent` flag if no LVM/fs found which can prevent dirty_writes through VFS, letting user know that it might create problems if using the computer while backing up.

**The Secondth Knight of Eight — Today at 1:13 AM**
maybe dmsetup
cuz it's a logical device
so you can have it as a separate device
**Lychee Juice 🤘 — Today at 1:14 AM**
🤔
**The Secondth Knight of Eight — Today at 1:14 AM**
and then have your backup program read the actual real device
so it's cached at logical level but the backup program bypasses that level
**Lychee Juice 🤘 — Today at 1:14 AM**
hn, sounds right
I'll read about it
idk if I can do it without performing a reboot, but concept seems gre8

### Fancy syntax description explanation arrow pointing

Misc: 

[File system drivers (Part 1) - The Linux Kernel documentation](https://linux-kernel-labs.github.io/refs/heads/master/labs/filesystems_part1.html)

```rust
use std::path::PathBuf;
use block_utils::{self, BlockUtilsError, Device, MediaType};

fn get_blocks() -> Result<Vec<Device>, BlockUtilsError> {
    Ok(block_utils::get_all_device_info_iter(block_utils::get_block_devices()?)?
        .filter_map(|x| x.ok())
        .filter(|x| x.capacity != 0)
        .filter(|x| x.media_type != MediaType::Ram)
        .collect())
}

fn get_corresponding_partitions(block_device_path: &Vec<String>) -> Vec<Vec<PathBuf>> {
    block_device_path.iter()
        .map(|x| block_utils::get_children_devpaths_from_path(x).unwrap_or_else(|_| vec![]))
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::File;
    use block_utils::{self, BlockUtilsError};
    use gptman::GPT;

    #[test]
    fn print_disks() -> Result<(), BlockUtilsError> {
        let block_devices = get_blocks()?;
        let block_paths = block_devices.iter().map(|x| format!("/dev/{}", x.name)).collect::<Vec<String>>();
        let part = get_corresponding_partitions(&block_paths);

        let blockp1 = &block_paths[0];

        let mut f = File::open(blockp1).unwrap();
        let gpt = GPT::find_from(&mut f).unwrap();

        // Use VFS to read generic fs, write into tar + metadata in a file
        //
        // mkdir /var/run/appname
        // mount /dev/block /var/run/appname

        println!("{block_devices:#?}");
        println!("{part:#?}");
        println!("{block_paths:#?}");
        println!("{gpt:#?}");

        Ok(())
    }
}`
```