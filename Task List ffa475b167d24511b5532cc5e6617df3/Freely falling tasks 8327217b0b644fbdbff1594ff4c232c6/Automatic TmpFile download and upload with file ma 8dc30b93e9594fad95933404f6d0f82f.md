# Automatic TmpFile download and upload with file manager upload dialog and pasting of url. Possibly stream using RAM (/tmp).

Done: No
Duration: Week
Tags: Archived, Low Priority, Stale

## Potential helps

[Curl to download and upload same file simultaneously](https://stackoverflow.com/questions/4880486/curl-to-download-and-upload-same-file-simultaneously)

[create 'virtual file' from bash command output?](https://serverfault.com/a/40285/960542)

```bash
#!/usr/bin/env bash

# Uploads a file streamed from a web URL

url="$1"
file="/tmp/$(sed 's/[:/\.]/_/g' <<< $url).fifo"

reset() {
    [ -p "$file" -o -f "$file" ] && rm "$file"
}
trap reset SIGINT
trap reset SIGTERM

reset
mkfifo "$file"
echo "$file" | xclip -sel clip
curl -L "$url" -o "$file" -s
echo -n "" | xclip -sel clip
reset
```

## Follow Ups

[](https://superuser.com/questions/1263014/allow-remote-urls-in-dolphin-open-file-dialog)

[Using the GTK file chooser to browse a remote filesystem?](https://stackoverflow.com/questions/55462739/using-the-gtk-file-chooser-to-browse-a-remote-filesystem)

### Older Discussions / Resources

[Bug 705755 - Allow applications to pass a list of allowed protocols to gtkfilechooser](https://bugzilla.gnome.org/show_bug.cgi?id=705755)

[Would you like to see support for remote file URLs in GTK file chooser?](https://www.reddit.com/r/linux/comments/26ilpv/would_you_like_to_see_support_for_remote_file/)

[Gimp-Matting/INSTALL at master · rggjan/Gimp-Matting](https://github.com/rggjan/Gimp-Matting/blob/master/INSTALL#L62-L68)

[gvfs: GIO virtual file system - Linux Man Pages (7)](https://www.systutorials.com/docs/linux/man/7-gvfs)

[Linux file descriptor - getting current redirection stdout file?](https://stackoverflow.com/questions/40270315/linux-file-descriptor-getting-current-redirection-stdout-file)