# CTP mini project

Deadline: March 6, 2022 → April 3, 2022
Done: Yes
Duration: More than Month
Status: Archived
Tags: Assignment

<aside>
💡 **Optional Breakers (for partial play partial web pull):**
1. Cannot access the MediaSource Object from the video tag by the javascript DOM query of attributes. Will use another method by HtmlMediaElement (video tag’s type).
2. MediaRecorder literally records the playback with skips/seeks, also video and audio don’t really sync.
    substitute: to be found, probably using HtmlMediaElement.requestVideoFrameCallback, .bufferred, etc

</aside>

## Chrome Extension

**Manifest V2 vs V3**

[Introducing chrome.scripting - Chrome Developers](https://developer.chrome.com/blog/crx-scripting-api/)

**UI:** [https://developer.chrome.com/docs/extensions/mv3/options](https://developer.chrome.com/docs/extensions/mv3/options/) + [https://developer.chrome.com/docs/extensions/mv3/user_interface](https://developer.chrome.com/docs/extensions/mv3/user_interface/)
**chrome.storage:** [https://developer.chrome.com/docs/extensions/reference/storage](https://developer.chrome.com/docs/extensions/reference/storage)

# Extension (C/C++ → WASM)

1. [https://medium.com/@tdeniffel/pragmatic-compiling-from-c-to-webassembly-a-guide-a496cc5954b8](https://medium.com/@tdeniffel/pragmatic-compiling-from-c-to-webassembly-a-guide-a496cc5954b8) - Seems like Best Guide
2. [https://stackoverflow.com/questions/49611290/using-webassembly-in-chrome-extension](https://stackoverflow.com/questions/49611290/using-webassembly-in-chrome-extension)
3. [https://openhome.cc/eGossip/WebAssembly/C2Wasm.html](https://openhome.cc/eGossip/WebAssembly/C2Wasm.html)
4. [https://developer.mozilla.org/en-US/docs/WebAssembly/C_to_wasm](https://developer.mozilla.org/en-US/docs/WebAssembly/C_to_wasm) + [https://developer.mozilla.org/en-US/docs/WebAssembly/existing_C_to_wasm](https://developer.mozilla.org/en-US/docs/WebAssembly/existing_C_to_wasm)
5. [https://ihsavru.medium.com/calling-javascript-code-from-c-c-using-webassembly-a9445c11bc6d](https://ihsavru.medium.com/calling-javascript-code-from-c-c-using-webassembly-a9445c11bc6d)

# File Download and Storage

1. [https://emscripten.org/docs/api_reference/fetch.html](https://emscripten.org/docs/api_reference/fetch.html)
2. [https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API) (key-value pairs)
3. [https://developer.chrome.com/docs/extensions/reference/fileSystem](https://developer.chrome.com/docs/extensions/reference/fileSystem/)

[Out of curiosity, how does youtube-dl work?](https://www.reddit.com/r/youtubedl/comments/mh51mc/out_of_curiosity_how_does_youtubedl_work/)

# Examples

[https://github.com/petasittek/chrome-extension-starter-kit](https://github.com/petasittek/chrome-extension-starter-kit)

[https://github.com/GoogleChrome/chrome-extensions-samples](https://github.com/GoogleChrome/chrome-extensions-samples)

[https://github.com/ffmpegwasm/chrome-extension-app](https://github.com/ffmpegwasm/chrome-extension-app)

[https://github.com/jjang4001/wasm-chrome-boilerplate](https://github.com/jjang4001/wasm-chrome-boilerplate)

[https://github.com/inflatablegrade/Extension-with-WASM](https://github.com/inflatablegrade/Extension-with-WASM)

### Similar Projects

[https://github.com/mattdesl/mp4-wasm](https://github.com/mattdesl/mp4-wasm)

[https://github.com/Jeansidharta/Youtube-Playback-speed-extension](https://github.com/Jeansidharta/Youtube-Playback-speed-extension)

[https://github.com/lukejacksonn/youtube-eco](https://github.com/lukejacksonn/youtube-eco)

[https://github.com/Thann/play-with-mpv](https://github.com/Thann/play-with-mpv)

Injection

[https://github.com/ben174/rick-roulette](https://github.com/ben174/rick-roulette)

## Snippets

[chrome-extensions-samples/background.js at main · GoogleChrome/chrome-extensions-samples](https://github.com/GoogleChrome/chrome-extensions-samples/blob/main/examples/hello-world/background.js)

```json
"content_scripts": [
        {
            "matches": ["http://*/*", "https://*/*", "file://*/*", "*://*/*"],
            "js": ["content-script.js"]
        }
    ],
```

```jsx
// On any youtube video @ browser console
let video = document.getElementsByTagName("video")[0];
const doSomethingWithTheFrame = (now, metadata) => {
  console.log(now, metadata);
  video.requestVideoFrameCallback(doSomethingWithTheFrame);
};
video.requestVideoFrameCallback(doSomethingWithTheFrame);
```

```jsx
chrome.extension.sendMessage({}, function () {
    if (window.location.hostname !== 'www.youtube.com') {
        // youtube only, for now
        return;
    }
    // ...
}
```

```jsx
document.getElementsByTagName("video")[0].setAttribute("src", newURL);
```

```jsx
let video = document.getElementsByTagName("video")[0];
let stream = video.captureStream(10);
let mediaRecorder = new MediaRecorder(stream);
let videoData = [];
mediaRecorder.ondataavailable = function(e) {
    videoData.push(e.data);
};
mediaRecorder.start();
function showCapture() {
  return new Promise(resolve => {
    mediaRecorder.stop();
    setTimeout(() => {
      // Wrapping this in setTimeout, so its processed in the next RAF
      video.src = window.URL.createObjectURL(new Blob(videoData));
      resolve();
    }, 0);
  });
};
```

```jsx
SourceBuffer.prototype.appendBufferOrig = SourceBuffer.prototype.appendBuffer;
SourceBuffer.prototype.appendBuffer = function (e) {
    console.log(e);
    return this.appendBufferOrig(e);
};
```

```bash
#!/bin/sh

# Track --get-url option, get inside.
# traceback.print_stack() to get the backing tree of call stack.

# https://github.com/ytdl-org/youtube-dl/issues/28289#issuecomment-789369891
http 'https://www.youtube.com/youtubei/v1/player?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8' videoId=Ji0frklnzko context:='{"client": {"clientName": "WEB", "clientVersion": "2.20201021.03.00"}}'

# Focusing properties:
# itag: format-id
# url: download able url
# contentLength: size in bytes (/1024/1024 -> MB)
# qualityLabel: displayable label for quality
# mimeType: mime ;; extraction regex: '((?:[^\/]+)\/(?:[^;]+))(?:;\s*codecs="([^"]+)")?'  ;; parsing codecs: https://github.com/ytdl-org/youtube-dl/blob/6508688e88c83bb811653083db9351702cd39a6a/youtube_dl/utils.py#L4217-L4283
#     e.g. 'video/mp4; codecs="avc1.4d401f"'           136
#     or 'video/mp4; codecs="avc1.64001F, mp4a.40.2"'  22

# Note: Youtube throttles chunks >~10M, if only_video or only_audio, solution: 'http_chunk_size': 10485760

http 'https://youtube.googleapis.com/youtube/v3/playlistItems?part=contentDetails&playlistId=PLh9FKPSq4kuSCuol1U3gki3zOH7gGDPCD&key=AIzaSyD9aedfJUSROqYb2Ku8JEHgfyKDA3ZKo7U' 'Authorization:Bearer [TOKEN]'
```

[MediaSource Example with XHR](https://developer.mozilla.org/en-US/docs/Web/API/MediaSource) + [MediaRecorderAPI](https://developer.mozilla.org/en-US/docs/Web/API/MediaRecorder)

How to use JS class from C/C++/WASM

[val.h - Emscripten 3.1.6-git (dev) documentation](https://emscripten.org/docs/api_reference/val.h.html)

[https://emscripten.org/docs/porting/connecting_cpp_and_javascript/Interacting-with-code.html#calling-javascript-functions-as-function-pointers-from-c](https://emscripten.org/docs/porting/connecting_cpp_and_javascript/Interacting-with-code.html#calling-javascript-functions-as-function-pointers-from-c)

```bash
ChannelID
GET https://youtube.googleapis.com/youtube/v3/channels?part=id&mine=true&key=[YOUR_API_KEY] HTTP/1.1

Authorization: Bearer [YOUR_ACCESS_TOKEN]
Accept: application/json

```

### Resources

[https://www.youtube.com/watch?v=wtKounNCnkc](https://www.youtube.com/watch?v=wtKounNCnkc)

[How to write a Makefile with separate source and header directories?](https://stackoverflow.com/a/30602701/11377112)

[https://www.w3.org/TR/media-source-2/](https://www.w3.org/TR/media-source-2/)

## Discussions on forums (w/ possible concerns)

> Does youtube use mediasource?
YouTube uses **HTML5 Media Source Extensions (MSE)**
 to control how data is delivered to the browser. ... This technique is called adaptive bitrate streaming and is enabled in YouTube by the MPEG-DASH multi-bitrate media format.
> 
> 
> [How video streaming works on the web: An introduction](https://medium.com/canal-tech/how-video-streaming-works-on-the-web-an-introduction-7919739f7e1)
> 
> [If Blob URLs are immutable, how does Media Source Extension API use them to stream videos?](https://stackoverflow.com/questions/54613972/if-blob-urls-are-immutable-how-does-media-source-extension-api-use-them-to-stre)
> 
> Actually how youtube plays:
> 
> [What is blob in the ?](https://stackoverflow.com/questions/33389001/what-is-blob-in-the-video-src-bloburl)
> 

[Loading chunks into html5 video](https://stackoverflow.com/questions/32789417/loading-chunks-into-html5-video)

[HTML5 Video - Percentage Loaded?](https://stackoverflow.com/questions/5029519/html5-video-percentage-loaded)

[https://stackoverflow.com/questions/32699721/javascript-extract-video-frames-reliably](https://stackoverflow.com/questions/32699721/javascript-extract-video-frames-reliably)

[https://stackoverflow.com/questions/69812557/how-does-loading-html5-video-chunks-actually-work](https://stackoverflow.com/questions/69812557/how-does-loading-html5-video-chunks-actually-work)

[Receiving a video parts and append them to a MediaSource using javascript](https://stackoverflow.com/questions/51523512/receiving-a-video-parts-and-append-them-to-a-mediasource-using-javascript?rq=1)

Video/Audio Format in Chrome’s Network tab: XHR fragmentation (something under dash player)

Reuse of currently owned data in browser (MediaRecorder). Also another way is to *somehow* link to the data preview of chrome’s network recorder (Ct-Sh-i).

[How to download (or get as a blob) a MediaSource used for streaming HTML5 video?](https://stackoverflow.com/questions/50938089/how-to-download-or-get-as-a-blob-a-mediasource-used-for-streaming-html5-video)

Secure: [https://groups.google.com/a/chromium.org/g/chromium-extensions/c/5DJHT9TCy5M](https://groups.google.com/a/chromium.org/g/chromium-extensions/c/5DJHT9TCy5M)

To find: how to extract bufferrred video content from a video html

OR: how to extract bufferrred video content into bytearray from a video html

[https://github.com/gildas-lormeau/SingleFile/issues/565](https://github.com/gildas-lormeau/SingleFile/issues/565)

[Google OAuth2 and Chrome Extensions(MV2) - Gain Access to Your User's Google Data](https://www.youtube.com/watch?v=5grkEnYytF4)

```json
// MV3 Migration (later, not now)

"host_permissions": ["https://youtube.com", "https://youtube.googleapis.com", "https://*.googlevideo.com"],
```

[How to pass a string to C code compiled with emscripten for WebAssembly](https://stackoverflow.com/questions/46815205/how-to-pass-a-string-to-c-code-compiled-with-emscripten-for-webassembly)

[chrome.tabs - Chrome Developers](https://developer.chrome.com/docs/extensions/reference/tabs/#event-onUpdated)

[emscripten/library_html5.js at dba6fd1b678c05e3c37b31f12f79f1867a3823e0 · emscripten-core/emscripten](https://github.com/emscripten-core/emscripten/blob/dba6fd1b678c05e3c37b31f12f79f1867a3823e0/src/library_html5.js#L277)

[Does the chrome.identity api work in chrome extensions in brave? · Issue #7693 · brave/brave-browser](https://github.com/brave/brave-browser/issues/7693#issuecomment-744061175)