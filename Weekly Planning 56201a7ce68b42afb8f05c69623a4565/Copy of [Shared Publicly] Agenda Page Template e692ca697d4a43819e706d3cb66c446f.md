# Copy of [Shared Publicly] Agenda Page Template

# Weekly Agenda |

- Calendar View
    
    [Calendar](Copy%20of%20%5BShared%20Publicly%5D%20Agenda%20Page%20Template%20e692ca697d4a43819e706d3cb66c446f/Calendar%20714840dcdd0e4434a0ab49a283d76e24.csv)
    

# Make Time

- Guidance
    
    <aside>
    💡 Highlight - Something I will focus and get done today for sure (I look forward to this) | Grateful - Something I'm grateful for today | Let go - Something I need to let go
    
    </aside>
    

---

## MONDAY

---

- Highlight:
- Grateful:
- Let go:

## TUESDAY

---

- Highlight:
- Grateful:
- Let go:

## WEDNESDAY

---

- Highlight:
- Grateful:
- Let go:

## THURSDAY

---

- Highlight:
- Grateful:
- Let go:

## FRIDAY

---

- Highlight:
- Grateful:
- Let go:

## SATURDAY

---

- Highlight:
- Grateful:
- Let go:

## SUNDAY

---

- Highlight:
- Grateful:
- Let go:

## Routine

---

- [ ]  Meditate
- [ ]  Stretch
- [ ]  Read

---

## Weekly Action Items

Things that need to be done this week

- [ ]  To-do

## Daily To-do's!

Things that need to be done today!

- [ ]  

[Archived](Copy%20of%20%5BShared%20Publicly%5D%20Agenda%20Page%20Template%20e692ca697d4a43819e706d3cb66c446f/Archived%205c2986a390cc433381cd4c24fa2e8566.md)

---