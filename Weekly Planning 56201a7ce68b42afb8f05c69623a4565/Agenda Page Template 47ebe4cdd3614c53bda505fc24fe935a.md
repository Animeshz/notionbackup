# Agenda Page Template

# Weekly Agenda |

- Calendar View
    
    [Calendar](Agenda%20Page%20Template%2047ebe4cdd3614c53bda505fc24fe935a/Calendar%20d986248d87d046d8b5b3c60ac4f97169.csv)
    

---

## Weekly Action Items

Things that need to be done this week

## Daily To-do's!

Things that need to be done today!

- [ ]  

[Archived](Agenda%20Page%20Template%2047ebe4cdd3614c53bda505fc24fe935a/Archived%202ecaace1a1d847f99ce3f5ca527fe5de.md)

---