# Customization

## Gtk Theme/Icons

**Full Theme**

[Skeuos Gtk Theme](https://www.gnome-look.org/p/1441725)

[Fluent Gtk Theme](https://www.gnome-look.org/p/1477941)

[MacOS White Sur Theme](https://www.gnome-look.org/p/1403328)

[Flat Remix Gtk Theme](https://www.gnome-look.org/p/1214931)

[Qogir Theme](https://www.gnome-look.org/p/1230631)

[Kripton Theme](https://www.gnome-look.org/p/1365372)

**Icon Theme**

[Flat Remix Icon Theme](https://www.opendesktop.org/p/1012430)

## Wallpapers

[https://www.deviantart.com/arsenixc/art/Street2-night-786418635](https://www.deviantart.com/arsenixc/art/Street2-night-786418635)