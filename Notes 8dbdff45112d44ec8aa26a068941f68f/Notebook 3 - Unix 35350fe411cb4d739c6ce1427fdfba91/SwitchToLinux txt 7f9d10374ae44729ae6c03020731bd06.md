# SwitchToLinux.txt

### To be explored

1. Deepin Desktop
2. Cutefish Desktop
3. Implement a few stuffs from AdvanOs concept
4. Scripts @ [https://github.com/ricobl/dotfiles/tree/master/bin](https://github.com/ricobl/dotfiles/tree/master/bin) + https://github.com/makccr/dot

Wox -> RoFi/dmenu [https://www.youtube.com/watch?v=a2GWqF32U8Q](https://www.youtube.com/watch?v=a2GWqF32U8Q)
Terminal Color Scheme -> Gogh
Adobe CC -> [https://youtu.be/YSIDfyxK6Ig](https://youtu.be/YSIDfyxK6Ig) [https://github.com/Gictorbit](https://github.com/Gictorbit) + Gimp/[https://krita.org/en/features/highlights](https://krita.org/en/features/highlights)
Notepad++ -> notepadqq, nvim, vscode(vscodium?), doom-emacs
Explorer -> ranger (shell based) or gui (thunar/nautilus/nemo?).
[https://github.com/jlevy/the-art-of-command-line](https://github.com/jlevy/the-art-of-command-line)[https://github.com/dylanaraps](https://github.com/dylanaraps)

Make script for auto pull ofupstream repo on bootup
Push to hdd (make remote repo)
Try linux snapshot feature.

Script for getting file size from download header.
[https://discord.com/channels/635612648934735892/635625954219261982/878487770178396231](https://discord.com/channels/635612648934735892/635625954219261982/878487770178396231)[https://distrochooser.de/](https://distrochooser.de/)

Coloring/Styling in bash:
[https://stackoverflow.com/questions/2924697/how-does-one-output-bold-text-in-bash](https://stackoverflow.com/questions/2924697/how-does-one-output-bold-text-in-bash)[https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux](https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux)

Fingerprint login - I guess not supported EgisTech EH575 (1c7a:0575).
Do reverse engineering - decompile driver and analyse the code, analyse memory read & writes while scanning fingerprint, use windows api for communication with the scanner. Find more resources and tools used for such in the web. Also find out how egis570 was made, its efforts.

Touchpad Pinch-Zoom & gestures:
[https://bill.harding.blog/2021/06/06/linux-touchpad-like-macbook-update-touchpad-gestures-land-to-qt-gimp-and-x-server/](https://bill.harding.blog/2021/06/06/linux-touchpad-like-macbook-update-touchpad-gestures-land-to-qt-gimp-and-x-server/)[https://www.reddit.com/r/Ubuntu/comments/oknz0d/how_to_get_touchpad_gesture_support_in_ubuntu_2004/](https://www.reddit.com/r/Ubuntu/comments/oknz0d/how_to_get_touchpad_gesture_support_in_ubuntu_2004/)[https://www.reddit.com/r/firefox/comments/mui5sy/still_no_smooth_pinchzooming_option_for_those/](https://www.reddit.com/r/firefox/comments/mui5sy/still_no_smooth_pinchzooming_option_for_those/)

Dotfile management:
[https://www.reddit.com/r/commandline/comments/g3agw6/pilgo_manage_your_dotfiles_using_a_configuration](https://www.reddit.com/r/commandline/comments/g3agw6/pilgo_manage_your_dotfiles_using_a_configuration)[https://github.com/shade-linux/soul/blob/main/soul](https://github.com/shade-linux/soul/blob/main/soul)
yadm - but concerned about syncing files outside $HOME, and installing some scripts and programs. Although encryption is OP.

- [https://github.com/TheLocehiliosan/yadm/issues/93](https://github.com/TheLocehiliosan/yadm/issues/93)

[https://www.youtube.com/watch?v=Mk_puq8yynE](https://www.youtube.com/watch?v=Mk_puq8yynE) - resize efi windows w/o data loss

For bash, coding style and best practises:
[https://github.com/icy/bash-coding-style](https://github.com/icy/bash-coding-style) + [https://github.com/dylanaraps/pure-bash-bible](https://github.com/dylanaraps/pure-bash-bible)

Some resources:
[https://stackoverflow.com/questions/17890904/how-do-you-select-text-in-vim](https://stackoverflow.com/questions/17890904/how-do-you-select-text-in-vim) vim selection
[https://www.youtube.com/watch?v=Ydzw70SvF-g](https://www.youtube.com/watch?v=Ydzw70SvF-g) - best vim visual-block mode tutorial
[https://dev.to/l04db4l4nc3r/herbstluftwm-a-dead-simple-window-manager-48ao](https://dev.to/l04db4l4nc3r/herbstluftwm-a-dead-simple-window-manager-48ao) - Use herbstluftwm dead simple

[https://www.youtube.com/watch?v=GKviflL9XeI](https://www.youtube.com/watch?v=GKviflL9XeI) - gre8 entry to tiling wm
[https://gist.github.com/MohamedAlaa/2961058](https://gist.github.com/MohamedAlaa/2961058) - TMux
[https://www.youtube.com/watch?v=dtuy09mqBPI](https://www.youtube.com/watch?v=dtuy09mqBPI) - startx
[https://www.youtube.com/watch?v=pouX5VvX0_Q](https://www.youtube.com/watch?v=pouX5VvX0_Q) - install wm & xorg
[https://www.youtube.com/watch?v=C2a7jJTh3kU](https://www.youtube.com/watch?v=C2a7jJTh3kU) - fish shell
[https://en.wikipedia.org/wiki/Man_page#Manual_sections](https://en.wikipedia.org/wiki/Man_page#Manual_sections) - Man page sections
[https://github.com/arcolinux/arcolinux-bspwm/blob/master/etc/skel/.config/bspwm/sxhkd/sxhkdrc](https://github.com/arcolinux/arcolinux-bspwm/blob/master/etc/skel/.config/bspwm/sxhkd/sxhkdrc) - sample config kb
[https://www.youtube.com/watch?v=_wcrytoLufA](https://www.youtube.com/watch?v=_wcrytoLufA) - i3lock + xidlehook
[https://stackoverflow.com/questions/47503734/what-does-printf-033c-mean](https://stackoverflow.com/questions/47503734/what-does-printf-033c-mean) - ANSI escape codes for terminal colors & redraw sequences

[https://www.reddit.com/r/bspwm/comments/gfyo83/spawn_new_workspacesdesktops_dynamically/](https://www.reddit.com/r/bspwm/comments/gfyo83/spawn_new_workspacesdesktops_dynamically/)[https://gitlab.com/thecashewtrader/dotfiles-2/-/tree/main/.scripts](https://gitlab.com/thecashewtrader/dotfiles-2/-/tree/main/.scripts) - useful scripts
[https://github.com/sdushantha/dotfiles/blob/master/bin/bin/utils/ocr](https://github.com/sdushantha/dotfiles/blob/master/bin/bin/utils/ocr) - ocr script
A color picking script as well, w/keybinding?
[https://www.reddit.com/r/unixporn/comments/obnfi6/i3gaps_just_allout_on_the_nord](https://www.reddit.com/r/unixporn/comments/obnfi6/i3gaps_just_allout_on_the_nord) - top-left of the bar, opened apps.
Animations - [https://askubuntu.com/questions/699579/ascii-screensaver-for-the-command-line-or-a-tui](https://askubuntu.com/questions/699579/ascii-screensaver-for-the-command-line-or-a-tui) and [https://askubuntu.com/questions/699159/ascii-animations-that-are-viewed-in-the-command-line](https://askubuntu.com/questions/699159/ascii-animations-that-are-viewed-in-the-command-line)
Rofi/Dmenu easy to open files [https://youtu.be/VKSxDWLLfn4?t=3m43s](https://youtu.be/VKSxDWLLfn4?t=3m43s)[https://imgur.com/gallery/bZHDF#aaosiZ2](https://imgur.com/gallery/bZHDF#aaosiZ2) - floating polybar?
[https://www.youtube.com/watch?v=qXKM5K95WIc](https://www.youtube.com/watch?v=qXKM5K95WIc) - bspwm setup ez
[https://www.reddit.com/r/unixporn/comments/ofj2c1/qtile_my_first_ever_rice_the_unicode_arrow_is_the](https://www.reddit.com/r/unixporn/comments/ofj2c1/qtile_my_first_ever_rice_the_unicode_arrow_is_the) - rofi config
[https://github.com/adrian5/oceanic-next-vim](https://github.com/adrian5/oceanic-next-vim) - colorscheme
[https://discord.com/channels/635612648934735892/635625917623828520/863113516810895360](https://discord.com/channels/635612648934735892/635625917623828520/863113516810895360) - bottom coloscheme
[https://github.com/siduck76/dotfiles/tree/master/wall](https://github.com/siduck76/dotfiles/tree/master/wall) - maybe some wallpapers?
[https://github.com/sineto/vpsm](https://github.com/sineto/vpsm)[https://www.reddit.com/r/termux/comments/ad4bei/tmuxhtop_cmus_cmatrix_cava](https://www.reddit.com/r/termux/comments/ad4bei/tmuxhtop_cmus_cmatrix_cava) - tmux keybinds
[https://www.reddit.com/r/i3wm/comments/4z2rww/rofi_google_search](https://www.reddit.com/r/i3wm/comments/4z2rww/rofi_google_search) / [https://github.com/fogine/rofi-search](https://github.com/fogine/rofi-search) - rofi search
[https://www.reddit.com/r/bspwm/comments/etfm3m/for_those_who_like_tabs](https://www.reddit.com/r/bspwm/comments/etfm3m/for_those_who_like_tabs) - tab integration with bspwm
[https://www.reddit.com/r/unixporn/comments/g8ycuz/oc_polybar_polywidgets_seen_something_similar_on](https://www.reddit.com/r/unixporn/comments/g8ycuz/oc_polybar_polywidgets_seen_something_similar_on) - polywidgets (prolly better use eww, but just for refs) also that guy has a lot of things :P (like [https://www.reddit.com/r/unixporn/comments/nqri3y/openbox_did_someone_say_widgets](https://www.reddit.com/r/unixporn/comments/nqri3y/openbox_did_someone_say_widgets))
[https://www.reddit.com/r/bspwm/comments/iwcwte/bspwm_empty_desktop_mouse_click](https://www.reddit.com/r/bspwm/comments/iwcwte/bspwm_empty_desktop_mouse_click) / [https://github.com/baskerville/bspwm/issues/595-](https://github.com/baskerville/bspwm/issues/595-) handle right clicks on desktop
[https://github.com/archcraft-os/archcraft-style-bspwm](https://github.com/archcraft-os/archcraft-style-bspwm) - archcraft bspwm setup
[https://github.com/Chrysostomus/bspwm-scripts/blob/c5041d83742d04c1afe73abc90a32c527c8a9eec/configs/sxhkd/sxhkdrc](https://github.com/Chrysostomus/bspwm-scripts/blob/c5041d83742d04c1afe73abc90a32c527c8a9eec/configs/sxhkd/sxhkdrc) - dynamic workspaces (scroll/3-finger-swipe) + ctx menu?
[https://github.com/phillbush/xmenu](https://github.com/phillbush/xmenu) + [https://github.com/OliverLew/xdg-xmenu](https://github.com/OliverLew/xdg-xmenu) - ctx menu
[https://github.com/xero/figlet-fonts](https://github.com/xero/figlet-fonts), cmatsuoka/figlet, ascii-boxes/boxes - ASCII art from text (font-imposable)
[https://github.com/agarrharr/awesome-cli-apps](https://github.com/agarrharr/awesome-cli-apps)[https://gist.github.com/takase1121/57bfa9ca3fffff33408dde08b0e6269a](https://gist.github.com/takase1121/57bfa9ca3fffff33408dde08b0e6269a) - vmtools
[https://www.reddit.com/r/unixporn/comments/ohb9y9/oc_awesomewm_concept](https://www.reddit.com/r/unixporn/comments/ohb9y9/oc_awesomewm_concept) -> [https://www.reddit.com/r/unixporn/comments/op73tw/awesomewm_neumorphic_awesome](https://www.reddit.com/r/unixporn/comments/op73tw/awesomewm_neumorphic_awesome)[https://discord.com/channels/635612648934735892/635625917623828520/868354152043008070](https://discord.com/channels/635612648934735892/635625917623828520/868354152043008070) - vim setup
[https://www.google.com/search?hl=en-IN&tbs=simg:CAESywIJS69P-RONB-QavwILELCMpwgaOQo3CAQSE94L8jOZD2OAC6Uy2CfsHNM9uRUaGh1M8BAhSW5U4t1AHKslQ-Z2fguwXQplrRNNIAUwBAwLEI6u_1ggaCgoICAESBMhwRYUMCxCd7cEJGuABCiUKEW5hdHVyYWwgbGFuZHNjYXBl2qWI9gMMCgovbS8wM2QyOHkzCj4KK3Ryb3BpY2FsIGFuZCBzdWJ0cm9waWNhbCBjb25pZmVyb3VzIGZvcmVzdHPapYj2AwsKCS9tLzAxY3Y5bQodCgpjZyBhcnR3b3Jr2qWI9gMLCgkvbS8wMnR4MWsKJgoScmVkIHNreSBhdCBtb3JuaW5n2qWI9gMMCgovbS8wODBkazJoCjAKG3RlbXBlcmF0ZSBjb25pZmVyb3VzIGZvcmVzdNqliPYDDQoLL2cvMTIxdGJ2aHAM&q=digital+art&tbm=isch&sa=X&ved=2ahUKEwizxYK-6ozyAhVZyDgGHTdAAsMQjJkEegQIAhAC&biw=1366&bih=700](https://www.google.com/search?hl=en-IN&tbs=simg:CAESywIJS69P-RONB-QavwILELCMpwgaOQo3CAQSE94L8jOZD2OAC6Uy2CfsHNM9uRUaGh1M8BAhSW5U4t1AHKslQ-Z2fguwXQplrRNNIAUwBAwLEI6u_1ggaCgoICAESBMhwRYUMCxCd7cEJGuABCiUKEW5hdHVyYWwgbGFuZHNjYXBl2qWI9gMMCgovbS8wM2QyOHkzCj4KK3Ryb3BpY2FsIGFuZCBzdWJ0cm9waWNhbCBjb25pZmVyb3VzIGZvcmVzdHPapYj2AwsKCS9tLzAxY3Y5bQodCgpjZyBhcnR3b3Jr2qWI9gMLCgkvbS8wMnR4MWsKJgoScmVkIHNreSBhdCBtb3JuaW5n2qWI9gMMCgovbS8wODBkazJoCjAKG3RlbXBlcmF0ZSBjb25pZmVyb3VzIGZvcmVzdNqliPYDDQoLL2cvMTIxdGJ2aHAM&q=digital+art&tbm=isch&sa=X&ved=2ahUKEwizxYK-6ozyAhVZyDgGHTdAAsMQjJkEegQIAhAC&biw=1366&bih=700) - wallpapers (digital art) / [https://images.hdqwalls.com/download/the-valley-minimal-4k-9y-3840x2160.jpg](https://images.hdqwalls.com/download/the-valley-minimal-4k-9y-3840x2160.jpg)[https://www.youtube.com/watch?v=cmldp8CRNrY](https://www.youtube.com/watch?v=cmldp8CRNrY) - DWM patches and important aspects
[https://www.reddit.com/r/archlinux/comments/hlw8bc/least_ram_usage_for_archlinux_how_to_configure](https://www.reddit.com/r/archlinux/comments/hlw8bc/least_ram_usage_for_archlinux_how_to_configure) - bspwm RAM Usage
[https://gitlab.com/dwt1/dmscripts](https://gitlab.com/dwt1/dmscripts) - distro tube deployment scripts
[https://www.reddit.com/r/bspwm/comments/fwtsjg/so_i_built_myself_a_scratchpad](https://www.reddit.com/r/bspwm/comments/fwtsjg/so_i_built_myself_a_scratchpad) - scratchpad which basically pops in from a hidden workspace into current workspace by keybind without affecting tiled windows and go back.
[https://github.com/UltraNyan/rice/tree/master/Scripts](https://github.com/UltraNyan/rice/tree/master/Scripts) + [https://github.com/dylanaraps/bin](https://github.com/dylanaraps/bin) - random scripts
[https://github.com/nuxshed/dotfiles/blob/main/bin/.bin/rec](https://github.com/nuxshed/dotfiles/blob/main/bin/.bin/rec) - x11 grab and more scripts to be stolen uwu
[https://www.reddit.com/r/bspwm/comments/e9i0us/scratchpad](https://www.reddit.com/r/bspwm/comments/e9i0us/scratchpad) - bspwm scratchpad
[https://github.com/mitchweaver/bin](https://github.com/mitchweaver/bin) - some useful scripts

Polybar -> ipc module for async updates... It misses interval updates though...
[https://unix.stackexchange.com/questions/80143/how-to-create-a-daemon-which-would-be-listening-to-dbus-and-fire-script-on-messa](https://unix.stackexchange.com/questions/80143/how-to-create-a-daemon-which-would-be-listening-to-dbus-and-fire-script-on-messa)

Setup Tools:
[https://github.com/deviantfero/wpgtk](https://github.com/deviantfero/wpgtk)[https://github.com/adi1090x/polybar-themes](https://github.com/adi1090x/polybar-themes)[https://github.com/Murzchnvok/polybar-collection](https://github.com/Murzchnvok/polybar-collection)[https://github.com/addy-dclxvi/tint2-theme-collections](https://github.com/addy-dclxvi/tint2-theme-collections)[https://github.com/adi1090x/rofi](https://github.com/adi1090x/rofi)[https://github.com/adi1090x/plymouth-themes](https://github.com/adi1090x/plymouth-themes)[https://github.com/siduck76/chadwm](https://github.com/siduck76/chadwm)[https://github.com/anishathalye/dotbot](https://github.com/anishathalye/dotbot)[https://github.com/noisek/aether](https://github.com/noisek/aether)[https://www.youtube.com/watch?v=8qSeZ7ET7PQ](https://www.youtube.com/watch?v=8qSeZ7ET7PQ) - archcraft inspirations
[https://github.com/distrochooser/distrochooser](https://github.com/distrochooser/distrochooser) + [https://rizonrice.club/Rice:Resources](https://rizonrice.club/Rice:Resources) + [https://github.com/fosslife/awesome-ricing](https://github.com/fosslife/awesome-ricing)[https://github.com/Torbet/Dot-It-Up](https://github.com/Torbet/Dot-It-Up)[https://nordthemewallpapers.com/All/](https://nordthemewallpapers.com/All/)[https://github.com/QuietTheme/Xresources/blob/master/.Xresources](https://github.com/QuietTheme/Xresources/blob/master/.Xresources) - one-darcula coloscheme
[https://github.com/webpro/awesome-dotfiles](https://github.com/webpro/awesome-dotfiles) -> [https://github.com/lra/mackup](https://github.com/lra/mackup) / [https://github.com/bashdot/bashdot](https://github.com/bashdot/bashdot) / [https://github.com/pearl-core/pearl](https://github.com/pearl-core/pearl)[https://github.com/b4skyx/dotfiles](https://github.com/b4skyx/dotfiles)[https://polybar.github.io](https://polybar.github.io/)[https://themer.dev](https://themer.dev/) / [https://coolors.co](https://coolors.co/)[https://www.reddit.com/r/unixporn/comments/3aly81/where_do_you_guys_get_you_wallpapers_from](https://www.reddit.com/r/unixporn/comments/3aly81/where_do_you_guys_get_you_wallpapers_from)[https://github.com/Neeraj029/wallpapers](https://github.com/Neeraj029/wallpapers) + [https://github.com/aynp/dracula-wallpapers](https://github.com/aynp/dracula-wallpapers) - wallpapers
[https://github.com/hartwork/grub2-theme-preview](https://github.com/hartwork/grub2-theme-preview) - for screenshotting after grub customization (even for testing) :)

Inspiration/Dotfiles:
[https://github.com/saimoomedits/dotfiles](https://github.com/saimoomedits/dotfiles)[https://github.com/rxyhn/AwesomeWM-Dotfiles](https://github.com/rxyhn/AwesomeWM-Dotfiles)[https://www.reddit.com/r/unixporn/comments/tcqwrt/openbox_my_second_gruvbox_setup_with_dots/](https://www.reddit.com/r/unixporn/comments/tcqwrt/openbox_my_second_gruvbox_setup_with_dots/)[https://www.reddit.com/r/unixporn/comments/rlc8b5/kde_plasma_it_has_been_less_than_a_day_and_yet](https://www.reddit.com/r/unixporn/comments/rlc8b5/kde_plasma_it_has_been_less_than_a_day_and_yet) - Left right bottom panel to open when press Mod key
[https://www.reddit.com/r/unixporn/comments/pq8m5r/dwm_widgets_two_layout_do_you_like_light_theme](https://www.reddit.com/r/unixporn/comments/pq8m5r/dwm_widgets_two_layout_do_you_like_light_theme)[https://github.com/Barbarossa93/Genome](https://github.com/Barbarossa93/Genome)[https://github.com/obliviousofcraps/mf-dots](https://github.com/obliviousofcraps/mf-dots)[https://www.reddit.com/r/unixporn/comments/j3mfc6/i3gaps_ready_for_fall](https://www.reddit.com/r/unixporn/comments/j3mfc6/i3gaps_ready_for_fall)[https://github.com/siduck76/dotfiles](https://github.com/siduck76/dotfiles)[https://github.com/eeek76/jwm-realdots](https://github.com/eeek76/jwm-realdots)[https://www.reddit.com/r/unixporn/comments/p98l5z/bspwm_aquarium_i_spent_too_much_time_on_this](https://www.reddit.com/r/unixporn/comments/p98l5z/bspwm_aquarium_i_spent_too_much_time_on_this)[https://github.com/siduck76/chadwm](https://github.com/siduck76/chadwm) - dwm bar siduck76 rice.
[https://www.reddit.com/r/unixporn/comments/il5xjc/i3gaps_serene_dusk](https://www.reddit.com/r/unixporn/comments/il5xjc/i3gaps_serene_dusk)[https://www.reddit.com/r/unixporn/comments/o9ot4f/i3_first_tiling_wm_setup](https://www.reddit.com/r/unixporn/comments/o9ot4f/i3_first_tiling_wm_setup) - specially polybar and rofi
[https://www.reddit.com/r/unixporn/comments/o9olok/bspwm_darker_tokyodark](https://www.reddit.com/r/unixporn/comments/o9olok/bspwm_darker_tokyodark) - polybar and rofi
[https://www.reddit.com/r/unixporn/comments/ois0hx/qtile_one_wm_5_color_schemes](https://www.reddit.com/r/unixporn/comments/ois0hx/qtile_one_wm_5_color_schemes)[https://www.reddit.com/r/unixporn/comments/n7umuk/grub_my_arcade_grub_ricing](https://www.reddit.com/r/unixporn/comments/n7umuk/grub_my_arcade_grub_ricing)[https://www.reddit.com/r/unixporn/comments/oetme1/grub2_made_a_mario_theme](https://www.reddit.com/r/unixporn/comments/oetme1/grub2_made_a_mario_theme)[https://www.reddit.com/r/unixporn/comments/m5522z/grub2_had_some_fun_with_grub](https://www.reddit.com/r/unixporn/comments/m5522z/grub2_had_some_fun_with_grub)[https://www.reddit.com/r/unixporn/comments/mpgww7/grub_attack_on_titan_themes](https://www.reddit.com/r/unixporn/comments/mpgww7/grub_attack_on_titan_themes)[https://www.reddit.com/r/voidlinux/comments/gbgakb/grub_theme_for_void_from_old_forum](https://www.reddit.com/r/voidlinux/comments/gbgakb/grub_theme_for_void_from_old_forum)[https://github.com/akshat46/FlyingFox](https://github.com/akshat46/FlyingFox)[https://www.reddit.com/r/unixporn/comments/bgeeof/bspwm_new_kernel_new_rice](https://www.reddit.com/r/unixporn/comments/bgeeof/bspwm_new_kernel_new_rice) - everything :P
[https://www.reddit.com/r/unixporn/comments/ocasfa/qtile_with_palenight_color_scheme](https://www.reddit.com/r/unixporn/comments/ocasfa/qtile_with_palenight_color_scheme)[https://www.reddit.com/r/unixporn/comments/haogs0/bspwm_i_have_spent_too_much_time_on_this](https://www.reddit.com/r/unixporn/comments/haogs0/bspwm_i_have_spent_too_much_time_on_this)[https://www.reddit.com/r/unixporn/comments/hv4j8u/oc_archcraft_a_minimalistic_linux_distribution](https://www.reddit.com/r/unixporn/comments/hv4j8u/oc_archcraft_a_minimalistic_linux_distribution)[https://www.reddit.com/r/unixporn/comments/obak1a/awesome_still_in_love_with_alpine](https://www.reddit.com/r/unixporn/comments/obak1a/awesome_still_in_love_with_alpine) - topbar
[https://www.reddit.com/r/unixporn/comments/kk63vc/bspwm_gruvbox_bspwm](https://www.reddit.com/r/unixporn/comments/kk63vc/bspwm_gruvbox_bspwm) - polybar underline, Add a wpm meter in polybar?
[https://github.com/sherubthakur/dotfiles](https://github.com/sherubthakur/dotfiles) - rofi config
[https://www.reddit.com/r/unixporn/comments/ojfpxz/dwm_simple_satisfied](https://www.reddit.com/r/unixporn/comments/ojfpxz/dwm_simple_satisfied) - rofi and scripts
[https://github.com/Axarva/dotfiles-2.0](https://github.com/Axarva/dotfiles-2.0) - steal some keybindings :P
[https://cdn.discordapp.com/attachments/635625917623828520/865177532381659136/simplescreenrecorder-2021-07-15_15.36.36.mp4](https://cdn.discordapp.com/attachments/635625917623828520/865177532381659136/simplescreenrecorder-2021-07-15_15.36.36.mp4) - inactive window dimming (siduck76)
[https://discord.com/channels/635612648934735892/635625917623828520/804584815906258965](https://discord.com/channels/635612648934735892/635625917623828520/804584815906258965) - RoFi, polybar
[https://cdn.discordapp.com/attachments/635625758412505093/866231694402256936/shot.png](https://cdn.discordapp.com/attachments/635625758412505093/866231694402256936/shot.png) - lightdm theme (in setup tools above)
[https://cdn.discordapp.com/attachments/635625917623828520/866110998032023582/duminica_18_iulie_2021_014231_0300.png](https://cdn.discordapp.com/attachments/635625917623828520/866110998032023582/duminica_18_iulie_2021_014231_0300.png) - polybar (disk)
[https://github.com/TechnicalDC/dotfiles](https://github.com/TechnicalDC/dotfiles) - rofi + most of bspwm config / [https://www.youtube.com/watch?v=t_Z5DfNz_dg](https://www.youtube.com/watch?v=t_Z5DfNz_dg)[https://www.reddit.com/r/i3wm/comments/l5p095/a_small_wifi_menu_for_myself](https://www.reddit.com/r/i3wm/comments/l5p095/a_small_wifi_menu_for_myself) /  [https://life-prog.com/tech/a-simple-wifi-menu-with-rofi-on-i3/](https://life-prog.com/tech/a-simple-wifi-menu-with-rofi-on-i3/) + [https://github.com/zbaylin/rofi-wifi-menu/blob/master/rofi-wifi-menu.sh](https://github.com/zbaylin/rofi-wifi-menu/blob/master/rofi-wifi-menu.sh) - wifi on rofi?
[https://github.com/drishal/dotfiles/blob/b3a9f71749936631d90667bf1c60adf6bb2c585c/.profile](https://github.com/drishal/dotfiles/blob/b3a9f71749936631d90667bf1c60adf6bb2c585c/.profile) - some aliases?
[https://www.reddit.com/r/unixporn/comments/ov7onx/yabai_palenight_twilight](https://www.reddit.com/r/unixporn/comments/ov7onx/yabai_palenight_twilight) - rofi like music player
[https://www.reddit.com/r/unixporn/comments/ow4mmg/awesomewm_my_first_rice_colors_inspired_by_doom](https://www.reddit.com/r/unixporn/comments/ow4mmg/awesomewm_my_first_rice_colors_inspired_by_doom)[https://www.reddit.com/r/unixporn/comments/owoo27/bspwm_my_first_competition_post](https://www.reddit.com/r/unixporn/comments/owoo27/bspwm_my_first_competition_post) - colorscheme and shadows
Discord -> from: Swexti#8112 (Kouhai-kun) ==> [https://media.discordapp.net/attachments/635625925748457482/872417957345644614/unknown.png](https://media.discordapp.net/attachments/635625925748457482/872417957345644614/unknown.png) / [https://cdn.discordapp.com/attachments/635625917623828520/763103299187507274/unknown.png](https://cdn.discordapp.com/attachments/635625917623828520/763103299187507274/unknown.png) / [https://cdn.discordapp.com/attachments/635625917623828520/764922411376181279/2020-10-11-123824_1680x1050_scrot.png](https://cdn.discordapp.com/attachments/635625917623828520/764922411376181279/2020-10-11-123824_1680x1050_scrot.png) / [https://cdn.discordapp.com/attachments/635625917623828520/755719627371446332/gruvboxify_everything.png](https://cdn.discordapp.com/attachments/635625917623828520/755719627371446332/gruvboxify_everything.png) / [https://cdn.discordapp.com/attachments/635625917623828520/752083781997035571/unknown.png](https://cdn.discordapp.com/attachments/635625917623828520/752083781997035571/unknown.png) / [https://cdn.discordapp.com/attachments/635625917623828520/749335086259765328/cream.png](https://cdn.discordapp.com/attachments/635625917623828520/749335086259765328/cream.png) / [https://cdn.discordapp.com/attachments/635625917623828520/748201210628145152/2020-08-26-170239_1680x1050_scrot.png](https://cdn.discordapp.com/attachments/635625917623828520/748201210628145152/2020-08-26-170239_1680x1050_scrot.png) / [https://cdn.discordapp.com/attachments/635625917623828520/746796058083852418/openbox.jpg](https://cdn.discordapp.com/attachments/635625917623828520/746796058083852418/openbox.jpg) /
[https://www.reddit.com/r/unixporn/comments/p1yzqo/xfce_nord/](https://www.reddit.com/r/unixporn/comments/p1yzqo/xfce_nord/) - rofi
[https://github.com/MCotocel/dotfiles](https://github.com/MCotocel/dotfiles)[https://www.reddit.com/r/unixporn/comments/ovqod4/openbox_not_sure_how_ricing_works_but_i_figured_i](https://www.reddit.com/r/unixporn/comments/ovqod4/openbox_not_sure_how_ricing_works_but_i_figured_i) - bar and wallpaper
[https://www.reddit.com/r/UsabilityPorn/comments/oz0s85/sway_like_wayland/](https://www.reddit.com/r/UsabilityPorn/comments/oz0s85/sway_like_wayland/)[https://github.com/urielzo/polybar-theme-for-bspwm](https://github.com/urielzo/polybar-theme-for-bspwm) - that widget like polybar dropdown
[https://github.com/jayden-chan/dotfiles](https://github.com/jayden-chan/dotfiles) - rofi styling
[https://www.reddit.com/r/unixporn/comments/ozqowb/qtile_why_not](https://www.reddit.com/r/unixporn/comments/ozqowb/qtile_why_not) - OP compeition post (almost all, but specially starship shell config)
[https://github.com/rayes0/dotfiles](https://github.com/rayes0/dotfiles) - sidebar and calendar
[https://github.com/paulirish/dotfiles](https://github.com/paulirish/dotfiles) - 2020 updates
[https://www.reddit.com/r/unixporn/comments/p569tt/leftwm_cygnus_theme](https://www.reddit.com/r/unixporn/comments/p569tt/leftwm_cygnus_theme) - rofi (theme like wox)
[https://www.reddit.com/r/unixporn/comments/p42g3l/i3_simple_man_rice_one_dark_nord_dracula_mixture/](https://www.reddit.com/r/unixporn/comments/p42g3l/i3_simple_man_rice_one_dark_nord_dracula_mixture/)[https://www.reddit.com/r/unixporn/comments/p5h6ic/sway_clean/](https://www.reddit.com/r/unixporn/comments/p5h6ic/sway_clean/) - bar placement only
[https://www.reddit.com/r/unixporn/comments/p6tyf4/bspwm_us_and_them](https://www.reddit.com/r/unixporn/comments/p6tyf4/bspwm_us_and_them)[https://cdn.discordapp.com/attachments/635625917623828520/864910945235107850/shot_2021-07-14_22-44-05.png](https://cdn.discordapp.com/attachments/635625917623828520/864910945235107850/shot_2021-07-14_22-44-05.png) / [https://github.com/AtifChy/dotfiles](https://github.com/AtifChy/dotfiles) - overpowered xmonad setup
[https://github.com/Barbarossa93/Forester](https://github.com/Barbarossa93/Forester) - Tree neofetch
[https://github.com/ChocolateBread799/sidebar](https://github.com/ChocolateBread799/sidebar) - eww bar
[https://github.com/addy-dclxvi/almighty-dotfiles](https://github.com/addy-dclxvi/almighty-dotfiles) - almighty really, collection of config files
[https://www.reddit.com/r/unixporn/comments/pckabs/xfceawesomewm_spent_a_little_too_much_time_on_the](https://www.reddit.com/r/unixporn/comments/pckabs/xfceawesomewm_spent_a_little_too_much_time_on_the) - rofi :)
[https://discord.com/channels/635612648934735892/635625925748457482/883003365888507974](https://discord.com/channels/635612648934735892/635625925748457482/883003365888507974)[https://github.com/creio/dots](https://github.com/creio/dots) - material icons on bar
[https://github.com/tam-carre/dotfiles](https://github.com/tam-carre/dotfiles) - some kinetic movements and productivity
[https://github.com/totoro-ghost/dotfiles-up/tree/master/.config/rofi](https://github.com/totoro-ghost/dotfiles-up/tree/master/.config/rofi) - rofi scripts, specially shortcuts folder
[https://www.reddit.com/r/UsabilityPorn/comments/pv14jh/i3gaps_usable_and_beautiful_you_decide](https://www.reddit.com/r/UsabilityPorn/comments/pv14jh/i3gaps_usable_and_beautiful_you_decide)[https://www.reddit.com/r/UsabilityPorn/comments/pzm0qa/the_setup_i_use_for_my_daily_work_im_loving_the/](https://www.reddit.com/r/UsabilityPorn/comments/pzm0qa/the_setup_i_use_for_my_daily_work_im_loving_the/)[https://github.com/angelofallars/dotfiles](https://github.com/angelofallars/dotfiles) - bar and logout ([https://cdn.discordapp.com/attachments/635625917623828520/893042005591076884/unknown.png](https://cdn.discordapp.com/attachments/635625917623828520/893042005591076884/unknown.png))
[https://discord.com/channels/635612648934735892/635625917623828520/892962041642516530](https://discord.com/channels/635612648934735892/635625917623828520/892962041642516530) - exactly the bar
[https://www.reddit.com/r/unixporn/comments/qdeav6/awesomewm_fruity_awesome](https://www.reddit.com/r/unixporn/comments/qdeav6/awesomewm_fruity_awesome)[https://discord.com/channels/635612648934735892/635625917623828520/905743163136806923](https://discord.com/channels/635612648934735892/635625917623828520/905743163136806923) - bar design
[https://github.com/iSparsh/eww/tree/iSparsh-example](https://github.com/iSparsh/eww/tree/iSparsh-example) - bar
[https://github.com/jonesad-etsu/awesomewm-config](https://github.com/jonesad-etsu/awesomewm-config) - bar

Bspwm configs:
[https://www.reddit.com/r/bspwm/comments/6jj6le/move_window_to_workspace_and_then_switch_to_that](https://www.reddit.com/r/bspwm/comments/6jj6le/move_window_to_workspace_and_then_switch_to_that)[https://manned.org/bspc](https://manned.org/bspc) - bspc man page (web)

Extra tools:
Productivity: sharkdp/fd, rofi, picom, polybar, rofi_et, tmux, eww, Jaredk3nt/homepage, spotify-tui, siduck76/NvChad, zathura, fish, i3lock + xidlehook, fzf & rlocate, phillbush/xmenu, phillbush/xnotify, phillbush/pmenu, sdushantha/wifi-password, Alonely0/Voila, darkreader, codesnap (vscode), bsp-layout, dylanaraps/paleta, homeshick, TheLocehiliosan/yadm, gsamokovarov/jump, charmbracelet/glow, bat, dandavison/delta, ogham/exa, git-fuzzy, paulirish/git-open, ajeetdsouza/zoxide, telescope.nvim, elkowar/pipr, sharkdp/insect, bootandy/dust, Toqozz/slip, phisch/giph, BurntSushi/xsv, jhspetersson/fselect, termapps/enquirer (dialoguer-rs to bash)
SystemInfo: neofetch (+chick2d/neofetch-themes), rxfetch, Macchina-CLI/macchina, pfetch, [fet.sh](http://fet.sh/), jfetch, Manas140/fetch, FrenzyExists/frenzch.sh, pixelb/ps_mem, htop, (gotop>ytop), (bpytop>bashtop), amirashabani/bain
Terminals: Starship (theme for any shell), Alacritty, Kitty, siduck76/st, lukesmithxyz/st ([https://www.youtube.com/watch?v=uqLcvKYl-Ms](https://www.youtube.com/watch?v=uqLcvKYl-Ms))
Music Player: lollypop, spotify, spotify-tui, Orchid, KraXen72/playlist-manager
Music Visualizers: cava, ncmpcpp, TauonMusicBox, cli-visualizer
File Managers: ranger, nnn, fff, fzf, thunar, nautilus
Themes: Nord/Nordic, Phocus, Rigellute/rigel, Kripton
Fonts: FiraCode, microsoft/cascadia-code, noto sans bold, victormono, from [https://github.com/ryanoasis/nerd-fonts](https://github.com/ryanoasis/nerd-fonts)
Icons: BeautyLine, Papirus-Dark, Nerd Fonts
Colorschemes: dracula (+https://draculatheme.com/fish), onedark, gruvbox (don't like but yeh), Aritim Dark, Ayu Dark, Ayu Mirage, Tokyo Night, Tokyo Night Storm, bluz71/vim-moonfly-colors
Wallpaper: xsetroot, hsetroot, pywal=generate-theme-based-on-images
Screenshot tools: maim, peek, flameshot, shutter
Flexing: cmatrix, [pipes.sh](http://pipes.sh/), jallbrit/cbonsai, piuccio/cowsay, lolcat (colorer to existing stdin), dwt1/shell-color-scripts, stark/Color-Scripts, felix-u/neomorphic-custom-css, jp2a, [https://github.com/khanhas/spicetify-cli](https://github.com/khanhas/spicetify-cli) + [https://github.com/morpheusthewhite/spicetify-themes/tree/master/Dribbblish](https://github.com/morpheusthewhite/spicetify-themes/tree/master/Dribbblish), elkowar/Termibbl, [https://github.com/migueravila/Bento](https://github.com/migueravila/Bento), [https://github.com/kikiklang/pomme-page](https://github.com/kikiklang/pomme-page)
Others: gping, light-locker (lightdm), Aether (lightdm), yvbbrjdr/i3lock-fancy-rapid, deadd notification centre, sassman/t-rec-rs, spotDL/spotify-downloader, ritave/xeventbind, bahamas10/vsv, [https://github.com/toluschr/xdeb](https://github.com/toluschr/xdeb) (for void)

Hardware: Ducky one 2 mini keyboard, KEYCHRON K2 V2 keyboard, Vortex Tab 75 keyboard

Awesome Peoples:

- siduck76 / eeek76
- adi1090x
- Manas140
- [https://www.reddit.com/u/woowak01](https://www.reddit.com/u/woowak01) (for herbstluftwm and OC)
- FrenzyExists ([https://www.reddit.com/r/unixporn/comments/p98l5z/bspwm_aquarium_i_spent_too_much_time_on_this](https://www.reddit.com/r/unixporn/comments/p98l5z/bspwm_aquarium_i_spent_too_much_time_on_this))
- dylanaraps

Sniffs from void wiki as its going to be down lol: Extras/Geeky/AncientBackupsForRescue/Bspwm_void_wiki.md

Some useful commands:
wc
less

My dots:
Lets name my dots - Solid, coherent - clear/logical/easy-to-understand, start with `Concept`[https://www.reddit.com/r/linuxquestions/comments/jrbe2p/laptop_battery_life_in_linux_vs_windows](https://www.reddit.com/r/linuxquestions/comments/jrbe2p/laptop_battery_life_in_linux_vs_windows) - some real watts value, also which ryzen cpu does this guy has, enquiry tomorrow?
[https://github.com/Valeyard1/dotfiles](https://github.com/Valeyard1/dotfiles)[https://cdn.discordapp.com/attachments/635625917623828520/863754807458267146/Screenshot_20210711-131252.png](https://cdn.discordapp.com/attachments/635625917623828520/863754807458267146/Screenshot_20210711-131252.png) - year in progress, day in progress
[https://cdn.discordapp.com/attachments/655947537538088962/843932745285107772/unicksporno.png](https://cdn.discordapp.com/attachments/655947537538088962/843932745285107772/unicksporno.png) - temps and date
[https://www.reddit.com/r/Polybar/comments/i8g3vd/any_module_for_monitoring_internet_speed](https://www.reddit.com/r/Polybar/comments/i8g3vd/any_module_for_monitoring_internet_speed) - network speed
Add refs to topmost of file, with comment, specially those button{1,11} in sxhkd
Force open some apps like firefox / video player in full screen mode, if doesn't exist move to next desktop start and focus there
[https://wiki.gentoo.org/wiki/ACPI/ThinkPad-special-buttons](https://wiki.gentoo.org/wiki/ACPI/ThinkPad-special-buttons) + [https://man.voidlinux.org/elogind.8](https://man.voidlinux.org/elogind.8) + [https://www.reddit.com/r/voidlinux/comments/cx15d6/funky_behavior_of_xf86_keys](https://www.reddit.com/r/voidlinux/comments/cx15d6/funky_behavior_of_xf86_keys)

[https://github.com/b4skyx/dotfiles](https://github.com/b4skyx/dotfiles) - readme like this
[https://github.com/Shivshreyas/i3wm-nord-dotfiles](https://github.com/Shivshreyas/i3wm-nord-dotfiles) - organize directory structure as this... also some rofi inspiration.
[https://github.com/Manu343726/dotfiles/blob/master/install.sh](https://github.com/Manu343726/dotfiles/blob/master/install.sh) - install script/makefile?
[https://github.com/myTerminal/dotfiles/blob/master/.setup/linux/run](https://github.com/myTerminal/dotfiles/blob/master/.setup/linux/run) - bootstrapping packages generic to distro w/o bedrock
[https://github.com/Cyclenerd/postinstall](https://github.com/Cyclenerd/postinstall) - post install script inspiration for showing debugging of what is happening
[https://www.reddit.com/r/unixporn/comments/pckabs/xfceawesomewm_spent_a_little_too_much_time_on_the/](https://www.reddit.com/r/unixporn/comments/pckabs/xfceawesomewm_spent_a_little_too_much_time_on_the/) - triple layer rofi, app: mod+;, everything: alt+space;; or maybe use one letter and space as mode selector | [https://github.com/totoro-ghost/dotfiles-up/tree/master/.config/rofi](https://github.com/totoro-ghost/dotfiles-up/tree/master/.config/rofi) more stuffs

git config --global url."[https://$GITHUB_USERNAME:$GITHUB_PASSWORD@github.com](https://$GITHUB_USERNAME:$GITHUB_PASSWORD@github.com/)".insteadOf /"[https://github.com](https://github.com/)"
ls -l ~/.gitconfig

Lockscreen and other keybinds: [https://github.com/fdietze/dotfiles/blob/master/.config/herbstluftwm/autostart](https://github.com/fdietze/dotfiles/blob/master/.config/herbstluftwm/autostart)
bootstrapping script example: [https://github.com/gumieri/dotfiles/blob/master/bin/dotfiles-setup](https://github.com/gumieri/dotfiles/blob/master/bin/dotfiles-setup)