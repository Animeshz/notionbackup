# Useful Tools

Tags: Resurface

[r/FMHY](https://www.google.com/url?sa=t&source=web&rct=j&url=https://www.reddit.com/r/FREEMEDIAHECKYEAH/&ved=2ahUKEwjZ-r3mx4f4AhXBmeYKHap5DO0QFnoECAkQAQ&usg=AOvVaw1yjAlRxOiZIUKMRXCvoeMr)

[zzollo](https://zzollo.co/)

# System Fetch

`inxi` → CPU/Mem/Storage/Kern/Uptime

`inxi -G` / `lshw -c video` / `sudo intel_gpu_top` → GPU

`rxfetch` → Cool fetch tool

Widget/Windows

[https://github.com/elkowar/eww](https://github.com/elkowar/eww)

[https://github.com/ap29600/dama-rs](https://github.com/ap29600/dama-rs)

NextGen Terminal (currently only macos)

[https://github.com/warpdotdev/Warp](https://github.com/warpdotdev/Warp)

GitHub workflow locally

[https://github.com/nektos/act](https://github.com/nektos/act)

Unified Notification API

[https://github.com/novuhq/novu](https://github.com/novuhq/novu)

Homeserver for everybody:

[https://github.com/meienberger/runtipi](https://github.com/meienberger/runtipi)

## Utilities

[https://github.com/quickemu-project/quickemu](https://github.com/quickemu-project/quickemu)

[https://github.com/p-ranav/fccf](https://github.com/p-ranav/fccf)

[https://github.com/nvbn/thefuck](https://github.com/nvbn/thefuck)

[https://github.com/IceWhaleTech/CasaOS](https://github.com/IceWhaleTech/CasaOS)

[https://github.com/Textualize/rich-cli](https://github.com/Textualize/rich-cli)

[https://github.com/leviarista/github-profile-header-generator](https://github.com/leviarista/github-profile-header-generator)

[https://github.com/Cveinnt/LetsMarkdown.com](https://github.com/Cveinnt/LetsMarkdown.com)

[https://github.com/jorgebucaran/awsm.fish](https://github.com/jorgebucaran/awsm.fish)

### Misc Utilities

[https://github.com/oguzhaninan/Stacer](https://github.com/oguzhaninan/Stacer)

[https://github.com/aleclearmind/split-patch](https://github.com/aleclearmind/split-patch)

[How to Cast Android Screen on Linux - TechWiser](https://techwiser.com/cast-android-screen-on-linux/)

rmlint - removal of duplicates

ntfy - notification

rclone (rsync over web? not sure tho)

## Non-Important Random interesting stuffs

[https://github.com/TomSmeets/FractalArt](https://github.com/TomSmeets/FractalArt)

[poise - Rust](https://docs.rs/poise/latest/poise/)

[Untitled](Useful%20Tools%20a37a661072364d6fb19faf8ad768b5c8/Untitled%20Database%200cf7240ff98b49d3a046a4e6a58b33cc.csv)