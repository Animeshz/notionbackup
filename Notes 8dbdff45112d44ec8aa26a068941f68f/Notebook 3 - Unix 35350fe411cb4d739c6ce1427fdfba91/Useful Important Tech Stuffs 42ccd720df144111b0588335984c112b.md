# Useful/Important Tech Stuffs

## **Resize/Format/Reinitialize EFI partition**

- **GParted move partitions to add up more space in efi**
    
    **If gparted error occurs →**
    
    chkdsk C: /f
    reboot windows twice
    

### Format and Reinitialize EFI

**Windows**

Boot windows installation media, Shift + F10
diskpart
list disk
select disk 0
list partition
//create partition efi size=500
select partition 1
format quick fs=fat32
assign letter=S
select partition 3
assign letter=C
list volume
exit
bcdboot C:\Windows /s S:

**Linux**

Boot to shell
mount /dev/xxx /mnt
mount /dev/xxxp1 /mnt/boot/efi
for i in /sys /proc /dev; do mount --bind "$i" "/mnt$i"; done
chroot /mnt
install-grub /dev/xxx
update-grub
blkid
vi /etc/fstab
change /boot/efi 's UUID

### Acer BIOS reflash

Extract bios.exe (7z)
platform.ini Biosversioncheck flag=0 and VersionFormat=XN.NN to XN.DD
Flash same bios

## Resize FS

```bash
sudo cfdisk /dev/sdx
# extend and write

sudo resize2fs /dev/sdxx
```

## Cross-Chroot

```bash
sudo xbps-install qemu qemu-user-static binfmt-support
sudo ln -s /etc/sv/binfmt-support /var/service
update-binfmts --enable
# update-binfmts --display | grep aarch

sudo mount /dev/sda2 /mnt/rpi
sudo mount /dev/sda1 /mnt/rpi/boot -o rw,uid=(id -u),gid=(id -g)
for i in /sys /proc /dev; do sudo mount --rbind "$i" "/mnt/rpi$i"; done

sudo chroot /mnt/rpi
```

## Fully functional chroot (incl X11)

[xchroot/xchroot at master · mosquito/xchroot](https://github.com/mosquito/xchroot/blob/master/sbin/xchroot)

## NVim

Keymapping modes

[What is the difference between the remap, noremap, nnoremap and vnoremap mapping commands in Vim?](https://stackoverflow.com/questions/3776117/what-is-the-difference-between-the-remap-noremap-nnoremap-and-vnoremap-mapping)

Dialog, and optional replace (camelCase to snake_case)

[camelCase to underscore in vi(m)](https://stackoverflow.com/a/5185839/11377112)

Make directory if not exists while opened file in buffer already

[How to make a new directory and a file in Vim](https://stackoverflow.com/a/58833213/11377112)

move cursor to next occurence of a word: 

```python
# move cursor to next/prev occurence of a word:
[fF]+<letter>  # at that letter
[tT]+<letter>  # just before that letter
# can be used in combination with d or v to delete or select respectively

# Window Split
ctrl+w s  # horizontal
ctrl+w v  # vertical
```

### Set default for mime type (url-scheme or file) - xdg-open

[How do I set a new xdg-open setting?](https://askubuntu.com/questions/62585/how-do-i-set-a-new-xdg-open-setting)

## RaspberryPi Repeater/Router (bond uplink)

[Stackexchange Tutorial](https://raspberrypi.stackexchange.com/q/92782/71754) (ext-wifi is easier, and worked out of box, only repeater)

[StackExchange Tutorial](https://raspberrypi.stackexchange.com/a/118877/71754) (eth0 wlan0 bond → served to wlan1)

Create hotspot (baremetal): [https://anooppoommen.medium.com/create-a-wifi-hotspot-on-linux-29349b9c582d](https://anooppoommen.medium.com/create-a-wifi-hotspot-on-linux-29349b9c582d) (+ sometimes useful snippets @ [https://github.com/lakinduakash/linux-wifi-hotspot/blob/master/src/scripts/create_ap](https://github.com/lakinduakash/linux-wifi-hotspot/blob/master/src/scripts/create_ap))

For only-onboard wifi: Service path (to edit in chroot): `/usr/lib/systemd/system/<service_name>`

### Archived

## Lineage OS RaspberryPi3 TV Build

Downloads: [https://konstakang.com/devices/rpi3](https://konstakang.com/devices/rpi3)

Setup: [https://www.linuxadictos.com/en/como-convertir-tu-raspberry-pi-en-un-android-tv-pero-merece-la-pena.html](https://www.linuxadictos.com/en/como-convertir-tu-raspberry-pi-en-un-android-tv-pero-merece-la-pena.html) or [https://www.makeuseof.com/tag/build-android-tv-box-raspberry-pi](https://www.makeuseof.com/tag/build-android-tv-box-raspberry-pi/)

## BalenaOS

### To edit something on /

```bash
mount -o remount,rw /

# To edit config.json manually
vi /mnt/boot/config.json
```