# PostInstall Notes

<aside>
💡 To be tested out: [https://community.frame.work/t/thermal-management-on-linux-with-rapl-limits/17416](https://community.frame.work/t/thermal-management-on-linux-with-rapl-limits/17416)

</aside>

> [Size of a git repository](https://stackoverflow.com/questions/8185276/find-size-of-git-repository)
> 

# Current Settings

### Chromium Options

- Turned on options (outside dotfiles)
    
    #ignore-gpu-blocklist
    
    #webui-tab-strip
    
    #enable-zero-copy
    
    #enable-vp9-kSVC-decode-acceleration
    
    #enable-raw-draw
    
- Scaling Zoom (Pinch-like)
    
    [Finally! MacOS/Windows like touchpad zoom gesture on all Chromium Browsers!](https://www.reddit.com/r/linux/comments/rmuh0o/finally_macoswindows_like_touchpad_zoom_gesture/)
    
- Hardware Acceleration
    
    [Reference](https://github.com/saiarcot895/chromium-ubuntu-build/issues/98#issuecomment-757710499)
    
    ```bash
    # Void intel driver build option
    ./xbps-src -o nonfree pkg intel-media-driver
    # OR
    wget -o etc/conf https://github.com/Animeshz/templates/blob/0c2b74e6d2016f9d795b48315b2dae42b1156cf5/xbps-src/etc/conf#L3
    ./xbps-src pkg intel-media-driver
    ```
    
    [Enhanced H264ify](https://chrome.google.com/webstore/detail/enhanced-h264ify/omkfmpieigblcllmkgbflkikinpkodlk/related?hl=en) extension (primarily for forced hardware decoding on low quality - 144p)
    

### rofi options

-kb-cancel Alt-space,Escape -matching fuzzy -sorting-method fzf -sort

### GTK

[https://github.com/vinceliuice/Qogir-theme](https://github.com/vinceliuice/Qogir-theme)

[https://github.com/yeyushengfan258/ArcStarry-Cursors](https://github.com/yeyushengfan258/ArcStarry-Cursors)

## OP Terminal Keybinds

alt+{e,v}: open editor

alt+p: toggle &| less

alt+s: toggle sudo

{ctrl+w,alt+d}: {backscapes,delete} whole word

ctrl+d: same as delete button (1 letter)

alt+l: ls

ctrl+l: clear

ctrl+{u,y}: {cut,paste}

### USB Logitech Mouse or WD extHDD through USB-A (not detected in lsusb and descriptor read error)

Partially pull wait a second, push all the way to end again.

### Initial General Purpose Packages

fish-shell neovim starship rofi xorg herbstluftwm yadm os-prober linux 5.15 dbus iwd chrony noto-fonts-ttf kitty curl feh fd neovim

eww (rustup gcc) ArcStarry-Cursors (lxappeareance)

## OP tools (use and also research)

[wting/autojump](https://github.com/wting/autojump)

[marin-m/SongRec - audio search/recognition](https://github.com/marin-m/SongRec)