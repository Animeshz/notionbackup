# Sem2 - Project1

[How to send real-time sensor data to PC from Raspberry Pi Zero?](https://stackoverflow.com/questions/64642122/how-to-send-real-time-sensor-data-to-pc-from-raspberry-pi-zero)

# MQTT (Message Queuing Telemetry Transport)

[mqtt protocol - Google Search](https://www.google.com/search?q=mqtt+protocol&oq=mqtt+protocol)

> Heavily optimized message forwarding protocol for IoT devices specifically.
> 

> Broker-Client connection, where load handling (& message buffering in case subscriber connection outages) is done by broker, and client (IoT device / Receivers: Mobile Web) can publish/subscribe to topics (channel) on the broker.
> 

> This also **fills the gap to transfer data between multiple languages** (backend & frontend).
> 

Following article is great to understand how it works with an example code.

[Getting Started with MQTT](https://www.hivemq.com/blog/how-to-get-started-with-mqtt/)

EMQX: To run mqtt broker locally or as a cloud service

EMQX Public Broker: public hosted broker for testing ([https://www.emqx.com/en/mqtt/public-mqtt5-broker](https://www.emqx.com/en/mqtt/public-mqtt5-broker))

[EMQX: Open-Source, Cloud-Native MQTT Broker for IoT](https://www.emqx.io/)

[Use MQTT with Raspberry Pi](https://www.emqx.com/en/blog/use-mqtt-with-raspberry-pi)

### Examples

Previously linked pages there ^ and more:

[How to use MQTT in Rust](https://www.emqx.com/en/blog/how-to-use-mqtt-in-rust)

[https://github.com/BitKnitting/flutter_adafruit_mqtt](https://github.com/BitKnitting/flutter_adafruit_mqtt)

[https://github.com/fdisotto/esp32-mqtt](https://github.com/fdisotto/esp32-mqtt)

[https://thenewstack.io/python-mqtt-tutorial-store-iot-metrics-with-influxdb/](https://thenewstack.io/python-mqtt-tutorial-store-iot-metrics-with-influxdb/)

### Proposed solution with MQTT (of what I understood about the data formats)

1. **RPi/Arduino/IoT-Device:** Publishes to something like *#sensors/data* topic.
2. **MQTT Broker (Server):** Either hosted locally into a separate machine or into raspberry pi itself, or use one of MQTT cloud services.
3. **Calculation Client:** Can be on same machine as MQTT server, subscribes to *#sensors/data*, performs some calculations, publishes to another topic like *#sensors/calculation*.
4. **Frontend Client:** Mobile/Web device, which pulls data from both *#sensors/data* & *#sensors/calculation*, and renders it.
5. **Data Archiving Client (optional):** Can be on same machine as MQTT server, subscribes to both *#sensors/data* & *#sensors/calculation, and archives it on database or sth.*

[Payload Encryption - MQTT Security Fundamentals](https://www.hivemq.com/blog/mqtt-security-fundamentals-payload-encryption/)

[Retained Messages - MQTT Essentials: Part 8](https://www.hivemq.com/blog/mqtt-essentials-part-8-retained-messages/)

```python
import paho.mqtt.client as mqtt
import time

def on_connect(client, userdata, flags, rc):
    print(f"Connected with result code {rc}")

client = mqtt.Client()
client.on_connect = on_connect
client.connect("broker.emqx.io", 1883, 60)

# send a message to the raspberry/topic every 1 second, 5 times in a row
for i in range(5):
    # the four parameters are topic, sending content, QoS and whether retaining the message respectively
    retain = i == 2
    client.publish('raspberry/topic', payload=i, qos=2, retain=retain)
    print(f"send {i} to raspberry/topic")
    time.sleep(1)

# Clear the retain, by sending empty payload
# client.publish('raspberry/topic', payload='', qos=0, retain=True)

client.loop_forever()
```

<aside>
💡 Authentication - inbuilt
Buffer Storage (retain/network-outage) - inbuilt
Request/Response (API-like) - Mqtt5 inbuilt

Encryption - setup application-level layer (ref ^)
Database - setup

UI - build using flutter

</aside>

### Misc Things

[https://github.com/golemparts/rppal](https://github.com/golemparts/rppal)

[Quality of Service (QoS) 0,1, & 2 MQTT Essentials: Part 6](https://www.hivemq.com/blog/mqtt-essentials-part-6-mqtt-quality-of-service-levels/)

[can MQTT be used to implement Request / Response behavior](https://stackoverflow.com/q/15912520/11377112)

[MQTT 5: How the Response Topic, Correlation Data, and Response Information features work.](https://www.hivemq.com/blog/mqtt5-essentials-part9-request-response-pattern/)

[Understanding And Using MQTT v5 Request Response](http://www.steves-internet-guide.com/mqttv5-request-response/)

## Backend

[Raspberry Pi 3 Model B Pinouts](https://pinout.xyz/) + just in case [Additional Info about the device](https://www.etechnophiles.com/raspberry-pi-3-gpio-pinout-pin-diagram-and-specs-in-detail-model-b/)

[Arduino Pinouts](https://robu.in/arduino-pin-configuration/) + [Upload & Run code in Arduino (Video)](https://www.youtube.com/watch?v=_-g5sWQyROg)

**Raspberry Pi**

Soil Moisture sensor - [Implementation (Hardware+Code)](https://www.electroniclinic.com/soil-moisture-sensor-with-raspberry-pi-circuit-diagram-and-python-code/)  +  [Demonstration (Video)](https://www.youtube.com/watch?v=UWbeHqyzP-c)

Gas Sensor - [Hardware Implementation](https://tutorials-raspberrypi.com/configure-and-read-out-the-raspberry-pi-gas-sensor-mq-x/) + [Software/Code](https://github.com/tutRPi/Raspberry-Pi-Gas-Sensor-MQ/blob/master/mq.py)  +  [Demonstration (Video)](https://www.youtube.com/watch?v=iq098ymIHQ4)

**Arduino**

Soil Moisture Sensor [Arduino (Hardware+Code)](https://create.arduino.cc/projecthub/MisterBotBreak/how-to-use-a-soil-moisture-sensor-ce769b) 

Gas Sensor - [Hardware+Code (CO2 only)](https://circuitdigest.com/microcontroller-projects/interfacing-mq135-gas-sensor-with-arduino-to-measure-co2-levels-in-ppm)

We gotta get the continuous data from the sensor into raspberry pi via analog-to-digital-converter in between, move that data to API/Web & listen for / get actions from the mobile devices back, and with that send signals to the water irrigators.

### IoT

```python
import RPi.GPIO as GPIO
import pygsheets
import time

GPIO.setmode(GPIO.BOARD)

input_pin = 11  # Board: 11  BCM: 17
GPIO.setup(input_pin, GPIO.IN)

gc = pygsheets.authorize(service_file='creds.json')
sh = gc.open('sound_data')
worksheet1 = sh.worksheet('title', 'main')

def callback(c):
    input_value = GPIO.input(c)
    print(input_value)
    worksheet1.append_table(values=[input_value])

GPIO.add_event_detect(input_pin, GPIO.BOTH, bouncetime=300)
GPIO.add_event_callback(input_pin, callback)

while True:
    time.sleep(1)
```