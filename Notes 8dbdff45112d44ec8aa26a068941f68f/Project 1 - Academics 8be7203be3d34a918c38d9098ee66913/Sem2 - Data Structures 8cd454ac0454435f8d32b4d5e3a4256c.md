# Sem2 - Data Structures

### Classification →

- Primitive (int float char)
- Non-Primitive (UserDefined DS) → Linear (Array, LinkedList, Queue, Stack) | NonLinear (Tree, Graph)

1. Array vs LinkedList: Efficient for fetching vs modification
2. Stack: Last in - first out
3. Queue: First in - first out (linked list) (* circular queue array can be reused from front after rear side is filled)
4. Tree: Structure with one-parent-multiple-child relationship
5. Graph: Mathematically G=(v,e) where v=setof(vertices/nodes) & e=setof(edges)
    - Shortest distance between nodes: 1. no of hopes 2. weight of edges (in weighted edged graph)

- **Array →**
    - Location(a[i]) = BaseAddr + (i-LB)*sizeof(element);; LB = lower-bound
    - Location(a[i][j]) = BaseAddr + ((i-LB1)*n(col) + (j-LB2))*sizeof(element)
    - Location(a[i][j][k]) = BaseAddr + ((i-LB1)*n(row)*n(col) + (j-LB2)*n(col) + (k-LB3))*sizeof(element)

Assignment: Lower (upper 0) & Upper (lower 0) Triangular Matrix: Store in row-major and col-major order i.e. in show in linear memory (donot store 0, no memory allocated for it)

- **LinkedList →**
    
    ```cpp
    struct node {
       int data;
       struct node *next;
    }
    struct node a = { 5, NULL };
    struct node b = { 10, NULL };
    a.next = &b;
    ```
    
    - Deleting last node of LL
    - Deleting the node containing certain data in LL (first element condition → move w/common logic for others)
    - Insert a node (start / anywhere else)
        - Question: Insert data a.t the end of node
        - Question: Insert data before a node with certain data
    - Reverse a LinkedList
    - Check if there’s a cycle in the Linked List - flag of iterated
    - Implement Polynomial addition & subtraction using LL `node (int, int, ptr)`
        
        ```cpp
        PolyAdd(P1, P2, P3) {
          while (P1!=NULL && P2!=NULL)
            if (same pow P1,P2) P3->coeff = P1->coeff + P2->coeff; move;
            else if (p1->pow > p2->pow) p3 = p1; move_p1;
            else p3 = p2; move_p2;
        
          //Ani the code where you are putting p3 = p1 might be wrong because then it will also put the address to the next node in p3 to p1
        	while (P1!=NULL) p3 = p1; move_p1;
        	while (P2!=NULL) p3 = p2; move_p2;
        }
        
        PolyMult(P1, P2, P3) {
          for (p1 = P1; p1 != NULL; p1=p1->next)
        	  for (p2 = P2; p2 != NULL; p2=p2->next) {
              p3 = P3;
              while (p3->next->pow <= p1->pow + p2->pow) p3 = p3->next;
              
              if (p3->next-> pow == p1->pow + p2->pow) p3->coeff += p1->coeff * p2->coeff;
        			else insert_node_after(p3, node { coeff: p1->coeff * p2->coeff, pow: p1->pow + p2->pow })
            }
        }
        ```
        
    - DoubleLinkedList (prev & next ptr)
        - Delete last node: `while(p->next)p=p->next; if(p->prev)p→prev→next = NULL; free(p); p=NULL;`
        - Insert b4 element having value: `while (!(p->data == y || p->next == NULL)) p=p->next; q->prev = p->prev && q->next = p; p->prev->next=q; p->prev=q;`
        - Delete some node in the middle: `while (p->data != y) p=p->next; p->prev->next=p; p->next->prev=p->prev; free(p); p=NULL;`
- **Stack →**
    
    push(stack, element): stack
    
    pop(stack): element
    
    is_empty(stack): boolean
    
    is_full(stack): boolean
    
    ```cpp
    void push(s, N, top, x) {
      if (top == n-1) exit(1);  //stack overflow
      else s[++top] = x;
    }
    
    pop() {
      if (top == -1) exit(1);  //stack underflow
      return s[top--];
    }
    ```
    
    - Applications of Stack
        - Recursion
            - Tail Recursion: One direction `if (n<1) return; else { printf(n); call_itself(n-1); }` (single recursion is at the tail)
            
            // Comment by adi ( I can’t see the diff in the Tail and non-tail in the code . explain it to me later)
            
            - Non-Tail Recursion: `if (n<1) return; else { printf(n); call_itself(n-1); printf(n); }`
            - Indirect Recursion: 2 functions calling themselves (use tree to find the output, dfs path traversal)
            - Nested Recursion: e.g. A(m,n) = { n+1 if m=0 | A(m-1, 1) if n=0 | A(m-1, A(m,n-1)) otherwise }
        - (Infix-Postfix | Prefix-Postfix) conversion:
            - a+b*c = +a*bc (prefix)  =  abc*+ (postfix)
            - a+b-c = -+abc (prefix) = ab+c- (postfix)
            
            | Symbol | Priority | Associativity |
            | --- | --- | --- |
            | ↑ | 3 | R→L (inclusive with itself) |
            | *,/ | 2 | L→R (pop/exclusive) |
            | +,- | 1 | L→R (pop/exclusive) |
            - Programmatically: Scan L→R, print the operand if encountered, or push the operator in the stack. Higher priority can be pushed, but in case of vice versa, pop the stack until lower priority (so L→R). If `)` comes, pop all above in stack.
            - Exam: 3 columns → Input | Operator Stack | Postfix Expression
        - Postfix evaluation:
            - Scan L→R, Make Operand Stack
            
            ```bash
            foreach character:
              if operand: stack.push()
              if operator: stack.push(op1 (operator) op2)   # where op1 = first stack.pop, op2 = second stack.pop
            ```
            
        - Parenthesis matching:
            - Scan L→R, make stack containing opening brackets
            
            ```python
            foreach character:
              if opening_bracket: push()
              else: pop() && match equal pair
            
            if stack is empty at the end: expression is balanced
            
            3 improper conditions:
              mismatch
              remaining excess bracket
              too few opening bracket
            ```
            
        - Fibonaaci Series
            - Fib(n) = { n if n≤1 ;; otherwise Fib(n-1) + Fib(n-2) }
        - Tower of Hanoi problem
            
            
            | 1 |  |  |
            | --- | --- | --- |
            | 2 |  |  |
            | 3 |  |  |
            | 4 |  |  |
            | L | M | R |
            
            |  |  |  |
            | --- | --- | --- |
            |  | 1 |  |
            |  | 2 |  |
            | 4 | 3 |  |
            | L | M | R |
            
            |  |  |  |
            | --- | --- | --- |
            |  | 1 |  |
            |  | 2 |  |
            |  | 3 | 4 |
            | L | M | R |
            - To move 4, 7 steps to move 3 discs into M, 1 to move 4th disc from L to R, then 7 again for 3 discs M to R.
            
            ```c
            TOH(N, L, M, R) {     // M: temporary
              if (N==0) return;
              TOH(N-1, L, R, M);  // shift n-1 from L to M
              move(L, R);         // last disc from L to R
              TOH(N-1, M, L, R);  // shift n-1 from M to R
            }
            
            // if (N==even) first move → M (initially temporary)
            // if (N==odd) first move → R (net dest)
            ```
            
        
        ```cpp
        max_recursive(a, i, j) {  // i -> 0, j -> n-1
          return if (i==j) a[i];
          else if (a[i] > a[j]) max_recursive(a, i, j-1);
          else max_recursive(a, i+1, j);
        }
        
        print(a, n) {
          if (n!=0) print(a, n-1);
        	else printf("%d ", a[n]);
        }
        print(a, i, j) {
          if (i==j) printf("%d ", a[i]);
        	else printf("%d ", a[i]); print(a, i+1, j);
        }
        
        // Check output for this non-tail recursive function
        void NTR(n) {
          if (n <= 1) return;
          else {
            printf(n);
        		NTR(n-1);
            printf(n);
        		NTR(n-1);
          }
        }
        // OP: 4 3 2 2 3 2 2 4 3 2 2 3 2 2
        ```
        
        A(1,3) → A(0,A(1,2)) → A(1,2)+1 →A(0,A(1,1))+1 → A(1,1)+2 → A(0,A(1,0))+2 → 3+A(1,0) → 3+A(0,1) → 4
        
        A(2,4) → A(1,A(2,3)) → A(1,A(1,A(2,2))) → A(
        
- **Queue →**
    
    ```cpp
    F: front  (ini: -1 -> otherwise first existent element)
    R: rear   (ini: -1 -> otherwise last existent element)
    size: N
    
    is_full() = F==N-1
    is_empty() = F==R  // or is it F==-1
    
    void insert(q, N, F, R) {
      if (F == n-1) exit(1);  //stack overflow
      else q[++R] = x; if(R==0) F=0;
    }
    
    delete(q, N, F, R) {
      if (R == N-1) exit(1);  //stack underflow
      return (del s[++F]);
    }
    ```
    
    - Circular Queue →
        - Full: (R == N-1 && F == -1) || (R == F-1)
        - Underflow / Empty: F==-1    → reinitialize R=F=-1 if R==F in case of deletion
        - R++ (-N) or F++ (-N) in case F/R reaches end