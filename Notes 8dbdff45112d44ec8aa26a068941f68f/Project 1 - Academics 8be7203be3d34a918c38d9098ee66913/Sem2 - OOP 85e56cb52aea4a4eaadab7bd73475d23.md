# Sem2 - OOP

### Features of Java

OOP Language ex: VisualBasic Fortran Pascal

### OOP vs Procedural Programming

Classes and Objects | Procedures (Functions)

Access Specifiers | No access specifiers

Overloading | No overloading

Overriding | No overriding

Data abstraction & encapsulation | 

### Classes & Objects

```cpp
Cellphone (Class)
  - color        }
  - company name } Attributes
  - design       }
  - MakeCall()    }
  - SendMsg()     } Functions
  - RecCall()     }
  - RecMsg()      }

Object1 -> company name = Samsung
Object2 -> company name = Nokia
```

## OOPs

- Abstraction: Hiding the internal details
- Inheritance: Extending old structure (is-a relationship)
- Polymorphism: Many+Forms (can be achieved by method overloading —same-function-different-parameters or overriding —replacing-function-in-subclass)
- Method Binding (static —default || dynamic —in-polymorphism-when-overriding) & Message Passing (arguments):

```java
// MyFirstJavaProgram.java
public class MyFirstJavaProgram {
    public static void main(String args[]) {
				System.out.println("Hi");
    }
}
```

### Classification

Constructor: Default, NoArg, Parameterized

Inheritence:

Class: Single, MultiLevel, Hierarchial

Interfaces: Hybrid, Multiple

**final:** var, method, class. (Blank vars must be initialized by class/object construction for static/non-static respectively)