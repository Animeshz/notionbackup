# Sem1 - PC (Professional Communication)

## Paragraph Writing

A paragraph must be containing single idea, or wrap brief introduction of every idea in itself. It should not contain 2-3 out of n ideas in it.

**A paragraph is made up of:**

- Introduction - An interesting start.
- Main Content.
- Conclusion or Connection with next Paragraph.