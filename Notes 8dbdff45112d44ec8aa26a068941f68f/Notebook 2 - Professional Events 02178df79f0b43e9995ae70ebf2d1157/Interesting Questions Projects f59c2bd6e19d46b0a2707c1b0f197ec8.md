# Interesting Questions/Projects

## StackOverflow

[What is the best way to implement optional library dependencies in Rust?](https://stackoverflow.com/questions/26945875/what-is-the-best-way-to-implement-optional-library-dependencies-in-rust)

[How to deal with licences after forking a project?](https://opensource.stackexchange.com/a/4440)

## Articles

[Creating Tiny Desktop Apps With Tauri And Vue.js - Smashing Magazine](https://www.smashingmagazine.com/2020/07/tiny-desktop-apps-tauri-vuejs/)

Docker for deployment: https://github.com/GoogleContainerTools/distroless