# English Speaking / Talks

# Speech

WHAT    SO WHAT    NOW WHAT

## Attention Grabbers

(Quote/Proverb | Fact | Story)

- … but the fact of the matter is that …
- Things might be simple but at the same time they may not be so easy.

## Engage

- Raise your hand if you’ve done 1 of these 4 things before in your life. Has anybody done any … or …
- Put attention to audience - us, you, we (you’re in service of audience needs)
- Audience Centric Approach - less stressful for me, more engaging for audience.

## Call To Action

… is a necessary skill & I encourage everybody to master it.

[Topics](English%20Speaking%20Talks%208e4f0042cade4bb4a9993330e5db948d/Topics%205b7350849e254ceba5bbeaea79d61bb3.csv)