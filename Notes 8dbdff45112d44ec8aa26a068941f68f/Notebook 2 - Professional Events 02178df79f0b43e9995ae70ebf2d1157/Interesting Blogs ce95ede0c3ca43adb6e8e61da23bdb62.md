# Interesting Blogs

[Tailwind CSS IntelliSense is not auto-suggesting in VScode inside a svelte js application? it works if I reinstall it every time I open VScode](https://stackoverflow.com/questions/70602650/tailwind-css-intellisense-is-not-auto-suggesting-in-vscode-inside-a-svelte-js-ap)

[index - FREEMEDIAHECKYEAH](https://www.reddit.com/r/FREEMEDIAHECKYEAH/wiki/index)

[Tiny Rocket](https://jamesmunns.com/blog/tinyrocket/)

[Rewritten in Rust: Modern Alternatives of Command-Line Tools](https://zaiste.net/posts/shell-commands-rust/)

[14 great tips to make amazing CLI applications](https://dev.to/wesen/14-great-tips-to-make-amazing-cli-applications-3gp3)

[https://github.com/rzashakeri/beautify-github-profile](https://github.com/rzashakeri/beautify-github-profile)

[VSCode Extensions I'm in LOVE with](https://dev.to/tmchuynh/vscode-extensions-im-in-love-with-oab)

[8 Visualizations with Python to Handle Multiple Time-Series Data](https://towardsdatascience.com/8-visualizations-with-python-to-handle-multiple-time-series-data-19b5b2e66dd0)

[19 Hidden Sklearn Features You Were Supposed to Learn The Hard Way](https://towardsdatascience.com/19-hidden-sklearn-features-you-were-supposed-to-learn-the-hard-way-5293e6ff149)

[Modern Recommendation Systems with Neural Networks](https://towardsdatascience.com/modern-recommendation-systems-with-neural-networks-3cc06a6ded2c)

[Crash reporting in Rust](https://jake-shadle.github.io/crash-reporting/)

[Builder Lite](https://matklad.github.io/2022/05/29/builder-lite.html)