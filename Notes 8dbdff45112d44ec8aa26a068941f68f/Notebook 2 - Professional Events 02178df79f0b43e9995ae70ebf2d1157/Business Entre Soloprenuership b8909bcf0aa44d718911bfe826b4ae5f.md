# Business / Entre|Soloprenuership

Status: Open

## Franchise

A franchise is **a joint venture between a franchisor and a franchisee**. The franchisor is the original business. It sells the right to use its name and idea. The franchisee buys this right to sell the franchisor's goods or services under an existing business model and trademark.

[Entrepreneur Salary - How Much Pay Business Owners Should Get?](https://www.profitbooks.net/how-to-set-entrepreneur-salary/)

# Idea

# Sales

# Execution

# Timing

# Marketing

# Team