# Exploration and WishList

Status: Open

## AI/ML

- [ ]  Modern Recommendation System
- [ ]  NLU (Natural Language Understanding)
- [ ]  Sharpening the image using AI/ML
- [ ]  Normalize image after removal of an object by AI/ML (AKA adding missing details)  | [DeScript](descript.com/)
- [ ]  Spam Filteration
- [ ]  RealTime Gesture detection (OpenCV) and holographic computing (like in scifi-movies)
- [ ]  https://github.com/se1exin/face-detect-mqtt - Face Detection

## Architecture

- [ ]  Modular Plugin System for low-level stuffs like firmware etc.

## WebD

- [ ]  How [https://www.gsocorganizations.dev](https://www.gsocorganizations.dev/) is offline ready.
- [ ]  [https://codepen.io/soulwire/pen/DdGRYG](https://codepen.io/soulwire/pen/DdGRYG)
- [ ]  [https://codepen.io/natewiley/details/QWGdZJ](https://codepen.io/natewiley/details/QWGdZJ)
- [ ]  [https://codepen.io/hoanghien0410/pen/MMPaqm](https://codepen.io/hoanghien0410/pen/MMPaqm)
- [ ]  [https://codepen.io/johnblazek/pen/nxgVPR](https://codepen.io/johnblazek/pen/nxgVPR)
- [ ]  https://github.com/xtermjs/xterm.js/ - terminal on the web, check implementation for connection to shell (remote-shell too)

## Hardware

- [ ]  How this flexispot desk go up and down [Flexispot Comhar](https://www.flexispot.com/comhar-all-in-one-standing-desk-eg8-ew8) ([running demo](https://www.youtube.com/watch?v=hSVp4jaCWXE))
- [ ]  https://github.com/maglash64/g402_camera - firmware implementation of a mouse to use as camera

## Supply Chain Enhancement / Optimization / Seamless Workflow

Scalability is way better online, but still Click-Through-Rate is still low.

Better graphics / thumbnails, attracting logos, attention seeking music, simpler non-overwhelming interfaces, etc. are very crucial elements.

Cheer up for the statistics if there are enough usages, by notifications / emails etc. In order to retain the users.

### Space

Almost next to no friction in space, so needs equal amount of fuel to slow it down, as needed for acceleration.