# Project Ideas

Status: Open

<aside>
💡 **Concepts**

- [ ]  Group Discussion (not meeting) platform to improve an Idea. Filter out w/ AI or Judges.
With patent (share): Starter + Improver (splitted based on how useful they became)
- [ ]  Room made of thermocol foam rolls (foldable), ez and sasta cooling soln.
- [ ]  Information propagator - college news, misc stuffs, comparator for motivation (option to share or hide my stats), project/script publish/laid out so anyone who want can come to know about (for use/contribution/learn-new-from-others-PR) with bookmarking.
- [ ]  Efficient information saving/passing - parent hash & current delta. Resolve delta on the parent in the server. (mutually resolve conflict between whom conflict occurred - commenting patch suggestion ability). Name: sit (simple info tracker), kit, lit, zit ([already exists](https://github.com/Oblomov/zit) unmaintained also dependent on git as backend).
- [ ]  CPU Multiplexing/Clustering, from old processor to gain more parallelism
- [ ]  Self-Destructing flash storage or etc.
- [ ]  A very small anywhere attachable (magnetic) object locator (GPS-like finder)
- [ ]  Home File Server (using Android)
- [ ]  Mini Cooler - TissuePaper left n right (auto pumping), portable, water in outside foldable-part
- [ ]  Integration of physical stuffs using device primitives (e.g. Mobile: voice & Desktop: keyboard).
- [ ]  USB Pull Lock
- [ ]  Smart physical locking (door,suitcase,etc) system, generic.
- [ ]  Self Repairable digital cells, self learning digital bodies, synthetic biology!!!
- [ ]  Preserving knowledge by studying about brains, matrixizing it, creating immoral bodies then. But then valuation problems in probable virtual world (maybe company owned!?).
- [ ]  Torque Converter (friction disk as in AMT) between wheel and clutch for geared vehicle
- [ ]  Making computers primitive to work on images (like currently at text). Intermediate image format in between raster & vector, easily vectorizing a raster image.
- [ ]  (ML) Calculate importance/weight of a function/code. Like traffic through a GPS satellite on a road. → EagleMode/WebistePopularityIndex(graph-ankithooda/sidtalk-ref).
- [ ]  (ML) Classify code into certain defined paradigms etc. Code Quality checks.
- [ ]  (Low Priority) IoT if this then that integration (very unlikely optional ML)
- [ ]  (Low Priority) Virtual Rain background in webapp/tarui
- [ ]  (Low Priority) USB button/slider expansion card
</aside>

### Nix-Like configuration framework generic to distro (LFS), so that easily make bootstrapping scripts :) Maybe a rust project too!!?

### A centralized place/platform for personal opinions / polls. Voice raise (moderated), committee raises, club raises, etc.

With a common data format, so ez to visualize/analyze later on

### Discord Bot

### Implement stuffs from this concept

[Introducing AvdanOS (Concept)](https://www.youtube.com/watch?v=tXFEiw1aJTw)

[Introducing AvdanOS Pro](https://www.youtube.com/watch?v=woMGr7Ip2Jk)

[ChromeOS Redesign Concept by Concept Central](https://www.youtube.com/watch?v=70HwyNKrQ3s)

File manager specially with the hover/single-click circular panel

[Tobias Bernard on Twitter: "Probably not happening anytime soon for technical reasons (i.e. it'd probably be in an app instead of a shell feature), but wouldn't this be fun :P pic.twitter.com/zqWu8kOyNI / Twitter"](https://twitter.com/tobias_bernard/status/1536738308826583040?t=wuoAEADsUZ1iMPROQpCCDg&s=19)

### A platform for discussing potential problems, as every crime by anyone except serial killers are committed for a reason, if the reason can be resolved, wouldn’t it be nicer?