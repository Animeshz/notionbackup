# Frontend (WEB) Notes

Status: Resurface

# Top Resources

[https://github.com/freeCodeCamp/freeCodeCamp](https://github.com/freeCodeCamp/freeCodeCamp)

[41 Common Web Application Vulnerabilities Explained](https://securityscorecard.com/blog/common-web-application-vulnerabilities-explained)

[Top 5 Most Dangerous Web Application Vulnerabilities | Security On-Demand](https://www.securityondemand.com/top-5-web-application-vulnerabilities/?utm_source=rss&utm_medium=rss&utm_campaign=top-5-web-application-vulnerabilities)

# CSS

TailwindCSS, and its extensions (tailwindcsstoolbox.com, tailblocks), etc. Bootstrap is copy paste, TailWind is also html only, but not copy-paste, it has JIT compiler, and lightweight final dist.

### display

| Display value | Notes |
| --- | --- |
| block | default, take all horizontal space as needed, ensures \n before and after. Most elements has this as default. |
| inherit | copies parent. |
| inline | Just inline, no boxing (width/height). Example: span, a. |
| inline-block | inline, but ability to set height/width like block. |
| initial | reset back to tag’s original property (if parent/generic rule has overridden its property) |
| flex |  |
| grid |  |

### position

| Position value | Notes |
| --- | --- |
| static | default, elements lay out in order as in html. |
| relative | Addition of top/left/bottom/right, but this is very vague usecase, cuz just leaves the DOM tree, same style/effects but its like its dragged (even overflows from parent), relative to where it should’ve been. |
| absolute | DOM just ignores the element, no space reserved unlike relative, absolute positioning with addition of top/left/bottom/right, relative to the nearest relative parent, or html. |
| fixed | fixed, always, no scroll effect, relative to entire html page, has nothing to do with parents. |
| sticky | like static, but once limiting top/left/bottom/right reaches, it leaves where it should be and starts floating on the page while reserved position in html still exists. |