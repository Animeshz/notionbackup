# Time Management

To be really honest with you, I’ve a bad memory, I even bring my phone to the toilet so that I can note down my ideas before I forget about it…

For the last 2 years, till this day, I’ve been consistently dependent on ToDo lists to simplify my workflow.

And one thing that started to bother me last year was that my list grown too much, over 60 tasks (including small ones), but 60… I was unable to do any task, my schedule was broken, my whole day just gets exhausted without even noticing just by prioritizing one task over another, new task/ideas come and stack up, and this was starting to get into a dead end.

Then I take back a moment and tried different time management techniques to counter this. And that’s what I’m gonna talk here. Also I’d like to give a different perspective to see the time.

For the last 2 yrs, my biggest fear was to keep myself out of stress. You know 

- Your relative time doesn’t depend on how much absolute time you have, its determined by where & how you’re spending it.