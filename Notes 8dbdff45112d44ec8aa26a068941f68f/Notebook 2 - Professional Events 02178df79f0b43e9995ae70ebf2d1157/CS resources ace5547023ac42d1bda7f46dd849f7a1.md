# CS resources

Status: To Be Formatted

[Complex coding concepts explained through visual diagrams](https://betterprogramming.pub/complex-coding-concepts-explained-through-visual-diagrams-4a9b543cc323)

[Introduction - The Linux Kernel documentation](https://linux-kernel-labs.github.io/refs/heads/master/lectures/intro.html)

[https://github.com/kamranahmedse/developer-roadmap](https://github.com/kamranahmedse/developer-roadmap)

[https://github.com/EbookFoundation/free-programming-books](https://github.com/EbookFoundation/free-programming-books)

[https://github.com/jwasham/coding-interview-university](https://github.com/jwasham/coding-interview-university)

[https://github.com/topics/awesome](https://github.com/topics/awesome)

CP Guide:

[](https://duoblogger.github.io/assets/pdf/memonvyftw/guide-t-cp.pdf)

Rust Cheat Sheet

[LGR Cheat Sheet](https://docs.google.com/document/d/1kQidzAlbqapu-WZTuw4Djik0uTqMZYyiMXTM9F21Dz4/edit)

[Rust: Your code can be perfect](https://youtu.be/IA4q0lzmyfM)

# Productivity Resources / Tools

[https://github.com/readme/guides/code-as-documentation](https://github.com/readme/guides/code-as-documentation)

[https://github.com/adrianscheff/useful-sed](https://github.com/adrianscheff/useful-sed)

[https://techcrunch.com/2021/09/30/codesee-launches-oss-port-open-source-project-to-help-developers-visualize-code-base/amp/](https://techcrunch.com/2021/09/30/codesee-launches-oss-port-open-source-project-to-help-developers-visualize-code-base/amp/)

[https://www.tecmint.com/linux-project-management-tools/amp/](https://www.tecmint.com/linux-project-management-tools/amp/)

### Git

[https://www.freecodecamp.org/news/advanced-git-interactive-rebase-cherry-picking-reflog-and-more/amp](https://www.freecodecamp.org/news/advanced-git-interactive-rebase-cherry-picking-reflog-and-more/amp)

WebDev:
[https://www.freecodecamp.org/news/responsive-web-design-certification-redesigned/amp/](https://www.freecodecamp.org/news/responsive-web-design-certification-redesigned/amp/)[https://kinsta.com/blog/html-vs-html5/](https://kinsta.com/blog/html-vs-html5/)[https://dev.to/iainfreestone/10-trending-projects-on-github-for-web-developers-31st-december-2021-46c6](https://dev.to/iainfreestone/10-trending-projects-on-github-for-web-developers-31st-december-2021-46c6)

Dialogflow alternative: [https://rasa.com/docs/rasa/migrate-from/google-dialogflow-to-rasa/](https://rasa.com/docs/rasa/migrate-from/google-dialogflow-to-rasa/)

Rust NLP/NLU:
[https://github.com/lst-codes/rs-natural](https://github.com/lst-codes/rs-natural)[https://rasa.com/blog/intents-entities-understanding-the-rasa-nlu-pipeline/](https://rasa.com/blog/intents-entities-understanding-the-rasa-nlu-pipeline/)[https://towardsdatascience.com/keyword-extraction-a-benchmark-of-7-algorithms-in-python-8a905326d93f](https://towardsdatascience.com/keyword-extraction-a-benchmark-of-7-algorithms-in-python-8a905326d93f) (benchmark of keyword extract algo)
[https://towardsdatascience.com/a-complete-guide-to-natural-language-processing-nlp-c91f1cfd3b0c](https://towardsdatascience.com/a-complete-guide-to-natural-language-processing-nlp-c91f1cfd3b0c)

KVM/Qemu/USB:
[https://www.linux-kvm.org/page/USB_Host_Device_Assigned_to_Guest](https://www.linux-kvm.org/page/USB_Host_Device_Assigned_to_Guest)[https://www.earth.li/~noodles/blog/2012/10/kvm-usbmon-wireshark-win.html](https://www.earth.li/~noodles/blog/2012/10/kvm-usbmon-wireshark-win.html)

Mouse wakeup disable:
[https://askubuntu.com/questions/252743/how-do-i-prevent-mouse-movement-from-waking-up-a-suspended-computer](https://askubuntu.com/questions/252743/how-do-i-prevent-mouse-movement-from-waking-up-a-suspended-computer)

Java libs: [https://www.geeksforgeeks.org/top-10-libraries-every-java-developer-should-know/amp/](https://www.geeksforgeeks.org/top-10-libraries-every-java-developer-should-know/amp/)

News: [https://bill.harding.blog/](https://bill.harding.blog/)

Business tools: [https://opensource.com/article/21/12/open-source-business-tools](https://opensource.com/article/21/12/open-source-business-tools)