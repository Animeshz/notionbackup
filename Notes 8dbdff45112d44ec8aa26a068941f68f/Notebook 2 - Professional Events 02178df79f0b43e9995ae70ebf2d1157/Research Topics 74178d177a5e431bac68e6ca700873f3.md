# Research Topics

Status: Resurface

- [ ]  Soft Door-Close Mechanism - WashingMachine against gravity, Kitchen Cubord
- [ ]  Force-Torque Converter (Kitchen Appliances)
- [ ]  Car Transmission Mechanism, AMT Torque Converter
- [ ]  Relicensing (e.g. [ManimPango](https://github.com/ManimCommunity/ManimPango/releases))
- [ ]  UEFI related: SecureBoot keys extraction (https://github.com/DHowett/framework-secure-boot)
- [ ]  How to know when to stop / where max efficiency occur. E.g. torque vs acceleration, financial calculation stuffs, processing power vs clock speed vs cpu die size, lesser water quantity for faster warming or more for larger holding at power outage in geyser, etc.
- [ ]  [VatsalSir’s Youtube Comment Analyzer](https://github.com/vatsal2473/yt_comment_analyser/blob/main/comment_analysis.ipynb)