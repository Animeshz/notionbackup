# Project 1 - Academics

<aside>
💡 Ultimate goal of this project here

</aside>

## Top of mind

*(the top 3 considerations as of right now)*

[Sem2 - Project1](Project%201%20-%20Academics%208be7203be3d34a918c38d9098ee66913/Sem2%20-%20Project1%20ecdeb96a4264442cbe30f13152653e4d.md)

1. My favorite ~~bar~~ bookstore is moving to that new location
2. ...
3. ...

## Action Items

*(actions you need to take for this project)*

## Quick Links

*(Link to other Notion pages that may be relevant to this project)*

## Quick Points?

**Padhaku Seniors:** Priyanshu, Nehal Sharma, Tejas Taneja

Placements Main Topics: DSA + DBMS + OOPS + OS

## Notes

*(type in "/create linked database" and search for the notebook-database you'd like to insert here👇🏻)*

[Sem1 - DBMS](Project%201%20-%20Academics%208be7203be3d34a918c38d9098ee66913/Sem1%20-%20DBMS%2020202f3aa2814f59af002b8438e0d137.md)

[Sem1 - PC (Professional Communication)](Project%201%20-%20Academics%208be7203be3d34a918c38d9098ee66913/Sem1%20-%20PC%20(Professional%20Communication)%2000e5d90f7e874a66b0768bb3db79c11c.md)

[Sem1 - WebD](Project%201%20-%20Academics%208be7203be3d34a918c38d9098ee66913/Sem1%20-%20WebD%208b181b46b82d47f29a3b2b01114490f4.md)

[Sem2 - Data Structures](Project%201%20-%20Academics%208be7203be3d34a918c38d9098ee66913/Sem2%20-%20Data%20Structures%208cd454ac0454435f8d32b4d5e3a4256c.md)

[Sem2 - OOP](Project%201%20-%20Academics%208be7203be3d34a918c38d9098ee66913/Sem2%20-%20OOP%2085e56cb52aea4a4eaadab7bd73475d23.md)

## Clubs

[E-Cell](Project%201%20-%20Academics%208be7203be3d34a918c38d9098ee66913/E-Cell%20f4cab93e403349d6a4aaa7403023f93a.md)

[Axios - The Technical Club](Project%201%20-%20Academics%208be7203be3d34a918c38d9098ee66913/Axios%20-%20The%20Technical%20Club%205e84ec6041a546f2b04af4afb1f33fd8.md)

---

[Archived](Project%201%20-%20Academics%208be7203be3d34a918c38d9098ee66913/Archived%20d3130cbf402e4bc1940d2c9e9371e8e0.md)